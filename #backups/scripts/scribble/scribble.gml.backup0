// 2022-01-09 16:00:58
/// @param _string
/// @param ...
/// Returns a Scribble text element corresponding to the input string
/// If a text element with the same input string (and unique ID) has been cached, this function will return the cached text element
/// 

function scribble()
{
    #args _string,
    var _unique_id = (argument_count > 1)? string(argument[1]) : string(SCRIBBLE_DEFAULT_UNIQUE_ID);
    
    if (is_struct(_string) && (instanceof(_string) == "__scribble_class_element"))
    {
        __scribble_error("scribble() should not be used to access/draw text elements\nPlease instead call the .draw() method on a text element e.g. scribble(\"text\").draw(x, y);");
    }
    else
    {
        _string = string(_string);
    }
    
    var _weak = global.__scribble_ecache_dict[$ _string + ":" + _unique_id];
    if ((_weak == undefined) || !weak_ref_alive(_weak) || _weak.ref.__flushed)
    {
        return new __scribble_class_element(_string, _unique_id);
    }
    else
    {
        return _weak.ref;
    }
}