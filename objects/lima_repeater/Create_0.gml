/// @description code

// Hereda de lima panel.
event_inherited();

// Detectar objetos puestos sobre el panel.
index = 0;
index_last = 0;
index_now  = 0;

pressed = 0;

elements = ds_list_create();

steps = max(1, steps);	// no negativos o 0

#region Metodos

// Modificar Activate
Activate = function(_focus = false) {
	isActive = true;
	isFocus  = _focus;
	
	isVisible = true;
	alarm[11] = 1;
}

Teleport = function(_x, _y, _relative = false, _childs = true) {			
	var i = 0; repeat(ds_list_size(elements) ) {
		var in = elements[| i++];
		
		in.Teleport(_x, _y, _relative);
	}
}

RepeatCreate  = function() {
	var i = 0;
	
	repeat (repeats) {
		RepeatExecute(i++);
	}
	
	RepeatEnd();
}

RepeatExecute = function(count) {};

RepeatEnd = function() {};

RepeatAdd = function() {
	var _add = {
		panel: [],
		text : []
	};


	var i = 0; repeat(argument_count) {
		var _element = argument[i++];	
		
		switch (object_get_name(_element.object_index) ) {
			case lima_text :	array_push(_add.text , _element);	break; 	
			case lima_panel:	array_push(_add.panel, _element);	break; 
		}
	}
	
	return (_add);
}

RepeatClean = function() {
}

Advance = function(_value) {
	index_last = index;
	index = clip(0, repeats - 1, index + _value);
	index_now  = index;
	
	return (index);
}


#endregion

alarm[11] = 1;