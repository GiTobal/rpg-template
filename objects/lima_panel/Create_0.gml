event_inherited();

// Cambiar tipo
is = LIMA_COMPONENTS.PANEL;

panelSprite = sprite_index;
panelScaleH = image_yscale;
panelScaleW = image_xscale;
panelAlpha = image_alpha;
panelColor = image_blend;

panelX = x;
panelY = y;

#region Heredar
// Heredar propiedades basicas
if (inherit != noone) {
	color = inherit.color;
	font  = inherit. font;
	size  = inherit. size;
	
	halign = inherit.halign;
	valign = inherit.valign;
}

#endregion

// Plantillas
Templates = {
    selected:   global.__lima_dummy_function,  // isFocus = true  && isActive = true 
    deselected: global.__lima_dummy_function,  // isFocus = false && isActive = true
    active:     global.__lima_dummy_function,  // isFocus = true  && isActive = false
    desactive:  global.__lima_dummy_function   // isFocus = false && isActive = false
}

#region Metodos
selectTemplate = function() {
	if (isActive) {
		return (isFocus) ? Templates.selected : Templates.deselected;	
	} else {
		return (isFocus) ? Templates.active : Templates.desactive;
	}
}


#endregion

#region Eventos

lima_default_events();

/*
	Select_X: Al presionar cierta tecla este elemento deja de estar en foco y la variable correspondiente pasa a estarlo
*/
lima_create_event("SELECT_UP"	, LIMA_EVTYPE.ONKEY, [keyboard_check_pressed, vk_up	 ] , function() {
	if (select_up == undefined) exit;
	
	if (instance_exists(select_up) ) {
		// Solo poner en focus si select_up esta activo
		with (select_up) {
			if (isActive) {
				isFocus = true;
				updateTemplate();
				
				lima_pass_event("SELECT_UP");
			}
		}
		
		// Dejar de estar en el foco
		isFocus   = false;
		updateTemplate();
	}
});
lima_create_event("SELECT_DOWN" , LIMA_EVTYPE.ONKEY, [keyboard_check_pressed, vk_down ], function() {
	if (select_down == undefined) exit;
	
	if (instance_exists(select_down) ) {
		// Solo poner en focus si select_down esta activo
		with (select_down) {
			if (isActive) {
				isFocus = true;
				updateTemplate();
				
				lima_pass_event("SELECT_DOWN");
			}
		}
		
		// Dejar de estar en el foco
		isFocus   = false;
		updateTemplate();
	}
});
lima_create_event("SELECT_LEFT" , LIMA_EVTYPE.ONKEY, [keyboard_check_pressed, vk_left ], function() {
	if (select_left == undefined) exit;
	
	if (instance_exists(select_left) ) {
		// Solo poner en focus si select_left esta activo
		with (select_left) {
			if (isActive) {
				isFocus = true;
				updateTemplate();
				
				lima_pass_event("SELECT_LEFT");
			}
		}
		
		// Dejar de estar en el foco
		isFocus = false;
		
		updateTemplate();
	}
});
lima_create_event("SELECT_RIGHT", LIMA_EVTYPE.ONKEY, [keyboard_check_pressed, vk_right], function() {
	if (select_right == undefined) exit;
	
	if (instance_exists(select_right) ) {
		// Solo poner en focus si select_right esta activo
		with (select_right) {
			if (isActive) {
				isFocus = true;
				updateTemplate();
				
				lima_pass_event("SELECT_RIGHT");
			}
		}
		
		// Dejar de estar en el foco
		isFocus = false;
		
		updateTemplate()
	}
});

// Que hacer cuando es seleccionado
lima_create_event("SELECT", LIMA_EVTYPE.ONKEY, [keyboard_check_pressed, vk_enter], function() {
	print(name + " a sido presionado!");
});

#endregion