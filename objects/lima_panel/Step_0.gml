// Si no esta activo salir
if (!isActive) exit;

repeat (events.Size(true) ) {
    // Obtener eventos
    var _in = events.Next();    
        
    // Ejecutar evento
    switch (_in.type) {
        // Evento que ejecuta cada ciclo
        case LIMA_EVTYPE.INSELF:
            _in.ev(_in.arg);
            
            break;
            
        case LIMA_EVTYPE.ONCALL:
            var _reset = false;
            if (_in.oncall)  _reset = _in.ev(_in.arg);
            
            // Reiniciar llamado
            if (_reset) _in.oncall = false;
            
            break;
            
        case LIMA_EVTYPE.ONNOT:
            if (!_in.oncall) _in.ev(_in.arg);
            
            break;
			
		case LIMA_EVTYPE.ONKEY:
			if (isFocus) {
				if (alarm[10] == -1) {_in.ev(); }	
			}
			
			break;
    }
}