Occluder = new BulbDynamicOccluder(oRender.Render, x, y, image_xscale, image_yscale, image_angle); /// @is {BulbDynamicOccluder}

var _l = -0.5 * sprite_get_width (sprite_index);
var _t = -0.5 * sprite_get_height(sprite_index);

var _r =  0.5 * sprite_get_width (sprite_index);
var _b =  0.5 * sprite_get_height(sprite_index);

//If this instance has been rotated or stretched in the room editor, add every side as an occluder
//Use clockwise definitions!
Occluder.AddEdge(_l, _t,   _r, _t); //Top
Occluder.AddEdge(_r, _t,   _r, _b); //Right
Occluder.AddEdge(_r, _b,   _l, _b); //Bottom
Occluder.AddEdge(_l, _b,   _l, _t); //Left