event_inherited();

sprite_index = -1;

is = LIMA_COMPONENTS.TEXT;

skb = scribble(text).
starting_format(font, color).
align(halign, valign).
transform(size, size, 0);

typ = scribble_typist();	/// @is {__scribble_class_typist}

#region Metodos
updateTemplate = function() {
	skb.template(selectTemplate() );
}

updateText = function() {
	skb = scribble(text).
	starting_format(font, color).
	align(halign, valign).
	transform(size, size, 0);
}

reorganize = function() {
	updateTemplate();
}

/* @desc Hereda las templates */
Inherit = function(_lima_text) {
	var _s = Templates;
	var _skb = _lima_text.Templates;
	
	// A probarlo nomas perrita
	iter "in", _skb exc
		_s[$ key] = in;
	fin
}

noSelectMove = function() {
	lima_modify_event("SELECT_UP"	, LIMA_EVTYPE._NOTTYPE_);
	lima_modify_event("SELECT_DOWN"	, LIMA_EVTYPE._NOTTYPE_);
	
	lima_modify_event("SELECT_LEFT" , LIMA_EVTYPE._NOTTYPE_);
	lima_modify_event("SELECT_RIGHT", LIMA_EVTYPE._NOTTYPE_);		
}

#endregion

alarm[11] = 1;