is = LIMA_COMPONENTS._PARENT_;
childrens = [];
events    = new Bucket();   /// @is {Bucket} Eventos que puede ejecutar.
listeners = [];     // Datos que tiene que escuchar.

creator = noone;	// Quien lo crea.

// Obtener id del layer
layerID   = layer; // Usar layer simplemente
layerName = layer_get_name(layer);

// Posicion en el cuarto
room_x = room_width  - x;
room_y = room_height - y;

visible = isVisible;

// Indicar a su padre que es su hijo.
lima_send_child(parent);

#region Metodos

LimaStart = function() {}   //  Funcion que llama al iniciar
LimaEnd   = function() {}   //  Funcion que llama al terminar

/** @param visible? */
Disable = function(_visible = false) {
	isActive = false;
	isFocus  = false;
	
	isVisible = _visible;
}

/** @param focus? */
Activate = function(_focus = false) {
	isActive = true;
	isFocus  = _focus;
	
	isVisible = true;	
}

Teleport = function(_x, _y, _relative = false, _childs = true) {
	x = (!_relative) ? _x : x + _x;
	y = (!_relative) ? _y : y + _y;
			
	var i = 0; repeat(array_length(childrens) ) {
		var in = childrens[i++];
		
		in.Teleport(_x, _y, _relative);
	}
}

#endregion

/*
	isVisible: si se dibuja. No deja de procesar funciones.
	isActive : si procesa funciones (continua dibujando normalmente), no puede ejecutar funciones de teclado
	isFocus  : puede procesar funciones de teclado
	
	
*/

// Para debug
if (name == "") {
	name = object_get_name(object_index) + " " + string(irandom(100) );	
}