/// @description [Dibujar Slider]

draw_set_alpha(image_alpha);

// Barra de slider
if (slider_sprite != -1) {
	if (!isStretch) {	// mover un slider
		// Dependiendo del Eje en que se encuentra (no puede ser ambos)
		var _x = x + (!isAxis) ? (sprite_width  * (slider div slider_max) ) : slider_xoffset;
		var _y = y + ( isAxis) ? (sprite_height * (slider div slider_max) ) : slider_yoffset;
	
		draw_set_color(slider_sprite_color);
		draw_sprite(slider_sprite, 0, _x, _y);
	}
	else {	// rellenar una barra
		var _x = x + slider_xoffset + 1;	// Siempre esta 1 a la derecha
		var _y = y + slider_yoffset + 1;	// Siempre esta 1 hacia abajo
		
		var _w = (!isAxis) ? 0 : 0; 
	}
}

// Lineas
if (slider_lines > 0) {
	var _lines = sprite_width div (slider_lines + 1);
	var _x = 0;
	var _y = 0;
	
	repeat (_lines) {
		_x += x + (!isAxis) ? _lines : 0;
		_y += y + ( isAxis) ? _lines : 0;
		
		draw_set_color(slider_lines_color);
		draw_line(_x - slider_xoffset, _y - slider_yoffset, _x + slider_xoffset, _y + slider_yoffset);	
	}
}

draw_set_color(1);