/// @description [SLIDER]
event_inherited();

/*
	slider_sprite: Barra que se mueve al cambiar valores en la barra


*/


expresion = undefined;	// Metodo que utiliza para establecer el valor
setter = undefined;	// De quien tiene que obtener valores y establecerlos

#region Metodos
Slide = function(_value, _type = 0) {
	switch (_type) {
		case 0: slider = clamp(slider + _value, slider_min, slider_max);	break;
		case 1: slider = clip (slider_min, slider_max, slider + _value);	break;
	}
}

#endregion

#region Eventos

// -- Extend defaults
lima_extend_event("SELECT_RIGHT", function() {
	if (canSlide) exit;
	
	if (expresion != undefined) Slide(+step);
});

lima_extend_event("SELECT_LEFT" , function() {
	if (canSlide) exit;	
	
	if (expresion != undefined) Slide(-step);
});

#endregion

