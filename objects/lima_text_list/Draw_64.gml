// Dibujar titulo
event_inherited();

// Si no es visible salir
if (!isVisible) exit;

// Dibujar Lista
var _x = listX;
var _y = listY;

// Columnas
var i = listRowShow; repeat(listRowShowMax) {
    var _row = listSkb[i], _isrow = (i == listIndexRow);

    // Filas
    var j = listColShow; repeat(listColShowMax div 3) {
		var _skb = _row[j];		/// @is {__scribble_class_element}
		var _off = _row[j + 1];	/// Offset
		var _typ = _row[j + 2];	/// @is {__scribble_class_typist }

        var _iscol = (j == listIndexCol);
        
        #region Cambiar template
		if (isFocus) {	// Solo si esta siendo enfocado
	        if (_isrow && _iscol) {
	            _skb.template(skbTemplates.selected);
	            //print("seleccionado!");
	        } 
	        else {
	            _skb.template(skbTemplates.deselected);
	            //print("no seleccionado");
	        }
		}
		
        #endregion
        
        // Dibujar
        _skb.draw(_x + _off.x, _y + _off.y, _typ);
        
        // Aumentar Y
        _y += (listAument + listYoffset);
        
        j += 3;
    }
    
    // Aumentar X
    _x += (listAument + listXoffset);
    _y  = listY;    // Reiniciar Y
    
    ++i;
}


