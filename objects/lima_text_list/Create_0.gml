event_inherited();
/*      
        column  column
    row   0       1
    row   0       1
	
	listRowShowMax: Cuantas filas puede mostrar al mismo tiempo
	listColShowMax: Cuantas columnas puede mostrar al mismo tiempo
*/

// [row][column]
is = LIMA_COMPONENTS.TEXT_LIST;

listSkb = [[]];	// Crear lista de textos [scribble, typist]

// Indice
listIndexRow = 0;
listIndexCol = 0;
// [col][row]

// Desde que indice de columna/fila empezar a mostrar
listColShow = 0;
listRowShow = 0;

#region Reorganizar

#endregion

#region Referencia y posicion del texto
if (listReference) {
	listX ??= 0;
	listY ??= 0;

	listX += x;
	listY += y;
} 
else {
	listX ??= x;
	listY ??= y;
}
#endregion

#region Eventos pre-creados
lima_create_event("CIROW", LIMA_EVTYPE._NOTTYPE_, {}, global.__lima_dummy_function);
lima_create_event("CICOL", LIMA_EVTYPE._NOTTYPE_, {}, global.__lima_dummy_function);

lima_create_event("MOVE_INDEX", LIMA_EVTYPE.INSELF, {u: vk_up, d: vk_down, l: vk_left, r: vk_right}, function(arg) {
	var _col = keyboard_check_pressed(arg.d) - keyboard_check_pressed(arg.u);
	var _row = keyboard_check_pressed(arg.r) - keyboard_check_pressed(arg.l);
	
	listIndexCol = clip(0, listColShowMax - 3, listIndexCol + _col * 3);
	listIndexRow = clip(0, listRowShowMax - 1, listIndexRow + _row);
});

lima_create_event("SELECT_INDEX", LIMA_EVTYPE._NOTTYPE_, {}, global.__lima_dummy_function);

#endregion

#region Metodos
updateText = function() {}

reorganize = function() {
	// Actualizar
	updateText();
	
    var _len = array_length(listTexts);
    
    // Evitar errores
    if ((_len mod listRows) > 0) {array_push(listTexts, undefined); _len++; }
	
    var k = 0;
    var i = 0; repeat(listRows) {  // Rows
        listSkb[i] = [];
        
        var j = 0; repeat(_len div listRows) {  // Columns
			// Revisar si es un atom o no
			var _atom = listTexts [k++], _str, _pos;
			
			if (_atom == undefined) {
				continue;
			}
			// Si es un atom
			else if (is_struct(_atom) ) {
				_str = _atom.item1 ?? "";
				
				var _i2 = _atom.item2;
				if (is_real(_i2) ) {
					_pos = new Vector2(_i2, _i2);
				}
				else {
					_pos = _i2 ?? new Vector2();
				}
			}
			// No
			else {
				_str = _atom;
				_pos = new Vector2();
			}

			// Esto permite remplazar a los anteriores
			listSkb[i][j++]	= scribble(_str).template(skbTemplates.active);
			listSkb[i][j++] = _pos;					// Poisicion
			listSkb[i][j++]	= scribble_typist();	// Typist
        }
        
        if (!listColShowMax) listColShowMax = max(listColShowMax, array_length(listSkb[i] ) );
        
        ++i;
    }
    
    if (!listRowShowMax) listRowShowMax = max(listRowShowMax, array_length(listSkb) );
}

#endregion

// Para cambiar cosas en el instance creation (room)
alarm[11] = 1;