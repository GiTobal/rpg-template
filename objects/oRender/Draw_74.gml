if (mouse_check_button_pressed(mb_left) ) {
    with(instance_create_layer(mouse_x, mouse_y, layer, oLight) ) {
        var _random = irandom_range(2, 8);
        
        Light.xscale = _random;
        Light.yscale = _random;
    }
}