/// @desc Variables
event_inherited();

if (Scaled) {
    TweenFire("~", EaseOutCubic, "^1", "#patrol", "$", 5, "Light.xscale>", ScaleTo, "Light.yscale>", ScaleTo);     
}