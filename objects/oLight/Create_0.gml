/// @desc Variables
blendCycleSpeed = random_range(0.1, 1);
blendCycle = random(255);

Light = new BulbLight(RenderID, Mask, MaskIndex, x, y);    /// @is {BulbLight}

Light.xscale = Scale;
Light.yscale = Scale;

Light.bitmask = BitMask;
Light.alpha   = image_alpha;
Light.blend   = image_blend;

Light.castShadows = Shadows;
Light.visible     = Visible;

Light.penumbraSize = 30;