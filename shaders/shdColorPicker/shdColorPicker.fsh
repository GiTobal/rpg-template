varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main() {
    float sat = dot(v_vColour.rgb, vec3(.3, .59, .11) );
    vec3 f    = mix(vec3(sat, sat, sat), texture2D(gm_BaseTexture, v_vTexcoord).rgb, v_vTexcoord.r) * (1.0 - v_vTexcoord.g);
    
    gl_FragColor = vec4(f, 1.0);
}