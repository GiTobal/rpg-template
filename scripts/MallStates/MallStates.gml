enum MSTATE {DEF, _SIZE_}

/// @desc Donde se guardan las propiedades de los estados
function MallState(_key) : MallComponent(_key) constructor {
    __init    = mall_data(false, MN.B);  // Valor inicial
    __compare = mall_data(false, MN.B);  // Con que valor comparar
    
    __resists = {}; // Que estadistica resiste a este estado. Se usa un struct ya que es más rapido para buscar.
    __action  = {}; // Que accion afecta este estado      
    
    __prop = (new Counter(0, 0) ).ToggleIterate();  /// @is {Counter} Usar como ciclo
    __methodStart  = MALL_DUMMY_METHOD;  // Funcion a usar cuando se inicia el estado
    __methodUpdate = MALL_DUMMY_METHOD;  // Funcion a usar cuando se actualiza el estado
    __methodEnd    = MALL_DUMMY_METHOD;  // Funcion a usar cuando se finaliza el estado
    
    // Mensajes
    __propKey = [noone, noone, noone]; // Llaves para traducir
    
    __propStart  = "";  // Cuando inicia el estado
    __propUpdate = "";  // Cuando se actualiza el estado 
    __propEnd    = "";  // Cuando se termina el estado
    
    #region Metodos
    /// @param key
    /// @param ...
    /// @returns {MallState}
    static SetResists  = function() {
        // Evitar indefenidos
        if (is_undefined(argument0) ) return self;
        
        if (!is_array(argument0) ) {
            var i = 0; repeat(argument_count) {
                var _key = argument[i++];
                
                __resists[$ _key] = true;
            }
            
        } else { // Soporte array
            var _keys = argument0;
            
            var i = 0; repeat(array_length(_keys) ) {
                var _key = _keys[i++];
                
                __resists[$ _key] = true;
            }            
            
        }
        
        return self;
    }
    
    /// @param stat/part/action
    /// @param value
    /// @param number_type
    /// @param ...
    /// @returns {MallState}
    static SetAffected = function(_k, _v = 0, _t = 0) {
        var _inside, _class, _temp, _type, _value;

        // -- Comprobar si es una parte o estadistica
        var i = 0; repeat (argument_count div 3) {
            _inside = argument[i++];    /// @is {MallComponent}
            
            _temp = argument[i++];
            _type = argument[i++];
            
            _value  = mall_data(_temp, _type);  

            if (!is_struct(_inside) ) {
                var _statsGroup = MALL_GROUP.__stats;
                var _partsGroup = MALL_GROUP.__parts;                
                
                var _key = _inside;
                
                if (variable_struct_exists(_statsGroup, _key) ) {           // Es una estadistica
                    _class = _statsGroup[$ _key];    
            
                } else if (variable_struct_exists(_partsGroup, _key) ) {    // Es una parte
                    _class = _partsGroup[$ _key];
                    
                } else { 
                    // Las acciones se guardan en el estado
                    var _action = MALL_STORAGE.__actionsMaster;
                    
                    // Guardar y seguir iterando
                    if (_action.Exists(_key) ) {
                        __action[$ _key] = _value;
                        
                        continue;
                    }
                }
                
            } else _class = _inside;
        
            _class.AffectedBy(__key, _value);
        }

        return self;
    }
    
    /// @param mensaje_start
    /// @param mensaje_update
    /// @param mensaje_end
    static PropMensajes = function(_start, _update, _end) {
        __propStart  =  _start;
        __propUpdate = _update;
        __propEnd    =    _end;
            
        return self;    
    }
    
    /// @param start_value
    /// @param {number} end_value
    /// @param {number} aument_value
    /// @param {number} iterate_times
    /// @param method_start
    /// @param method_update
    /// @param method_end
    static PropCounter  = function(_start, _end, _aument, _iterateTimes, _methodStart, _methodUpdate, _methodEnd) {
        // Es para siempre o hasta que se cure u.u
        if (is_undefined(_start) ) {
            __prop = noone;
            return self;
        }
        
        __prop.__min = _start; // Valor del efecto inicial
        __prop.__max = _end;   // Valor al final del ciclo    
        
        __prop.__amount = _aument; // Cuanto aumentar el valor del efecto
        __prop.__iterateTime = max(_iterateTimes, 0);  // Cuantos ciclos estará activo (que no sea 0)
        
        // -- Metodos a usar
        __methodStart  = _methodStart  ?? __methodStart;    // Que no sea undefined 
        __methodUpdate = _methodUpdate ?? __methodUpdate;   // Que no sea undefined
        
        __methodEnd = _methodEnd ?? __methodEnd;    // Que no sea undefined
        
        return self;
    }

    #endregion
}

function MallStateProp(_key) constructor {
    __name = _key;
    __count = (new Counter(0, 0) ).ToggleIterate(); /// @is {Counter}
    
    // -- Metodos
    __mStart  = MALL_DUMMY_METHOD;
    __mUpdate = MALL_DUMMY_METHOD;
    __mEnd = MALL_DUMMY_METHOD;
    
    __logKeys = [noone, noone, noone];
    __logStart  = "";
    __logUpdate = "";
    __logEnd = "";
}

/**
	@desc	Crea un estado y agrega una estadistica al Storage en base al nuevo estado 
			utiliza prefijos para crear las estadisticas relacionadas
	@param	{String} Key
	@param	{String} [StatPrefix...]
*/
function mall_create_states(_key) {
    var _states = MALL_STORAGE.__statesMaster;
    var _prefix = [];
    
    var i = 1; repeat(argument_count - 1) {
        var _in = argument[i++];    // Obtener prefijos de Stat
        
        // Guardar los prefijos de stats
        array_push(_prefix, _in);
        
        // Crea las estadisticas
        mall_create_stats(_key + _in);
    }
    
    // Establecer estado con los prefijos de stat
    if (array_empty(_prefix) ) _prefix[0] = undefined;
    
    _states.Set(_key, _prefix);
}

/// @param {array} delete_array*
function mall_state_init(_deleteArray) {
    MALL_GROUP.InitStates(_deleteArray);    
}

/// @returns {array}
function mall_get_states() {
    return (MALL_STORAGE.__statesMaster).GetKeys();
}

/// @param state_key
function mall_get_state(_key) {
    return (MALL_GROUP.__states[$ _key] );
}

/// @param state_key
/// Obtiene el estado que se guarda en el master, este posee los prefijos.
function mall_state_master(_key) {
    return (MALL_STORAGE.__statesMaster).Get(_key);
}

/// @param state_key
/// @param start_value
/// @param number_type*
/// @param {array} more_defense_stat*
/// @returns {MallState}
function mall_state_customize(_key, _start, _type = MN.B, _moreStat) {
    var _state = mall_get_state(_key);
    
    _state.__init    = mall_data(_start, _type); // Establecer el valor  
    _state.__compare = mall_data(_start, _type); // Con que valor comparar.
    
    // Para no errores MSTATE.DEF debe ser 0!
    var _prefix = mall_state_master(_key)[MSTATE.DEF];
    
    // Obtener bien el nombre de la estadistica default.
    if (!is_undefined(_prefix) ) _prefix = _key + _prefix;
    
    // Estadistica default que lo defiende [0]
    _state.SetResists(_prefix);
    
    // Más estadisticas que lo defienden
    _state.SetResists(_moreStat);
    
    return (_state);
}



