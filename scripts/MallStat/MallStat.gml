#macro statfun function(lvl, stat, extra) // stat es un struct.
#macro MALL_STAT_SET 1 // 0: value 1: round(x) 2: floor(x)

/* stat in Party:
    solo: _stat.__lvlSingle,    // Si sube de nivel individualmente
    lvl :  1,   // Nivel de la estadistica si se usa individualmente
    base: -1,   // Base se define despeus (undefined: devuelve el valor inicial)
    
    upper : _isData ? _start.Copy() : _start + 1, // Valor de la estadistica sin ningun cambio Evitar 0 para que exp funcione de panuca
    actual: _isData ? _start.Copy() : _start, // Valor de la estadistica que va cambiando con el tiempo (Unico que se puede cambiar)
    last  : _isData ? _start.Copy() : _start, // Valor anterior de subir de nivel    
    
    condition: undefined // Condicion que debe cumplir para subir de nivel
*/
/// @desc Donde se guardan las propiedades de una estadistica
function MallStat(_key) : MallComponent(_key) constructor {
    // -- Lider y súbdito
    __leader    = undefined;
    __leaderKey = "";
    
    __minion = [];
    __affectedBy = {};
    
    __visualize = MALL_DUMMY_METHOD;	// Como muestra sus valores
    
    ///////////////////////////////////////////////////
    /// @type {Array.Real}
	__init  = mall_data(0, MN.R);
	/// @type {Array.Real}
    __limit = [0, 0];
    
    // No hay cambios
    __lvlMethod = MALL_DUMMY_METHOD;
    
    __lvlLimit  = [0, 0];   // Nivel minimo y maximo
    __lvlSingle =  false;   // Si sube de nivel aparte de otras estadisticas con su propia experencia etc
    
    __toMin = new Counter(0, 1, false, false);
    __toMax = new Counter(0, 1, false, false);
   
    __toMaxLevel = false;	// Luego de subir de nivel la primera vez dejar con el valor maximo
    
    #region Metodos
	/**
		@param _stat MallStat o String
		@param {bool} [_inh_limit] Copiar el limite de valores
		@param {bool} [_inh_lvl] Copiar configuracion de niveles
		@param {bool} [_inh_vis] Copiar forma de visualizacion
		@desc Copia el limite, valor inicial y formula de otro MallStat (String o MallStat)
	*/
    static Inherit = function(_stat, _inh_limit = true, _inh_lvl = true, _inh_vis = false) {
    	/// @type {Struct.MallStruct}
		_stat = is_string(_stat) ? mall_get_stat(_stat) : _stat;
		
		// Copiar valor inicial
		array_copy(__init, 0, _stat.__init, 0, MDATA._SIZE_);
		
        // Si se hereda el limite de valor
        if (_inh_limit) array_copy(__limit, 0, _stat.__limit, 0, 2);
		
		// Si se hereda lo relacionado al nivel (method y limite)
		if (_inh_lvl) {
	        __lvlMethod = method(undefined, _stat.__lvlMethod);	// Copiar formula para subir de nivel
			array_copy(__lvlLimit, 0, _stat.__lvlLimit, 0, 2);	// Copiar limite de nivel
		}
        
        if (_inh_vis) __visualize = _stat.__visualize;
        
        return self;
    }
    
    /// @param {MallStat} mall_stat
    static SetLeader = function(_mallStat) {
        // Agregar a los hijos del otro MallStat
        array_push(_mallStat.__minion, __key);
        
        __leader     = weak_ref_create(_mallStat); // Referencia al lider  
        __leaderKey  = _mallStat.__key;            // Nombre del lider    
        __affectedBy = snap_deep_copy(_mallStat.__affectedBy); // Copiar que estadisticas lo afecta
        
        var _limit = _mallStat.__limit;
        SetLimit(_limit[0], _limit[1] );
		
		// --- LVL
        __lvlMethod = undefined;	// eliminar funcion ya que ahora sube de nivel de acuerdo al lider.
        __lvlSingle =	  false;	// tiene el mismo nivel que su maestro.	
        
        var _limit = _mallStat.__lvlLimit;
		SetLevelLimit(_limit[0], _limit[1] );

        return self;
    }
    
	/** @param {Function} _method */
    static SetVisualize = function(_method) {
    	__visualize = _method;
    	return self;
    }
    
    /// @param min
    /// @param max
    // Solo numeros
    static SetLimit = function(_min, _max) {
    	__limit[0] = _min;
    	__limit[1] = _max;
		
        return self;
    }
    
    /// @param {Function} _method
    /// @param {Real} _min
    /// @param {Real} _max
    /// @param {Bool} _single
    static SetLevel = function(_method, _min = 0, _max = 100, _single = false) {
    	// Si ya existe un lider no usar
    	if (__leader != undefined) return self;
    	
    	__lvlMethod = method(undefined, _method);	// Scope local
        __lvlLimit  = [_min, _max];
        __lvlSingle = _single;
        
        return self;
    }
    
    /// @param {Real} _min
    /// @param {Real} _max
    static SetLevelLimit = function(_min, _max) {
    	__lvlLimit[0] = _min;
    	__lvlLimit[1] = _max;
    	
    	return self;
    }
    
    /// @param repeat
    /// @param times
    // Al subir de nivel deja esta estadistica en su menor valor
    static ToMin = function(_times = 1, _repeat = true) {
		// Desactiva toMax
		__toMax.__active = false;
	
		__toMin.SetActive(true, true);
		__toMin.__repeat = _repeat;
		__toMin.__max = _times;

        return self;
    }
    
    /// @param repeat
    /// @param times  
    /// Al subir de nivel deja esta estadistica en su nivel mayor
    static ToMax = function(_times = 1, _repeat = true) {
        // Desactiva toMin
        __toMin.__active = false;
        
        __toMax.__active = true;
        __toMax.__repeat = _repeat;
        __toMax.__max = _times;

        return self;
    }
    
    /// @param state_key
    /// @param value
    static AffectedBy  = function(_stateKey, _value) {
    	__affectedBy[$ _stateKey] = _value;
    	
        return self;
    }
    
    /// @param state_key
    static GetAffected = function(_stateKey) {
        return (__affectedBy[$ _stateKey] );    
    }

    #endregion
}

/// @param {string} key
/// @param ...
/// @desc Crear un (o varios) stat globalmente
function mall_create_stats() {
    var _stats = MALL_STORAGE.__statsMaster;
    
    var i = 0; repeat(argument_count) {
        var _key = argument[i++];
        
        _stats.Set(_key); // Se crea Stat
    }    
}

/** @return {Array.String} */
function mall_get_stats() {
	/// @type {Struct.MallStorage}
	var _sex = MallStorage();
		
	return ( (MALL_STORAGE.__statsMaster ).GetKeys() );
}

/// @param stat_key
/// @param set_group*
/// @returns {MallStat}
/// Regresa una configuracion de estadistica del grupo actual (No uso actual)


/**
@param {String} _key
@return {Struct.MallStat}
*/
function mall_get_stat(_key) {
	return (MALL_GROUP.__stats[$ _key] );
}

/// @param delete_array*
/// Inicia las estadisticas en el grupo para luego configurarlas a gustoS
function mall_stat_init(_deleteArray) {
	MALL_GROUP.InitStats(_deleteArray);
}

/// @param stat_key
/// @param start/stat_key
/// @param limit_min
/// @param limit_max
/// @param number_type*
/// @param displayKey*
/// @returns {MallStat}  Permite configurar una estadistica a gusto en un grupo
function mall_stat_customize(_key, _start = 0, _min = 0, _max = 9999, _type = 0, _disKey) {
    var _stat = mall_get_stat(_key); /// @is {MallStat}
	
	if (!is_string(_start) ) {
		_stat.__init = mall_data(_start, _type);   

		// Establecer limite si no se establecio el maestro
		_stat.SetLimit(_min, _max);
		
	} else {
		// heredar rapidamente
		_stat.Inherit(_start, , , true);
	}

    return (_stat).SetTranslate(_disKey);
}

/// @param {Real} _x
function __mall_stat_rounding(_x) {
	switch (MALL_STAT_SET) {	
		case 0: return _x;			break;
		case 1: return round(_x);	break;
		case 2: return floor(_x);	break;
		
		default: return _x; break;
	}	
}

// Devuelve una estructura con cada estadistica con un valor 0
function __mall_stat_struct() {
	var _statKeys = mall_get_stats();
	var _struct = {};
	
	var i = 0; repeat(array_length(_statKeys) ) {
		var _key = _statKeys[i++];
		
		_struct[$ _key] = 0;
	}
	
	return _struct;
}
