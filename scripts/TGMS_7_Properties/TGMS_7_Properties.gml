//============================
// Property Modifiers
//============================
function TPCeil(property)			{ return {data:[TPCeil, property, undefined]}; }
function TPFloor(property)			{ return {data:[TPFloor, property, undefined]}; }
function TPRound(property)			{ return {data:[TPRound, property, undefined]}; }
function TPShake(property, amount)	{ return {data:[TPShake, property, amount, undefined]}; }
function TPSnap(property, snap)		{ return {data:[TPSnap, property, snap, undefined]}; }

//============================
// Data Structure Properties
//============================
function TPArray(array, index)  { return {data:[TPArray, array, index]}; }
function TPGrid(grid, x, y)		{ return {data:[TPGrid, grid, x, y]}; }
function TPList(list, index)	{ return {data:[TPList, list, index]}; }
function TPMap(map, key)		{ return {data:[TPMap, map, key]}; }

//============================
// Special Properties
//============================
function TPTarget(target, name) { return {data:[TPTarget, is_real(target) ? target : weak_ref_create(target), name]}; }
function TPCol(property)		{ return {data:[TPCol, property,undefined,undefined,undefined]}; }
function TPPath(path, absolute) { return {data:[TPPath, path, absolute]}; }
function TPPathExt(path, x, y)	{ return {data:[TPPath, path, x, y]}; }
function TPUser(user_event)		
{ 
	if (argument_count == 1) { return {data:[TPUser, user_event, undefined]}; }
	if (argument_count == 2) { return {data:[TPUser, user_event, argument[1]]}; }
	static i = -1; i = -1; 
	static args = 0; args = array_create(argument_count-1);
	repeat(argument_count-1) { ++i; args[i] = argument[i+1]; }
	return {data:[TPUser, user_event, args]}; 
}

//============================
// PROPERTY SETTER FUNCTIONS
//============================

function TPFunc(target, name, setter)
{	/// @func TPFunc(target, name, setter, [getter])
	static STR_DOLLAR = "$";
	static STR_AMPERSAND = "&";
	static _assignName = "";
	setter = method(undefined, setter);
	
	// Don't think this is needed... users can use wildcard "*" directly
	//if (is_real(name) || name == "") { name = "*" }
	
	// Check for "<" or ">" to/from and strip it for variable name assignment
	if (string_byte_at(name, string_length(name)) == 60 || string_byte_at(name, string_length(name)) == 62)
	{
		_assignName = string_delete(name, string_length(name), 1);
	}
	else
	{
		_assignName = name;
	}
	
	// Connect setter method
	target[$ STR_DOLLAR+_assignName] = setter;
	
	// Connect getter method
	if (argument_count == 4 && argument[3] != undefined)
	{
		target[$ STR_AMPERSAND+_assignName] = method(undefined, argument[3]);
	}
	
	// This allows us to place this directly into a tween function if desired
	return name;
}

function TPFuncShared(name, setter) 
{	/// @func TPFuncShared(name, setter, [getter])

	// Make sure system is already started
	TGMS_Begin();

	// Associate shared variable name with a setter function for all targets
	global.TGMS.PropertySetters[? name] = method(undefined, setter);

	if (argument_count == 3 && argument[2] != undefined)
	{
		global.TGMS.PropertyGetters[? name] = method(undefined, argument[2]);
	}
}

function TPFuncSharedNormal(name, setter) 
{	/// @func TPFuncSharedNormal(name, setter, [getter])
	/*
		Normalized property scripts receive an eased value between 0 and 1
		with additional data passed for the start/dest values.
	*/
	
	if (argument_count == 2)
	{
		TPFuncShared(name, setter);	
	}
	else
	{
		TPFuncShared(name, setter, argument[2]);	
	}

	// Mark as a shared normalized property
	global.TGMS.PropertyNormals[? name] = 1;
}


function TGMS_7_Properties()
{
	if (variable_struct_exists(global.TGMS, "init_Properties"))
	{
		return;
	}
	else
	{
		global.TGMS[$ "init_Properties"] = true;
	}
	
	// DEFAULT INSTANCE PROPERTIES
	TPFuncShared("undefined",			function(){},							function(){return 0;});
	TPFuncShared("x",					function(v,t){t.x=v;},					function(t){return t.x;});
	TPFuncShared("y",					function(v,t){t.y=v;},					function(t){return t.y;});
	TPFuncShared("z",					function(v,t){t.z=v;},					function(t){return t.z;});
	TPFuncShared("direction",			function(v,t){t.direction=v;},			function(t){return t.direction;});
	TPFuncShared("speed",				function(v,t){t.speed=v;},				function(t){return t.speed;});
	TPFuncShared("hspeed",				function(v,t){t.hspeed=v;},				function(t){return t.hspeed;});
	TPFuncShared("vspeed",				function(v,t){t.vspeed=v;},				function(t){return t.vspeed;});
	TPFuncShared("image_angle",			function(v,t){t.image_angle=v;},		function(t){return t.image_angle;});
	TPFuncShared("image_alpha",			function(v,t){t.image_alpha=v;},		function(t){return t.image_alpha;});
	TPFuncShared("image_speed",			function(v,t){t.image_speed=v;},		function(t){return t.image_speed;});
	TPFuncShared("image_index",			function(v,t){t.image_index=v;},		function(t){return t.image_index;});
	TPFuncShared("image_xscale",		function(v,t){t.image_xscale=v;},		function(t){return t.image_xscale;});
	TPFuncShared("image_yscale",		function(v,t){t.image_yscale=v;},		function(t){return t.image_yscale;});
	TPFuncShared("image_scale",			function(v,t){t.image_xscale=v;t.image_yscale=v;},			function(t){return t.image_xscale;});
	TPFuncSharedNormal("image_blend",	function(v,t,d){t.image_blend=merge_colour(d[0],d[1],v);},	function(t){return t.image_blend;});
	TPFuncShared("path_position",		function(v,t){t.path_position=v;},		function(t){return t.path_position;});
	TPFuncShared("path_scale",			function(v,t){t.path_scale=v;},			function(t){return t.path_scale;});
	TPFuncShared("path_speed",			function(v,t){t.path_speed=v;},			function(t){return t.path_speed;});
	TPFuncShared("path_orientation",	function(v,t){t.path_orientation=v;},	function(t){return t.path_orientation;});
	TPFuncShared("depth",				function(v,t){t.depth=v;},				function(t){return t.depth;});
	TPFuncShared("friction",			function(v,t){t.friction=v;},			function(t){return t.friction;});
	TPFuncShared("gravity",				function(v,t){t.gravity=v;},			function(t){return t.gravity;});
	TPFuncShared("gravity_direction",	function(v,t){t.gravity_direction=v;},	function(t){return t.gravity_direction;});

	// Handle Built-in global variables as GameMaker doesn't seem to be recognising them as global values :(
	// Need this for the "Getters" to work right
	TPFuncShared("mouse_x", function(){},function(){return mouse_x;});
	TPFuncShared("mouse_y", function(){},function(){return mouse_y;});
	TPFuncShared("room_width", function(v){room_width=v;}, function(){return room_width;});
	TPFuncShared("room_height", function(v){room_height=v;}, function(){return room_height;});
	// Legacy Support
	TPFuncShared("health!", function(v){health=v;}, function(){return health;});
	TPFuncShared("score!", function(v){score=v;}, function(){return score;});
	

	#region Implement Property Modifiers
	
	TPFuncShared(TPCeil, function(value, target, data, tween)
	{	/*
			Example:
				TweenFire("$", 60, TPCeil("x"), x, mouse_x)
		*/
		if (is_undefined(data[1]))
		{
			var _prop = data[0];
			if (is_array(_prop)) // ?
			{
				_prop[@ 1] = 1;
			}
			else // Property Setter Built
			if (ds_map_exists(global.TGMS.PropertySetters, _prop))
			{
				data[@ 1] = 2;
				data[@ 0] = global.TGMS.PropertySetters[? _prop];
			}
			else
			if (target[$ _prop] != undefined) // Assume instance/struct
			{
				data[@ 1] = 3;
			}
			else // Assume string property
			{	// global
				if (string_pos("global.", _prop))
				{
					data[@ 1] = 4;
					data[@ 0] = string_replace(_prop, "global.", "");
				}
				else // self
				if (string_pos("self.", _prop))
				{
					data[@ 1] = 5;
					data[@ 0] = string_replace(_prop, "self.", "");
				}
				else // other
				if (string_pos("other.", _prop))
				{
					data[@ 1] = 6;
					data[@ 0] = string_replace(_prop, "other.", "");
				}
				else // TODO: More advanced handling
				{
					show_error("Advanced dot notation is not yet supported by this function!", false);
				}
			}
		}
			
		switch(data[1])
		{
		case 1:
			var _array = data[0];
			var _pass_data;
			var _length = array_length(_array)-1;
	
			if (_length == 1)
			{	
				_pass_data = _array[1];
			}
			else
			{
				_pass_data = array_create(_length);
				array_copy(_pass_data, 0, _array, 1, _length);
			}
	
			var _script = _array[0];
			_script(ceil(value), target, _pass_data, tween);
		break;
		case 2: data[0](ceil(value), target);					break;
		case 3: target[$ data[0]] = ceil(value);				break;
		case 4: variable_global_set(data[0], ceil(value));		break;
		case 5: tween[TWEEN.CALLER][$ data[0]] = ceil(value);	break;
		case 6: tween[TWEEN.OTHER][$ data[0]] = ceil(value);	break;
		}
	});

	TPFuncShared(TPFloor, function(value, target, data, tween) 
	{	
		/*
			Example:
				TweenFire("$", 60, TPFloor("x"), x, mouse_x)
		*/
		if (is_undefined(data[1]))
		{
			var _prop = data[0];
			if (is_array(_prop)) // ?
			{
				_prop[@ 1] = 1;
			}
			else // Property Setter Built
			if (ds_map_exists(global.TGMS.PropertySetters, _prop))
			{
				data[@ 1] = 2;
				data[@ 0] = global.TGMS.PropertySetters[? _prop];
			}
			else
			if (target[$ _prop] != undefined) // Assume instance/struct
			{
				data[@ 1] = 3;
			}
			else // Assume string property
			{	// global
				if (string_pos("global.", _prop))
				{
					data[@ 1] = 4;
					data[@ 0] = string_replace(_prop, "global.", "");
				}
				else // self
				if (string_pos("self.", _prop))
				{
					data[@ 1] = 5;
					data[@ 0] = string_replace(_prop, "self.", "");
				}
				else // other
				if (string_pos("other.", _prop))
				{
					data[@ 1] = 6;
					data[@ 0] = string_replace(_prop, "other.", "");
				}
				else // TODO: More advanced handling
				{
					show_error("Advanced dot notation is not yet supported by this function!", false);
				}
			}
		}
			
		switch(data[1])
		{
		case 1:
			var _array = data[0];
			var _pass_data;
			var _length = array_length(_array)-1;
	
			if (_length == 1)
			{	
				_pass_data = _array[1];
			}
			else
			{
				_pass_data = array_create(_length);
				array_copy(_pass_data, 0, _array, 1, _length);
			}
	
			var _script = _array[0];
			_script(floor(value), target, _pass_data, tween);
		break;
		case 2: data[0](floor(value), target);					break;
		case 3: target[$ data[0]] = floor(value);				break;
		case 4: variable_global_set(data[0], floor(value));		break;
		case 5: tween[TWEEN.CALLER][$ data[0]] = floor(value);	break;
		case 6: tween[TWEEN.OTHER][$ data[0]] = floor(value);	break;
		}
	});

	// TPRound()
	TPFuncShared(TPRound, function(value, target, data, tween) 
	{	
		/*
			Example:
				TweenFire("$", 60, TPRound("x"), x, mouse_x)
		*/
		if (is_undefined(data[1]))
		{	
			var _prop = data[0];
			if (is_array(_prop)) // ?
			{
				_prop[@ 1] = 1;
			}
			else // Property Setter Built
			if (ds_map_exists(global.TGMS.PropertySetters, _prop))
			{
				data[@ 1] = 2;
				data[@ 0] = global.TGMS.PropertySetters[? _prop];
			}
			else
			if (target[$ _prop] != undefined) // Assume instance/struct
			{
				data[@ 1] = 3;
			}
			else // Assume string property
			{	// global
				if (string_pos("global.", _prop))
				{
					data[@ 1] = 4;
					data[@ 0] = string_replace(_prop, "global.", "");
				}
				else // self
				if (string_pos("self.", _prop))
				{
					data[@ 1] = 5;
					data[@ 0] = string_replace(_prop, "self.", "");
				}
				else // other
				if (string_pos("other.", _prop))
				{
					data[@ 1] = 6;
					data[@ 0] = string_replace(_prop, "other.", "");
				}
				else // TODO: More advanced handling
				{
					show_error("Advanced dot notation is not yet supported by this function!", false);
				}
			}
		}
			
		switch(data[1])
		{
		case 1:
			var _array = data[0];
			var _pass_data;
			var _length = array_length(_array)-1;
	
			if (_length == 1)
			{	
				_pass_data = _array[1];
			}
			else
			{
				_pass_data = array_create(_length);
				array_copy(_pass_data, 0, _array, 1, _length);
			}
	
			var _script = _array[0];
			_script(round(value), target, _pass_data, tween);
		break;
		case 2: data[0](round(value), target);					break;
		case 3: target[$ data[0]] = round(value);				break;
		case 4: variable_global_set(data[0], round(value));		break;
		case 5: tween[TWEEN.CALLER][$ data[0]] = round(value);	break;
		case 6: tween[TWEEN.OTHER][$ data[0]] = round(value);	break;
		}
	});
	
	TPFuncShared(TPShake, function(value, target, data, tween) 
	{	
		/*
			Example:
				TweenFire("$", 60, TPShake("x", 25), x, mouse_x)
		*/

		if (is_undefined(data[2]))
		{
			var _prop = data[0];
			
			if (is_array(_prop)) // ?
			{
				_prop[@ 2] = 1;
			}
			else // Property Setter Built
			if (ds_map_exists(global.TGMS.PropertySetters, _prop))
			{
				data[@ 2] = 2;
				data[@ 0] = global.TGMS.PropertySetters[? _prop];
			}
			else
			if (target[$ _prop] != undefined) // Assume instance/struct
			{
				data[@ 2] = 3;
			}
			else // Assume string property
			{	// global
				if (string_pos("global.", _prop))
				{
					data[@ 2] = 4;
					data[@ 0] = string_replace(_prop, "global.", "");
				}
				else // self
				if (string_pos("self.", _prop))
				{
					data[@ 2] = 5;
					data[@ 0] = string_replace(_prop, "self.", "");
				}
				else // other
				if (string_pos("other.", _prop))
				{
					data[@ 2] = 6;
					data[@ 0] = string_replace(_prop, "other.", "");
				}
				else // TODO: More advanced handling
				{
					show_error("Advanced dot notation is not yet supported by this function!", false);
				}
			}
		}
			
		// "SHAKE" the final value
		if (tween[TWEEN.TIME] > 0 && tween[TWEEN.TIME] < tween[TWEEN.DURATION])
		{
			value += random_range(-data[1], data[1]);
		}
			
		switch(data[2])
		{
		case 1:
			var _array = data[0];
			var _pass_data;
			var _length = array_length(_array)-1;
	
			if (_length == 1)
			{	
				_pass_data = _array[1];
			}
			else
			{
				_pass_data = array_create(_length);
				array_copy(_pass_data, 0, _array, 1, _length);
			}
	
			var _script = _array[0];
			_script(value, target, _pass_data, tween);
		break;
		case 2: data[0](value, target);					break;
		case 3: target[$ data[0]] = value;				break;
		case 4: variable_global_set(data[0], value);	break;
		case 5: tween[TWEEN.CALLER][$ data[0]] = value; break;
		case 6: tween[TWEEN.OTHER][$ data[0]] = value;	break;
		}
	});

	TPFuncShared(TPSnap, function(value, target, data, tween) 
	{	
		/*
			Example:
				TweenFire("$", 60, TPSnap("x", 32), x, mouse_x)
		*/

		if (is_undefined(data[2]))
		{
			var _prop = data[0];
			if (is_array(_prop)) // ?
			{
				_prop[@ 2] = 1;
			}
			else // Property Setter Built
			if (ds_map_exists(global.TGMS.PropertySetters, _prop))
			{
				data[@ 2] = 2;
				data[@ 0] = global.TGMS.PropertySetters[? _prop];
			}
			else
			if (target[$ _prop] != undefined) // Assume instance/struct
			{
				data[@ 2] = 3;
			}
			else // Assume string property
			{	// global
				if (string_pos("global.", _prop))
				{
					data[@ 2] = 4;
					data[@ 0] = string_replace(_prop, "global.", "");
				}
				else // self
				if (string_pos("self.", _prop))
				{
					data[@ 2] = 5;
					data[@ 0] = string_replace(_prop, "self.", "");
				}
				else // other
				if (string_pos("other.", _prop))
				{
					data[@ 2] = 6;
					data[@ 0] = string_replace(_prop, "other.", "");
				}
				else // TODO: More advanced handling
				{
					show_error("Advanced dot notation is not yet supported by this function!", false);
				}
			}
		}
			
		// "SNAP" the final value
		//value -= value % data[1];
		value = 10000 * value div (10000*data[1]) * (10000*data[1]) / 10000; // This can be more accurate than modulus
			
		switch(data[2])
		{
		case 1:
			var _array = data[0];
			var _pass_data;
			var _length = array_length(_array)-1;
	
			if (_length == 1)
			{	
				_pass_data = _array[1];
			}
			else
			{
				_pass_data = array_create(_length);
				array_copy(_pass_data, 0, _array, 1, _length);
			}
	
			var _script = _array[0];
			_script(value, target, _pass_data, tween);
		break;
		case 2: data[0](value, target);					break;
		case 3: target[$ data[0]] = value;				break;
		case 4: variable_global_set(data[0], value);	break;
		case 5: tween[TWEEN.CALLER][$ data[0]] = value; break;
		case 6: tween[TWEEN.OTHER][$ data[0]] = value;	break;
		}
	});
	#endregion
	
	#region Implement Data Structure Properties
	
	TPFuncShared(TPArray, 
	function(value, target, data) 
	{
		if (is_string(data[0])) 
		{ 
			data[@ 0] = target[$ data[0]] != undefined ? target[$ data[0]] : variable_global_get(data[0]);
		}
		
		array_set(data[0], data[1], value);
	}, 
	function(target, data) 
	{
		if (is_string(data[0])) 
		{ 
			data[@ 0] = target[$ data[0]] != undefined ? target[$ data[0]] : variable_global_get(data[0]);
		}
		
		return data[0][data[1]]; 
	});

	TPFuncShared(TPList, 
	function(value, target, data) 
	{
		if (is_string(data[0]))
		{
			data[@ 0] = target[$ data[0]] != undefined ? target[$ data[0]] : variable_global_get(data[0]);
		}
		
		ds_list_replace(data[0], data[1], value);
	}, 
	function(target, data) 
	{
		if (is_string(data[0]))
		{
			data[@ 0] = target[$ data[0]] != undefined ? target[$ data[0]] : variable_global_get(data[0]);
		}
	
		return ds_list_find_value(data[0], data[1]);
	});

	TPFuncShared(TPGrid, 
	function(value, target, data) 
	{
		if (is_string(data[0]))
		{
			data[@ 0] = target[$ data[0]] != undefined ? target[$ data[0]] : variable_global_get(data[0]);
		}
		
		ds_grid_set(data[0], data[1], data[2], value);
	}, 
	function(target, data) 
	{
		if (is_string(data[0]))
		{
			data[@ 0] = target[$ data[0]] != undefined ? target[$ data[0]] : variable_global_get(data[0]);
		}
		
		return ds_grid_get(data[0], data[1], data[2]);
	});

	TPFuncShared(TPMap, 
	function(value, target, data) 
	{
		if (is_string(data[0]))
		{
			data[@ 0] = target[$ data[0]] != undefined ? target[$ data[0]] : variable_global_get(data[0]);
		}
		
		ds_map_replace(data[0], data[1], value);
	},
	function(target, data) 
	{
		if (is_string(data[0]))
		{
			data[@ 0] = target[$ data[0]] != undefined ? target[$ data[0]] : variable_global_get(data[0]);
		}
		
		return ds_map_find_value(data[0], data[1]);
	});
	#endregion
	
	#region Implement Special Properties
	
	TPFuncShared(TPTarget, 
	function(value, target, variable) 
	{
		target = variable[1];
	
		if (is_struct(target))
		{
			target.ref[$ variable[0]] = value;
		}
		else
		{
			target[$ variable[0]] = value;
		}
	}, 
	function(target, data, tween)
	{
		if (is_struct(data[1]))
		{
			return (data[1].ref[$ data[0]]);
		}
		else
		{
			return data[1].id[$ data[0]]; // id might be here to prevent return error??
		}
	});

	TPFuncShared(TPUser, 
	function(value, target, data) 
	{
		TWEEN_USER_VALUE = value;
		TWEEN_USER_TARGET = target;
		TWEEN_USER_DATA = data[1];
		
		if (TGMS_OPTIMISE_USER)
		{
			event_perform_object(target.object_index, ev_other, ev_user0+data[0]);
		}
		else
		with(TWEEN_USER_TARGET)
		{
			event_user(data[0]);
		}
	}, 
	function(target, data, event) 
	{
		TWEEN_USER_GET = 1;
		TWEEN_USER_TARGET = target;
		TWEEN_USER_DATA = data[1];
		
		if (TGMS_OPTIMISE_USER)
		{
			event_perform_object(TWEEN_USER_TARGET.object_index, ev_other, ev_user0+data[0]);
		}
		else
		with(TWEEN_USER_TARGET)
		{
			event_user(data[0]);
		}
		
		data = TWEEN_USER_GET; // Repurpose 'data' to avoid var overhead
		TWEEN_USER_GET = 0;
		return data;
	});

	TPFuncSharedNormal(TPCol, 
	function(value, target, data, tween)
	{
		if (is_undefined(data[3]))
		{
			var _prop = data[0];
			
			if (is_array(_prop)) // ?
			{
				_prop[@ 3] = 1;
			}
			else // Property Setter Built
			if (ds_map_exists(global.TGMS.PropertySetters, _prop))
			{
				data[@ 3] = 2;
				data[@ 0] = global.TGMS.PropertySetters[? _prop];
			}
			else
			if (target[$ _prop] != undefined) // Assume instance/struct
			{
				data[@ 3] = 3;
			}
			else // Assume string property
			{	// global
				if (string_pos("global.", _prop))
				{
					data[@ 3] = 4;
					data[@ 0] = string_replace(_prop, "global.", "");
				}
				else // self
				if (string_pos("self.", _prop))
				{
					data[@ 3] = 5;
					data[@ 0] = string_replace(_prop, "self.", "");
				}
				else // other
				if (string_pos("other.", _prop))
				{
					data[@ 3] = 6;
					data[@ 0] = string_replace(_prop, "other.", "");
				}
				else // TODO: More advanced handling
				{
					show_error("Advanced dot notation is not yet supported by this function!", false);
				}
			}
		}
		
		switch(data[3])
		{
		case 2: data[0](merge_colour(data[1], data[2], value), target);					break;
		case 3: target[$ data[0]] = merge_colour(data[1], data[2], value);				break;
		case 4: variable_global_set(data[0], merge_colour(data[1], data[2], value));	break;
		case 5: tween[TWEEN.CALLER][$ data[0]] = merge_colour(data[1], data[2], value); break;
		case 6: tween[TWEEN.OTHER][$ data[0]] = merge_colour(data[1], data[2], value);	break;
		}
	});

	TPFuncShared(TPPath,
	function(amount, target, pathData, tween) // SETTER
	{
		var _path, _xstart, _ystart, _xrelative, _yrelative;
		
		if (is_real(pathData))
		{	// ABSOLUTE
			_path = pathData;
			_xstart = path_get_x(_path, 0);
			_ystart = path_get_y(_path, 0);
			_xrelative = 0;
			_yrelative = 0;
		}
		else
		{
			_path = pathData[0];
			_xstart = path_get_x(_path, 0);
			_ystart = path_get_y(_path, 0);
		
			if (array_length(pathData) == 3)
			{
				_xrelative = pathData[1] - _xstart;
				_yrelative = pathData[2] - _ystart;
			}
			else
			if (pathData[1]) // Absolute
			{
				_xrelative = 0;
				_yrelative = 0;
			}
			else
			{
				_xrelative = target.x - _xstart;
				_yrelative = target.y - _ystart;
				pathData[@ 1] = target.x; // Right... if I don't do this, it'll always use the update x/y position to offset.. which is wrong!
				pathData[@ 2] = target.y;
			}
		}
	
		if (tween[TWEEN.MODE] == TWEEN_MODE_REPEAT)
		{
			var _xDif = path_get_x(_path, 1) - _xstart;
			var _yDif = path_get_y(_path, 1) - _ystart;
	            
			if (amount >= 0)
			{   
				_xrelative += _xDif * floor(amount); 
				_yrelative += _yDif * floor(amount);
				amount = amount % 1;
			}
			else 
			if (amount < 0)
			{
				_xrelative += _xDif * ceil(amount-1);
				_yrelative += _yDif * ceil(amount-1);
				amount = 1 + amount % 1;
			}
				
			target.x = path_get_x(_path, amount) + _xrelative;
			target.y = path_get_y(_path, amount) + _yrelative;
		}
		else
		if (amount > 0)
		{
			if (path_get_closed(_path) || amount < 1)
			{
				target.x = path_get_x(_path, amount % 1) + _xrelative;
				target.y = path_get_y(_path, amount % 1) + _yrelative;
			}
			else
			{
				var _length = path_get_length(_path) * (abs(amount)-1);
				var _direction = point_direction(path_get_x(_path, 0.999), path_get_y(_path, 0.999), path_get_x(_path, 1), path_get_y(_path, 1));
	                
				target.x = path_get_x(_path, 1) + _xrelative + lengthdir_x(_length, _direction);
				target.y = path_get_y(_path, 1) + _yrelative + lengthdir_y(_length, _direction);
			}
		}
		else 
		if (amount < 0)
		{
			if (path_get_closed(_path))
			{
				target.x = path_get_x(_path, 1+amount) + _xrelative;
				target.y = path_get_y(_path, 1+amount) + _yrelative;
			}
			else
			{
				var _length = path_get_length(_path) * abs(amount);
				var _direction = point_direction(_xstart, _ystart, path_get_x(_path, 0.001), path_get_y(_path, 0.001));
	                
				target.x = _xstart + _xrelative - lengthdir_x(_length, _direction);
				target.y = _ystart + _yrelative - lengthdir_y(_length, _direction);
			}
		}
		else // amount == 0
		{
			target.x = _xstart + _xrelative;
			target.y = _ystart + _yrelative;
		}
	},
	function(target, data, tween) // GETTER
	{
		return target.path_position;	
	});
	#endregion


	//=========================
	// AUTO PROPERTIES
	//=========================

	// Default global property setter
	global.TGMS.Variable_Global_Set = function(value, null, variable) 
	{
		return variable_global_set(variable, value);
		null = 0; // Prevent complaint about unused 'null' (target)
	}


	// Default instance property setter
	global.TGMS.Variable_Instance_Set = function(value, target, variable) 
	{
		target[$ variable] = value;
	}


	// NOTE: This could be sped up in the future to pre-check between structs and instance targets... this is used in the dynamic properties with dot . syntax
	global.TGMS.Variable_Struct_String_Set = function(value, target, data) 
	{
		target = data[0]; // We don't use the ACTUAL tween target
			
		if (is_undefined(data[3])) // See if we haven't set extended data yet for execution control
		{
			static str_dollar = "$";
				
			if (is_real(target)) // INSTANCE TARGET
			{
				// METHOD
				if (target[$ str_dollar+data[1]] != undefined)
				{
					data[@ 3] = 1;
					data[@ 1] = str_dollar+data[1];
				}
				else // FUNCTION
				if (ds_map_exists(global.TGMS.PropertySetters, data[1]))
				{
					data[@ 3] = 2;
					data[@ 1] = global.TGMS.PropertySetters[? data[1]];
				}
				else // DYNAMIC
				{
					data[@ 3] = 3;
				}
			}
			else // STRUCT TARGET
			{
				// METHOD
				if (target.ref[$ str_dollar+data[1]] != undefined)
				{
					data[@ 3] = 4;
					data[@ 1] = str_dollar+data[1];
				}
				else // FUNCTION
				if (ds_map_exists(global.TGMS.PropertySetters, data[1]))
				{
					data[@ 3] = 5;
					data[@ 1] = global.TGMS.PropertySetters[? data[1]];
				}
				else // DYNAMIC
				{
					data[@ 3] = 6;
				}
			}
		}
			
		// TODO: Could potentially optimise method setters by directly storing method -- but playing safe for now to prevent memory issues?
		switch(data[3])
		{
		case 1: target[$ data[1]](value, target, data[2]);
		break;
		case 2: data[1](value, target, data[2]);
		break;
		case 3: target[$ data[1]] = value;
		break;
		case 4: target.ref[$ data[1]](value, target.ref, data[2]);
		break;
		case 5: data[1](value, target.ref, data[2]);
		break;
		case 6: target.ref[$ data[1]] = value;
		break;
		}
	}
}


// NOTE: Do not try to optimise these checks. They need to be checked each time anyway.
// NOTE: Keep this as a function to improve performance!!
function TGMS_Variable_Get(target, variable, caller, caller_other) 
{		
	static STR_DOT = ".";
	static STR_SELF = "self";
	static STR_OTHER = "other";
	static STR_GLOBAL = "global";
	static STR_AMPERSAND = "&";
	
	// ADVANCED ARRAY
	if (is_array(variable))
	{	
		// SCRIPT -- Return
		if (ds_map_exists(global.TGMS.PropertyGetters, variable[0])) 
		{
			return global.TGMS.PropertyGetters[? variable[0]](target, variable[1]);
		}

		// Get variable string from advanced data and keep executing below...
		variable = variable[1];
		// Get variable string from inner array if WE MUST GO DEEPER! Muhahaha (I'm ok)
		if (is_array(variable)) { variable = variable[0]; }
	}

	// METHOD
	if (target[$ STR_AMPERSAND+variable] != undefined)
	{	
		return target[$ STR_AMPERSAND+variable](target, variable);
	}

	// FUNCTION
	if (ds_map_exists(global.TGMS.PropertyGetters, variable)) 
	{
		return global.TGMS.PropertyGetters[? variable](target);
	}

	// INSTANCE
	if (target[$ variable] != undefined)
	{
		return target[$ variable];
	}
	
	// CALLER
	if (caller[$ variable] != undefined)
	{
		return caller[$ variable];
	}
	
	// GLOBAL
	if (variable_global_exists(variable)) 
	{
		return variable_global_get(variable);
	}
	
	// NUMBER
	if (string_byte_at(variable, 1) <= 57) 
	{
		return real(variable);
	}
	
	// Extended
	// global.thing.sub_thing
	if (string_count(STR_DOT, variable) >= 2)
	{
		var _dotPos = string_pos(STR_DOT,variable);
		var _prefix = string_copy(variable, 1, _dotPos-1);
		
		variable = string_copy(variable, _dotPos+1, 100);
		var _postfix = string_copy(variable, string_pos(STR_DOT, variable)+1, 100);
		
		// Object variable
		if (object_exists(asset_get_index(_prefix)))
		{
			target = asset_get_index(_prefix).id[$ string_copy(variable, 1, string_pos(STR_DOT, variable)-1)];
		}
		else
		switch(_prefix)
		{
			case STR_SELF:
				target = caller[$ string_copy(variable, 1, string_pos(STR_DOT, variable)-1)];
			break;
			
			case STR_GLOBAL:
				target = variable_global_get(string_copy(variable, 1, string_pos(STR_DOT, variable)-1));
			break;
			
			case STR_OTHER:
				target = caller_other[$ string_copy(variable, 1, string_pos(STR_DOT, variable)-1)];
			break;
		}
		
		// METHOD
		if (target[$ STR_AMPERSAND+_postfix] != undefined)
		{	
			return target[$ STR_AMPERSAND+_postfix](target, _postfix);
		}
		
		// FUNCTION
		if (ds_map_exists(global.TGMS.PropertyGetters, _postfix))
		{
			return global.TGMS.PropertyGetters[? _postfix](target);
		}
		
		// DYNAMIC
		return target[$ string_copy(variable, string_pos(STR_DOT, variable)+1, 100)];
	}
	
	// EXPRESSION -- Short
	var _prefix = string_copy( variable, 1, string_pos(STR_DOT, variable)-1 );
	var _postfix = string_copy(variable, 1+string_pos(STR_DOT, variable), 100);
	
	// Object variable
	if (object_exists(asset_get_index(_prefix)))
	{
		return variable_instance_get(asset_get_index(_prefix).id, _postfix);
	}

	// Caller/Self variable
	if (_prefix == STR_SELF)
	{	
		// METHOD
		if (caller[$ STR_AMPERSAND+_postfix] != undefined)
		{	
			return caller[$ STR_AMPERSAND+_postfix](caller, _postfix);
		}
		
		// FUNCTION
		if (ds_map_exists(global.TGMS.PropertyGetters, _postfix))
		{
			return global.TGMS.PropertyGetters[? _postfix](caller);
		}

		// DYNAMIC
		return caller[$ _postfix];
	}
	
	// Other variable
	if (_prefix == STR_OTHER)
	{
		// METHOD
		if (caller_other[$ STR_AMPERSAND+_postfix] != undefined)
		{	
			return caller_other[$ STR_AMPERSAND+_postfix](caller_other, _postfix);
		}
		
		// FUNCTION
		if (ds_map_exists(global.TGMS.PropertyGetters, _postfix))
		{
			return global.TGMS.PropertyGetters[? _postfix](caller_other);
		}
		
		// DYNAMIC
		return caller_other[$ _postfix];
	}
	
	//
	// I NEED TO ADD OPTIMSED CHECKS HERE!!!
	//
	
	// Global variable
	if (_prefix == STR_GLOBAL)
	{
		// NOTE: Optimisation check not required here because of Optimisation check above (Teach to use "global.value" with TPFuncShared()
		return variable_global_get(_postfix);
	}
	
	// Target variable
	if (target[$ _prefix] != undefined)
	{
		target = target[$ _prefix];
		
		// METHOD
		if (target[$ STR_AMPERSAND+_postfix] != undefined)
		{	
			return target[$ STR_AMPERSAND+_postfix](target, _postfix);
		}
		
		// FUNCTION
		if (ds_map_exists(global.TGMS.PropertyGetters, _postfix))
		{
			return global.TGMS.PropertyGetters[? _postfix](target);
		}
		
		return target[$ _postfix];
	}

	// Caller variable
	if (caller[$ _prefix] != undefined)
	{
		target = caller[$ _prefix];
		
		// METHOD
		if (target[$ STR_AMPERSAND+_postfix] != undefined)
		{	
			return target[$ STR_AMPERSAND+_postfix](target, _postfix);
		}
		
		// FUNCTION
		if (ds_map_exists(global.TGMS.PropertyGetters, _postfix))
		{
			return global.TGMS.PropertyGetters[? _postfix](target);
		}
		
		// DYNAMIC
		return target[$ _postfix];
	}

	// Global
	target = variable_global_get(_prefix);
	return target[$ _postfix];
}

