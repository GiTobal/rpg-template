global.__lima_dummy_function = function(arg) {}
global.__lima_text_template = {
    selected:   global.__lima_dummy_function,   // Si es seleccionado       
    deselected: global.__lima_dummy_function,   // Si esta deseleccionado
    active:     global.__lima_dummy_function,   // Si esta activo
    desactive:  global.__lima_dummy_function    // Si no esta activo
}

enum LIMA_COMPONENTS {
    PANEL,
    BOX,
    IMAGE,
    TEXT,
    TEXT_LIST,

    _PARENT_,
    _SIZE_
}

enum LIMA_EVTYPE {
    INSELF, //  se ejecuta cada ciclo.
    ONCALL, //  Se ejecuta cada ciclo, pero primero debe ser llamado.
    ONNOT,  //  Se ejecuta cada ciclo mientras no se llame.
    ONKEY,  //  Se ejecuta cuando se presiona una tecla.
    
    _NOTTYPE_,  // Simplemente crear funciones al inicio
    _SIZE_
}

/// @param left
/// @param top
/// @param right
/// @param bottom
function LimaLTRB(_l, _t, _r, _b) constructor {
    l = _l;
    t = _t;
    r = _r;
    b = _b;
    
    /// @returns {LimaLTRB}
    static Copy = function() {
        return (new LimaLTRB(l, t, r, b) );
    }
}

/// @param item_1
/// @param [item_2]
/// @param [item_3]
function LimaAtom(_item1, _item2, _item3) constructor {
	item1 = _item1;
	item2 = _item2;
	item3 = _item3;
}

/// @param child/child_array
/// @desc Permite a un elemento de lima difinir a sus hijos
function lima_define_child(_child) {
    if (!is_array(_child) ) {
        // Si no se pasa algo valido salir nomas.
        if (_child == undefined || _child == id) exit;
        
        _child.parent = id;
        
        // Heredar layer
        _child.layer_name = layer_name;
        _child.layer_id   = layer_id;
                
        // Heredar sangría?
        if (_child.inheritIndent)       _child.ind = ind.Copy();
        // Heredar separación?
        if (_child.inheritSeparation)   _child.sep = sep.Copy();
    }
    // Array de hijos
    else {
        var i = 0; repeat(array_length(_child) ) lima_define_child(_child[i++] );
    }
}

/// @param parent
function lima_send_child(_parent) {
	/*var i = 0; repeat(argument_count) {
		var in = argument[i++];
		
		if (in.parent == noone || in.parent == id) continue;
		
		in.parent = 
	}*/
	
	
    if (parent != noone || parent == id) exit;
    
    parent = _parent;
    
    with (parent) array_push(_parent.childrens, other.id);
}


#region Eventos
/// @param event_name
/// @param event_type
/// @param arguments
/// @param function
/// @param context*
function lima_create_event(_name, _type, _arg, _ev, _user = id) {
    var _event = {
		user: _user,
		name: _name,
		type: _type,
		arg: _arg,
		
		oncall: false,
		onnot : false,
		
		key_con: -1,
		key: -1,
		
		ev: undefined,
		ex: undefined,
		mo: undefined,
		pass: false,
		time: 0
	}
	
	// Crear estructura
	with (_event) {
		switch (_type) {
			case LIMA_EVTYPE.ONKEY:
				key = _arg[1];
				key_con = _arg[0];
				
				ex = method(_user, _ev);
				
				ev = function() {	
					if (key_con(key) && !pass) ex();
					pass = false;
				}
				break;
		
			default:
				ev = method(_user, _ev);
			
				break;
		}
	}

    
    // Agregar
    events.Set(_name, _event);
}

/**
	@param event_name
	@param new_type
	@param new_arguments
	@param [new_method]
	@param [new_context]
	@desc Modifica un evento ya creado, se puede cambiar el tipo, argumentos y el metodo por separado.
*/
function lima_modify_event(_name, _type, _arg, _ev, _user = id) {
    var _event = events.Get(_name);
	
	with (_event) {
		type = _type ?? type;
		arg  = _arg  ??  arg;
		
		switch (type) {
			case LIMA_EVTYPE.ONKEY:
				key_con = arg[0];
				key		= arg[1];
			
				if (_ev != undefined) ex = method(_user, _ev);
	
				break;
				
			default:
				if (_ev != undefined) ev = method(_user, _ev);			
			
				break;
		}
	}
}

/** @desc Extiende el funcionamiento de un evento (es como heredar lineas de codigo) */
function lima_extend_event(_name, _ev, _user = id) {
    var _event = events.Get(_name);
	
	with (_event) {
		switch (type) {
			case LIMA_EVTYPE.ONKEY:
				ex = method(_user,  ex);
				mo = method(_user, _ev);
			
				ev = function() {
					if (key_con(key) && !pass) {ex(); mo(); }
					pass = false;
				}

				break;
				
			default:
				mo = method(_user, _ev);
				ex = method(_user,  ev);
				ev = function() {
					ex();
					mo();
				}

				break;
		}
	}
}

function lima_pass_event(_name) {
	var _event = events.Get(_name);
	
	_event.pass = true;
}

function lima_default_events() {
    // Mover
    lima_create_event("Move", LIMA_EVTYPE._NOTTYPE_, {x: 0, y: 0}, function(arg) {
        // Me
        x += arg.x;
        y += arg.y;
        
        // Mover a los hijos
        var i = 0; repeat (array_length(childrens) ) {
            with (childrens[i++] ) {     
                x += arg.x;
                y += arg.y;
            }
        }
    });
    
    // Teletransportar
    lima_create_event("TP", LIMA_EVTYPE._NOTTYPE_, {x:0, y:0}, function(arg) {
        // Me
        x = arg.x;
        y = arg.y;
        
        // Mover a los hijos
        var i = 0; repeat (array_length(childrens) ) {
            with (childrens[i++] ) {     
                x = other.x + arg.x;
                y = other.y + arg.y;
            }
        }         
    });

}


#endregion


#region Listeners
/// @param instance/structure
/// @param Name
/// @param id*	Valores que seguir
function lima_add_listener(_ref, _name, _id) {
	// Para buscar
	array_push(listeners, _name);
	variable_instance_set(id, _name, weak_ref_create(_ref) );
}

/// @param string
function lima_listener_parser(_string) {
	var _len    = string_length(_string);
	var _result = ""; 
	var _copy = "";
	
	var i = 1;
	var _mode = 0;
	
	repeat(_len) {
		switch (_mode) {
			case 0:	// Copiar normalmente
				_copy = string_copy(_string, 1, i);	
				var _at = string_char_at(_copy, i++);
				// Funcion
				if (_at == "?") {
					var _pos = string_pos("?", _string);
					
					
					_mode = 1;
				} 
				// Listener
				else if (_at == "@") {
					
				}
				// Texto normal
				else {
					
				}
				break;
		}
	}
	
	return _string;
}

function lima_update_listeners() {
	var i = 0; repeat(array_length(listeners) ) {
			
	}
	
}


#endregion









