#macro __CLASICO_VERSION "1.0.0"
#macro __CLASICO_PREFIX "[Clasico]"
#macro __CLASICO_SHOW false

#macro START_TIMER	var time1, time2; time1 = get_timer();
#macro END_TIMER	time2 = get_timer(); show_debug_message("time: " + string( (time2 - time1) / 1000) + " [ms]");

#macro random_color make_color_rgb(irandom(255), irandom(255), irandom(255) )

#macro print show_debug_message

#macro ARG_ARRAY __arguments
#macro ARG_INDEX __i
#macro ARG_MAKE_ARRAY\
	var __arguments = [];\
	var __i = 0; repeat(argument_count) {array_push(__arguments, argument[__i] ); ++__i; }

#region Instancias
/// @param id
/// @param object
/// @param check_descendant?
/// @desc Comprueba si una instancia es un objeto o descendiente
function instance_object(_id, _obj, _check = false) {
	with (_id) {
		return (is_object(_obj, _check) );
	}
}

/// @param object
function instance_to_array(_obj) {
	var _array = [];
	
	with (_obj) array_push(_array, id);
	
	return (_array);
}

/// @param object
/// @param direction
/// @param distance
/// Detecta objetos en una direccion y devuelve una lista con los que encontró (la lista no hay que borrarla)
function instance_detect(_object, _dir, _dis) {
	static _collisionList = ds_list_create();
	
	var _len = -1;
	var x_offset = abs(sprite_xoffset - (sprite_width  / 2) );
	var y_offset = abs(sprite_yoffset - (sprite_height / 2) );
	
	// Dependiendo de la direccion
	switch(_dir) {
		case 90 :	_len = collision_line_list(x + x_offset, y - y_offset, x + x_offset, y - (y_offset + _dis) , _object, false, true, _collisionList, true);   break; //Arriba
		case 0  :	_len = collision_line_list(x + x_offset, y - y_offset, x + (x_offset + _dis), y - y_offset , _object, false, true, _collisionList, true);   break; //Derecha
		case 180:	_len = collision_line_list(x + x_offset, y - y_offset, x - (x_offset + _dis), y - y_offset , _object, false, true, _collisionList, true);   break; //Izquierda
		case 270:   _len = collision_line_list(x + x_offset, y - y_offset, x + x_offset, y + (y_offset - _dis) , _object, false, true, _collisionList, true);   break; //Abajo
 
		default: show_debug_message("Error Actores: Direccion mal usada" ); break;
	}

	return [_collisionList, _len];
}

/// @param object
/// @param  xoffset
/// @param  yoffset
function stick_to(_obj, _x, _y) {
	x = _obj.x + _x;
	y = _obj.y + _y;
}

/// @param object
/// @param  xoffset
/// @param  yoffset
/// @param  angleoffset
function stick_to_angle(_obj, _xoffset, _yoffset, _angleoffset) {
	var _dis = point_distance (_obj.x, _obj.y, _obj.x + _xoffset, _obj.y + _yoffset);
	var _dir = point_direction(_obj.x, _obj.y, _obj.x + _xoffset, _obj.y + _yoffset);
	
	// Pos
	x = _obj.x + lengthdir_x(_dis, _dir + _obj.image_angle);
	y = _obj.y + lengthdir_y(_dis, _dir + _obj.image_angle);
	
	// Angulo
	image_angle = _obj.image_angle + _angleoffset;
}


#endregion

#region Is

	#region Native
/// @param room
/// @returns {bool}
function is_room(_room) {
	return (room == _room);
}

/// @param object
/// @param check_descendant?
/// @returns {bool}
/// @desc Comprueba si esta instancia es un objeto o descendiente
function is_object(_obj, _check = false) {
	return (object_index == _obj) || (_check && object_is_ancestor(object_index, _obj) );
}

/// @returns {bool}
/// @desc Comprueba si unas cordenadas se encuentran entre un rango establecido
function is_here(_x, _y, _x1, _y1, _x2, _y2) {
	return ((_x > _x1) && (_x < _x2) && (_y > _y1) && (_y < _y2) );
}

#endregion

	#region Math
/// @param {Id.Vector2} Vector2
/// @returns {Bool}
function is_vector2(_vec2) {
	return (is_struct(_vec2) && (_vec2.__is == Vector2) );
}

/// @param {Line} line
/// @returns {bool}
function is_line(_line) {
	return (is_struct(_line) && (_line.__is == Line) );	
}

/// @param {Rectangle} rectangle
/// @returns {bool}
function is_rectangle(_rect) {
	return (is_struct(_rect) && (_rect.__is == Rectangle) );	
}

#endregion

	#region QL
/// @param {Canvas} canvas
/// @returns {bool}
function is_canvas(_canvas) {
	return (is_struct(_canvas) && (_canvas.__is == Canvas) );	
}

/// @param {Parser} parser
/// @returns {bool}
function is_parser(_parser) {
	return (is_struct(_parser) && (_parser.__is == Parser) );	
}

/// @param {Data} data
/// @returns {bool}
function is_data(_data) {
	return (is_struct(_data) && (_data.__is == Data) );	
}

/// @param {DeltaTime} delta
/// @returns {bool}
function is_delta(_delta) {
	return (is_struct(_delta) && (_delta.__is == DeltaTime) );
}

/// @param {Bucket} bucket
/// @returns {bool}
function is_bucket(_bucket) {
	return (is_struct(_bucket) && (_bucket.__is == Bucket) );
}

#endregion

#endregion

#region Math
/// @param value1
/// @param value2
/// @param step
function approach(a, b, _step) {
	if (a < b){
		return min (a + abs(_step), b);
	} else {
		return max (a - abs(_step), b);
	}
}

/// @param min
/// @param max
/// @param value
/// @desc Si valor es menor que minimo devuelve maximo y si valor es mayor que maximo devuelve minimo.
function clip(_min, _max, _val) {
	// Ir más rapido
	if (_val > _max) {return _min; } else 
	if (_val < _min) {return _max; }
	
	return (_val);
}

/// @param value
/// @param range_a
/// @param range_b
/// @desc Si un valor se encuentra entre a y b
function between(_val, _ra, _rb) {
	var _l = min(_ra, _rb), _r = max(_ra, _rb);

	return (_l < _val && _val < _r);
}


#endregion

#region String
/// @desc Establece caracteres al azar
function string_random_symbol(_rep = 1) {
	var _sym1 = irandom_range(32, 47);		// !"#$%&'()*+,-./
	var _sym2 = irandom_range(58, 64);		// :;<=>?@
	
	var _txt = ""; repeat(_rep) _txt += chr(choose(_sym1, _sym2) );
	
	return _txt;
}

/// @desc Establece letras al azar
function string_random_letter(_rep = 1) {
	var _letter1 = irandom_range(65,  90);
	var _letter2 = irandom_range(97, 122);
	
	var _txt = ""; repeat(_rep) _txt += chr(choose(_letter1, _letter2) );		
	
	return _txt;
}

function string_random(_rep = 1) {
	var _txt = ""; repeat(_rep) {
		var _symbol = string_random_symbol(); 
		var _letter = string_random_letter();
		
		_txt += choose(_symbol, _letter);
	}
	
	return (_txt);
}

/** @param _real in seconds */
function string_time_hms(_seconds) {
	var _hours		= _seconds div 3600;
	_seconds		= _seconds mod 3600;
	var _minutes	= _seconds div 60;
	_seconds		= _seconds mod 60;
    
	return ((_hours   div 10) > 0 ? "" : "0") + string(_hours  ) + ":"
	        + ((_minutes div 10) > 0 ? "" : "0") + string(_minutes) + ":" 
	        + ((_seconds div 10) > 0 ? "" : "0") + string(floor(_seconds));
}

#endregion

#region Percent
/// @param percent
/// @returns {bool}
function percent_chance(_percent) {
	return (random(100) <= _percent);
}

/// @param percent
/// @param winner_value
/// @param loser_value
function percent_set(_percent, _winner, _loser) {
	// chance to set value to winner, otherwise value is set to loser
	return (percent_chance(_percent) ) ? _winner : _loser;
}

/// @param percent
/// @returns {number}
function percent_between(_percent) {
	return (_percent - random(_percent) ) / max(0.01, _percent);
}


#endregion

#region Array

/** @param {Array} Array @return {Bool} */
function array_empty(_array) {return (array_length(_array) < 1); }

/// @param array
/// Devuelve el menor valor del array
function array_min(_array) {
	var _temp = undefined;
	
	var i = 0; repeat(array_length(_array) ) {
		var _in = _array[i++] ?? 0;	
		
		_temp = min(_temp ?? _in, _in);
	}
		
	return (_temp);
}

/// @param array
/// Devuelve el mayor valor del array
function array_max(_array) {
	var _temp = undefined;
	
	var i = 0; repeat(array_length(_array) ) {
		var _in = _array[i++] ?? 0;	

		_temp = max(_temp ?? _in, _in);
	}
		
	return (_temp);
}

/// Reinicia un array 
function array_reset(_array) {
	array_delete(_array, 0, array_length(_array) - 1);	
}

/// @param size1
/// @param size2
/// @param ....
/// @Sahaun make it
function array_create_nd() {
    if (argument_count == 0) return 0;
    
    var _array = array_create(argument[0]),
        _args  = array_create(argument_count-1),
        _i;
        
    _i = 0; repeat(argument_count-1) {
        _args[@ _i] = argument[_i+1];
        ++_i;
    }
    
    _i = 0; repeat(argument[0]) {
        _array[@ _i] = script_execute_ext(array_create_nd, _args);
        ++_i;
    }
    
    return _array;
}

/// @param {array} array
/// @returns {array} el array dado vuelta
function array_reverse(_array) {
	var _size = array_length(_array), _reversed = [];
	
	var i = 0; repeat(_size) {array_push(_reversed, _array[@ (_size - 1) + i++] ); }
	
	return _reversed;
}

/// @param {array} array The array to swap values within.
/// @param {number} source The first index.
/// @param {number} destination The second index.
function array_swap(_array, _i, _j) {
	var _temp = _array[_i];
	
	_array[@ _i] = _array[_j];
	_array[@ _j] = _temp;
}

/// @param {array} array The array to shuffle.
/// @desc Shuffles the values within the array, ordering them randomly.
function array_shuffle(_array) {
	var _len1 = array_length(_array), _len2 = _len1 - 1;

	var _seed = random_get_seed();	randomize();
	
	repeat (_len1) {array_swap(_array, irandom(_len2), irandom(_len2)); }
	
	random_set_seed(_seed);
}

/// @param {array} array1 The first array.
/// @param {array} array2 The second array.
/// @return {array} The created array.
/// @desc Merges two arrays into a new one, appending values from the second array to the end of the first array
/// [1, 2, 3] && [3, 4, 5] -> [1, 2, 3, 3, 4, 5]
function array_merge(_a1, _a2) {
	var _s1 = array_length(_a1), _s2 = array_length(_a2);
	var _merged = array_create(_s1 + _s2, 0);
	
	array_copy(_merged, 0, _a1, 0, _s1);
	array_copy(_merged, _s1, _a2, 0, _s2);
	
	return _merged;
}

/// @param {array} array
/// @param {array} delete_array
/// Debe ser un array con string (Ambos!)
function array_delete_other(_dest, _delete) {

}


#endregion

#region Other
/// @return N/A (0)
/// 
/// Executes a method call for each element of the given struct/array/data structure.
/// This iterator is shallow and will not also iterate over nested structs/arrays (though
/// you can of course call foreach() inside the specified method)
/// 
/// This function can also iterate over all members of a ds_map, ds_list, or ds_grid.
/// You will need to specify a value for [dsType] to iterate over a data structure
///
/// The specified method is passed the following parameters:
/// 
/// arg0  -  Value found in the given struct/array
/// arg1  -  0-indexed index of the value e.g. =0 for the first element, =1 for the second element etc.
/// arg2  -  When iterating over structs, the name of the variable that contains the given value; otherwise <undefined>
/// 
/// The order that values are sent into <method> is guaranteed for arrays (starting at
/// index 0 and ascending), but is not guaranteed for structs due to the behaviour of
/// GameMaker's internal hashmap
/// 
/// @param struct/array/ds   Struct/array/data structure to be iterated over
/// @param method            Method to call for each element of this given struct/array/ds
/// @param [dsType]          Data structure type if iterating over a data structure
/// 
/// @jujuadams 2020-06-16

function foreach()
{
    var _ds       = argument[0];
    var _function = argument[1];
    var _ds_type  = (argument_count > 2)? argument[2] : undefined;
    
    if (is_struct(_ds))
    {
        var _names = variable_struct_get_names(_ds);
        var _i = 0;
        repeat(array_length(_names))
        {
            var _name = _names[_i];
            _function(variable_struct_get(_ds, _name), _i, _name);
            ++_i;
        }
    }
    else if (is_array(_ds))
    {
        var _i = 0;
        repeat(array_length(_ds))
        {
            _function(_ds[_i], _i, undefined);
            ++_i;
        }
    }
    else switch(_ds_type)
    {
        case ds_type_list:
            var _i = 0;
            repeat(ds_list_size(_ds))
            {
                _function(_ds[| _i], _i, undefined);
                ++_i;
            }
        break;
        
        case ds_type_map:
            var _i = 0;
            var _key = ds_map_find_first(_ds);
            repeat(ds_map_size(_ds))
            {
                _function(_ds[? _key], _i, _key);
                _key = ds_map_find_next(_ds, _key);
                ++_i;
            }
        break;
        
        case ds_type_grid:
            var _w = ds_grid_width( _ds);
            var _h = ds_grid_height(_ds);
            
            var _y = 0;
            repeat(_h)
            {
                var _x = 0;
                repeat(_w)
                {
                    _function(_ds[# _x, _y], _x, _y);
                    ++_x;
                }
                
                ++_y;
            }
        break;
        
        default:
            show_error("Cannot iterate over datatype \"" + string(typeof(_ds)) + "\"\n ", false);
        break;
    }
}

/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @returns {bool}
/// @desc Returns true if the mouse is within the rectangular region,
///		  where (x1, y1) is the top left corner and (x2, y2) is the bottom right.
function mouse_is_here(_x1, _y1, _x2, _y2) {
	return ((mouse_x > _x1) && (mouse_x < _x2) && (mouse_y > _y1) && (mouse_y < _y2) );
}

/// @return {bool} true if cursor is within object's bounding box
function mouse_over() {
	return (
		mouse_x >= bbox_left	&&
	    mouse_x <= bbox_right	&&
	    mouse_y >= bbox_top		&&
	    mouse_y <= bbox_bottom
	);
}

/// @param {weak_reference} weak_reference
function weak_get(_weak) {
	return (_weak.ref);
}

function alarm_delay(_index, _value, _id = id, _seconds = false) {
	if (_id.alarm[_index] == -1) {
		_id.alarm[_index] = (_seconds) ? game_get_speed(gamespeed_fps) * _value : _value;	
	}
}

#region Custom Restart
#macro game_restart __game_restart

function __game_restart() {
  // This will destroy all instances. 
  // Yes, this will run their cleanup events as well as their destroy event.
  with(all) instance_destroy();

  audio_stop_all();
  draw_texture_flush();

  // Go to the very first room, as per room order
  room_goto(room_first);
}

#endregion