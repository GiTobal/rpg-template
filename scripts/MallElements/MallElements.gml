#macro MELEMENT_ATK_PREFIX 0
#macro MELEMENT_DEF_PREFIX 1

/*
    Donde se guarda la configuracion para los elementos del proyecto
        Se configuran: 
            1) Que estado causa este elemento y la probabilidad de hacerlo
            2) Que estadistica defiende a este elemento
            3) Que estadistica ataca con este elemento
            4) Que estadistica puede absorver este elemento     (en base a un umbral)
            5) Que estadistica es reducida por este elemento    (en base a un umbral)
*/
function MallElement(_key) : MallComponent(_key) constructor {
    __produce = []; // Que estados produce [estado, probabilidad]
    
    __attack = []; //  Que estadistica ataca con este elemento
    __defend = []; //  Que estadistica defiende con este elemento
    
    __absorbed = {};    //  Que estadistica absorve este elemento
    __reduced  = {};    //  Que estadistica es reducido por este elemento
    
    #region Metodos
    /// @param state_key
    /// @param probability
    static AddProduce = function() {
        var i = 0; repeat(argument_count div 2) {
            array_push(__produce, [argument[i++], argument[i++] ] ); 
        }
        
        return self;
    }
    
    /// @param stat_key
    /// @param threshold
    static AddAbsorbed = function(_stat, _threshold) {
        if (argument_count <= 2) {
            __absorbed[$ _stat] = _threshold;    
        } else {    // Soporte para varios
            var i = -1; repeat(argument_count div 2) {AddAbsorbed(argument[i], argument[i++] ); }
        }
        
        return self;
    }
    
    /// @param stat_key
    /// @param threshold
    static AddReduced  = function(_stat, _threshold) {
        if (argument_count <= 2) {
            __reduced[$ _stat] = _threshold;
        } else {
            var i = -1; repeat(argument_count div 2) {AddReduced(argument[i++], argument[i++] ); }    
        }
        
        return self;
    }
    
    /// @param stat_key    
    static AddAttack = function() {
       var i = -1; repeat(argument_count) {array_push(__attack, argument[i++] ); } 
    }
    
    /// @param stat_key
    static AddDefend = function() {
        var i = -1; repeat(argument_count) {array_push(__defend, argument[i++] ); }
        return self;
    }

    #endregion
}

/// @param {string} key
/// @param ...
/// @desc Crea un elemento y agrega estadistica al storage en base a este, mediante prefijos para trabajar con ellos
function mall_create_elements(_key) {
    var _elements = MALL_STORAGE.__elementsMaster;
    var _prefix   = [];
    
    var i = 1; repeat(argument_count - 1) {
        var _in = argument[i++];    
            
        // Guardar los prefijos de elemento    
        array_push(_prefix, _in);
        
        // Crear estadisticas
        mall_create_stats(_key + _in);
    }
    
    // Establecer elemento con los prefijos de stat
    _elements.Set(_key, _prefix);
}

/// @parma delete_array*
function mall_element_init(_deleteArray) {
    MALL_GROUP.InitElements(_deleteArray);
}

/// @returns {array}
/// Devuelve todas las llaves de elemento existentes
function mall_keys_element() {
    return (MALL_STORAGE.__elementsMaster).GetKeys();
}

/// @param element_key
/// @param produce_state
/// @param produce_probability
/// @param ...
/// @returns {MallElement}
function mall_element_customize(_key) {
    var _prefix  = MALL_STORAGE.__elementsMaster.Get(_key); // Obtener prefijos de ataque y defensa
    var _element = MALL_GROUP.__elements[$ _key];   /// @is {MallElement}
    
    if (array_length(_prefix) >= 2) {   // Solo si existe 2 entradas en el array
        _element.AddDefend(_key + _prefix[MELEMENT_DEF_PREFIX] );  // Agregar estadistica def. de defensa
        _element.AddAttack(_key + _prefix[MELEMENT_ATK_PREFIX] );  // Agregar estadistica def. de ataque
    }

    // Agregar que estado produce este elemento
    var i = 1; repeat ( (argument_count - 1) div 2) {
        _element.AddProduce(argument[i++], argument[i++] ); 
    }
    
    return (_element);
}



