 

// Create Shared Tweener Singleton
function SharedTweener() 
{
	if (instance_exists(o_SharedTweener))
	{
		if (o_SharedTweener.image_angle == 1) // This is a hint that the shared tweener is fully activated
		{
			return o_SharedTweener.id;
		}
		else
		{
			instance_destroy(o_SharedTweener);
			return instance_create_depth(0,0,0,o_SharedTweener);
		}
	}
	
	instance_activate_object(o_SharedTweener);
	
	if (instance_exists(o_SharedTweener))
	{
		return o_SharedTweener.id;	
	}
	
	return instance_create_depth(0,0,0,o_SharedTweener);
}
	
	
function TGMS_FetchTween(tweenID)
{
	if (is_array(tweenID))
	{
		return tweenID;
	}
	else
	if (is_real(tweenID))
	{
		SharedTweener();
		
		// Check for tween in Tween Index Map
		if (ds_map_exists(global.TGMS.TweenIndexMap, tweenID))
		{
		    return global.TGMS.TweenIndexMap[? tweenID];
		}
		
		// Tween Offset Select
		if (!is_undefined(tweenID) && tweenID <= 0)
		{
			return global.TGMS.TweenIndexMap[? global.TGMS.TweenIndex-tweenID];	
		}
	}
	else // It's gotta be a struct, right? // TODO: THIS MIGHT BREAK WITH COMING GMS UPDATE!!
	if (is_struct(tweenID)) // Passed 'self' for a struct
	{
		return tweenID == self ? {target: tweenID} : tweenID;
	}
	else // Assume we have passed "self" for an instance
	if (tweenID != undefined)
	{
		return { target: tweenID.id };
	}
}


function TGMS_TargetExists(target) 
{
	if (is_real(target))
	{
		if (global.TGMS.TweensIncludeDeactivated)
		{		
		    if (instance_exists(target))
			{
		        return true;
		    }
    
			instance_activate_object(target);
    
		    if (instance_exists(target))
			{
		        instance_deactivate_object(target);
		        return true;
		    }
	
		    return false;
		}
		
		return instance_exists(target);
	}
	else
	if (target != undefined)
	{
		return weak_ref_alive(target);	
	}
	
	return false;
}

function TGMS_StringStrip(_string, _offset=0) 
{	/// @desc Lowers strings and removes underscores	
	/// @func TGMS_StringStrip(string, [offset])
	
	// Create static string cache
	static cache = global.TGMS.Cache;
	
	// Check cache for existing lowered string
	if (ds_map_exists(cache, _string))
	{
		return cache[? _string];
	}
	
	// Store original string
	var _string_og = _string;
	
	// Remove underscores
	static _underscore = "_";
	static _empty = "";
	_string = string_replace_all(_string, _underscore, _empty);
	
	// Convert string to lowercase
	repeat(string_length(_string)-_offset)
	{
		++_offset;
		var _byte = string_byte_at(_string, _offset);
		if (_byte <= 90 && _byte >= 65)
		{
			_string = string_set_byte_at(_string, _offset, _byte+32);
		}
	}

	// Store new string into cache
	cache[? _string_og] = _string;
	// Return new lower case string
	return _string;
}


function TGMS_Tween(script, args, tweenID)
{
	static STR_AT = "@"; // Cache string since GM's string caching is not working, last I checked.
	
	// Make sure we have a shared tweener singleton
	var _sharedTweener = SharedTweener();
	
	var TGMS = global.TGMS;	
	var _t, _tID, _pData;
	var _doStart = true;
	
	if (script == TweenDefine)
	{
		_tID = (tweenID > 0) ? tweenID : TGMS.TweenIndexMap[? TGMS.TweenIndex-tweenID]; // Cache tween id

		if (!TweenExists(_tID))
		{
			return 0;
		}
		
		_t = TGMS_FetchTween(_tID);  // Fetch raw tween data
		_t[@ TWEEN.CALLER] = is_struct(self) ? weak_ref_create(self) : id; // Struct or Instance calling the script
		_pData = []; // Set new property data array
	}
	else
	if (script == TweenPlay)
	{	
		if (is_array(tweenID))
		{
			_tID = tweenID;	
		}
		else
		{
			_tID = (tweenID > 0) ? tweenID : TGMS.TweenIndexMap[? TGMS.TweenIndex-tweenID]; // Cache tween id
		}
		
		if (!TweenExists(_tID))
		{
			return 0;
		}
		
		_t = TGMS_FetchTween(_tID);  // Fetch raw tween data
		_pData = _t[TWEEN.PROPERTY_DATA_RAW]; // Cache existing variable data list
		_t[@ TWEEN.DIRECTION] = 1;
		_t[@ TWEEN.SCALE] = abs(_t[TWEEN.SCALE]);
		_t[@ TWEEN.TIME] = 0;
		
		if (array_length(args))
		{	//^ IF NEW ARGUMENTS ARE SUPPLIED
			_pData = [];
			_t[@ TWEEN.AMOUNT] = 0;
			_t[@ TWEEN.CALLER] = is_struct(self) ? weak_ref_create(self) : id; // Struct or Instance calling the script TODO
		}
	}
	else
	{
		_tID = ++TGMS.TweenIndex;												// Get new unique tween id
		_t = array_create(TWEEN.DATA_SIZE);
		array_copy(_t, 0, TGMS.TweenDefault, 0, TWEEN.DATA_SIZE);
		_t[TWEEN.ID] = _tID;													// New tween created with unique id
		_t[TWEEN.CALLER] = is_struct(self) ? weak_ref_create(self) : id;		// Struct or Instance calling the script
		_t[TWEEN.TARGET] = _t[TWEEN.CALLER];									// Set default target to caller id
		_t[TWEEN.OTHER] = is_struct(other) ? weak_ref_create(other) : other.id;
		_t[TWEEN.DESTROY] = script != TweenCreate;								// Make persistent?
		ds_list_add(_sharedTweener.tweens, _t);									// Add tween to global tweens list
		TGMS.TweenIndexMap[? _tID] = _t;										// Associate tween with global id
		_pData = [];
	}
	
	var _paramCount = array_length(args)-1; // Number or paramters supplied
	var _qCallbacks = -1;					// Null value for callback queue
	var i = -1;								// Loop iterator
	var _p = -3;							// pData index .. THIS IS NEW!

	// Loop through AND process script parameters
	while(i < _paramCount)
	{		
		// Get next tag
		var _tag = args[++i]; 
		
		// TODO: Consider later...
		// NOTE: is_int64() DOES NOT WORK FOR HTML5
		//if (is_int64(_tag)) // EXPLICIT TAG -- e.g. TWEEN.DURATION (This can help reduce calling overhead)
		//{
		//	if (is_string(args[++i]))
		//	{
		//		var _cache = global.TGMS.Cache[? args[i]];
				
		//		if (is_undefined(_cache))
		//		{
		//			_cache = TGMS_StringStrip(args[i]);
		//		}
				
		//		switch(_tag)
		//		{
		//			case TWEEN.EASE: _t[@ TWEEN.EASE] = TGMS.ShortCodesEase[? _cache]; break;
		//			case TWEEN.MODE: _t[@ TWEEN.MODE] = TGMS.ShortCodesMode[? _cache]; break;
		//		}
		//	}
		//	else
		//	{
		//		_t[@ _tag] = args[i];
		//	}
		//}
		//else 
		
		if (is_struct(_tag)) // IF Advanced Property
		{
			_tag = _tag.data;
			var _argCount = array_length(_tag);
			var _command = _tag[0];
			var _toFrom = 58; // ":" default
			var _firstArg = _argCount > 1 ? _tag[1] : 0;

			if (_command == TPTarget) // TGMS_TODO: See if this can be moved into target setter function
			{	
				_firstArg = _tag[2];
				_tag[@ 2] = _tag[1];
				var _lastByte = string_byte_at(_firstArg, string_length(_firstArg));
			
				//if (">" or "<" or ":")
				if (_lastByte <= 62 && _lastByte >= 58)
				{	
					_toFrom = _lastByte;
					_firstArg = string_delete(_firstArg, string_length(_firstArg), 1);
				}
			}
			else
			if (is_string(_firstArg))
			{
				_firstArg = _argCount >= 2 ? _tag[1] : undefined;
				
				var _lastByte = string_byte_at(_firstArg, string_length(_firstArg));
			
				//if (">" or "<" or ":")
				if (_lastByte <= 62 && _lastByte >= 58)
				{	
					_toFrom = _lastByte;
					_firstArg = string_delete(_firstArg, string_length(_firstArg), 1);
				}

				// NOTE: This might need to be updated for proper target handling
				if (_command == TPUser)
				{
					_firstArg = TGMS_Variable_Get(id, _firstArg, id, other);
				}
			}
				
			switch(_argCount)
			{
			case 1: _tag = [_command, undefined]; break;
			case 2: _tag = ds_map_exists(TGMS.PropertyNormals, _command) ? [_command, [_firstArg, 0, 0]] : [_command, _firstArg]; break;
			default:
				var _xargs = array_create(_argCount-1);
				var _iArg = 0;
				_xargs[0] = _firstArg;
				
				repeat(_argCount-2)
				{
					++_iArg;
					_xargs[_iArg] = _tag[_iArg+1];
				}
				
				_tag = [_command, _xargs];
			}
			
			// Increment pData index
			_p += 3;
			
			switch(_toFrom)
			{
			case 58: // : Default
				i += 2; 
				array_push(_pData, _tag, args[i-1], args[i]);
			break;
				
			case 62: // > To
				array_push(_pData, _tag, STR_AT, args[++i]);
			break; 
				
			case 60: // < From
				array_push(_pData, _tag, args[++i], STR_AT);
			break;
			}
		}
		else // NOT ADVANCED PROPERTY
		{		
			/*
				// CHECK TO SEE IF THIS IS BEING CALLED FOR ALL TAGS???? EVEN VARIABLES!!
				THERE COULD A PROBLEM HERE WHERE CACHE CREATES A FALSE POSITIVE?
			*/
			//var _argLabel = _tag_is_string ? TGMS_ArgumentLabels[? TGMS_StringStrip(_tag)] : _tag;
			
			var _tag_is_string = is_string(_tag);
			var _argLabel;
			
			if (_tag_is_string)
			{
				_argLabel = TGMS.Cache[? _tag];
				
				if (is_undefined(_argLabel))
				{
					_argLabel = TGMS_StringStrip(_tag);
				}
				
				_argLabel = TGMS.ArgumentLabels[? _argLabel];
				
				//else
				//{
					//_argLabel = _cache
				//}
				
				//if (ds_map_exists(global.TGMS.Cache, _tag))
				//{
				//	_argLabel = global.TGMS.Cache[? _tag];	
				//}
				//else
				//{
				//	_argLabel = TGMS_StringStrip(_tag);	
				//}
			}
			else
			{
				_argLabel = _tag;	
			}
			
			// NEW
			if (is_numeric(_argLabel)) // ENUMS?
			{
				//if (is_string(_tag))
				if (_tag_is_string)
				{
					switch(string_byte_at(_tag, 1))
					{						
						// "@" Event Callback -- This makes sure that we use the right assigned target before actually adding the callbacks later in this function
						case 64:
							/* TODO: Add "@update" 
							if (_argLabel == -1) // THIS MEANS "@update" -- special case
							{
								array_push(_pData, _tag, args[i-1], args[i]);
							}
							else
							*/
							{
								if (_qCallbacks == -1) { _qCallbacks = ds_queue_create(); }
								ds_queue_enqueue(_qCallbacks, _argLabel, args[++i]);
							}
						break;
				
						// ">" Count OR ">>" Chain
						case 62:
							// Check for ">count" first
							if (string_length(_tag) == 1)
							{
								_t[@ _argLabel] = args[++i];
								break;
							}
					
							// Else we have a chain
							_doStart = false; // Tell system not to play the tween right away
				
							if (i < _paramCount && is_real(args[i+1])) // ADD PLAY CHAIN TO SELECTED TWEEN
							{	
								++i;
								TweenAddCallback(args[i] ? args[i] : args[i]+TGMS.TweenIndex, TWEEN_EV_FINISH, _sharedTweener, TweenPlay, _tID); // Use index based on whether or not greater than 0 ... -1
							}
							else // < ADD PLAY CHAIN TO PREVIOUSLY CREATED TWEEN
							{	
								TweenAddCallback(TGMS.TweenIndex-1, TWEEN_EV_FINISH, _sharedTweener, TweenPlay, _tID);
							}
						break;
						
						////////////// IS THIS ACTUALLY BEING CALLED? WHAT IS GOING ON HERE??? IS THIS DUPLICATED???!!!
						default:
							var _nextArg = args[++i];
				
							if (is_string(_nextArg))
							{
								var _cache = TGMS.Cache[? _nextArg];
				
								if (is_undefined(_cache))
								{
									_cache = TGMS_StringStrip(_nextArg);
								}
								
								switch(_argLabel)
								{
									case TWEEN.EASE: _t[@ TWEEN.EASE] = TGMS.ShortCodesEase[? _cache]; break;
									case TWEEN.MODE: _t[@ TWEEN.MODE] = TGMS.ShortCodesMode[? _cache]; break;
								}
							}
							else
							{
								_t[@ _argLabel] = _nextArg;
							}
						break;
					} 
				}
				else
				{
					//var _nextArg = args[++i];

					//if (is_string(_nextArg))
					//{
					//	switch(_argLabel)
					//	{
					//		case TWEEN.EASE: _t[@ TWEEN.EASE] = TGMS.ShortCodesEase[? TGMS_StringStrip(_nextArg)]; break;
					//		case TWEEN.MODE: _t[@ TWEEN.MODE] = TGMS.ShortCodesMode[? TGMS_StringStrip(_nextArg)]; break;
					//	}
					//}
					//else
					//{
					//	_t[@ _argLabel] = _nextArg;
					//}
					
					//var _nextArg = args[++i];
				
					if (is_string(args[++i]))
					{
						var _cache = TGMS.Cache[? args[i]];
				
						if (is_undefined(_cache))
						{
							_cache = TGMS_StringStrip(args[i]);
						}
						
						switch(_argLabel)
						{
							case TWEEN.EASE: _t[@ TWEEN.EASE] = TGMS.ShortCodesEase[? _cache]; break;
							case TWEEN.MODE: _t[@ TWEEN.MODE] = TGMS.ShortCodesMode[? _cache]; break;
						}
					}
					else
					{
						_t[@ _argLabel] = args[i];
					}
				}
			}
			else // Shorthand setters
			{
				var _shortIndex = TGMS.ShorthandTable[string_byte_at(_tag, 1)];
				
				if (_shortIndex) // CHECK FOR SHORTHAND SETTER
				{			
					switch(_shortIndex)
					{
						case TWEEN.EASE: _t[@ TWEEN.EASE] = TGMS.ShortCodesEase[? TGMS_StringStrip(_tag, 1)]; break;
						case TWEEN.MODE: _t[@ TWEEN.MODE] = TGMS.ShortCodesMode[? TGMS_StringStrip(_tag, 1)]; break;
						default: _t[@ _shortIndex] = real(string_delete(_tag, 1, 1)); break;
					}
				}
				else // WE HAVE A VARIABLE PROPERTY
				{
					_p += 3;
					
					switch(string_byte_at(_tag, string_length(_tag)))
					{	
					case 62: // > To
						array_push(_pData, string_delete(_tag, string_length(_tag), 1), STR_AT, args[++i]);
					break; 
				
					case 60: // < From
						array_push(_pData, string_delete(_tag, string_length(_tag), 1), args[++i], STR_AT);
					break;
					
					case 58: // : To From
						i += 2; 
						array_push(_pData, string_delete(_tag, string_length(_tag), 1), args[i-1], args[i]); 
					break;
					
					default: // To From
						i += 2; 
						array_push(_pData, _tag, args[i-1], args[i]);
					}
				}
			}
		}
	}
	
	// Set tween's group scale to 1.0 if it doesn't exist yet
	if (_t[TWEEN.GROUP] != 0 && !ds_map_exists(TGMS.GroupScales, _t[TWEEN.GROUP]))
	{	
		TGMS.GroupScales[? _t[TWEEN.GROUP]] = 1.0;	
	}
	
	// Assign newly created variable data list
	_t[@ TWEEN.PROPERTY_DATA_RAW] = _pData;
	
	// Finalize used target -- create weak reference if struct and get id is instance
	_t[@ TWEEN.TARGET] = is_struct(_t[TWEEN.TARGET]) ? weak_ref_create(_t[TWEEN.TARGET]) : _t[TWEEN.TARGET].id;

	// FINALIZE USED EASE TYPE
	if (is_real(_t[TWEEN.EASE]))
	{
		if (_t[TWEEN.EASE] < 100000) // ANIMATION CURVE ID
		{
			_t[@ TWEEN.EASE] = animcurve_get_channel(_t[TWEEN.EASE], 0);
		}
		else // FUNCTION ID
		{
			_t[@ TWEEN.EASE] = method(undefined, _t[TWEEN.EASE]);
		}
	}
	else
	if (is_array(_t[TWEEN.EASE])) // Handle multi-ease-type tween
	{	
		if (is_string(_t[TWEEN.EASE][0]))	 // CONVERT EASE STRINGS INTO EXISTING EASE METHODS
		{
			_t[TWEEN.EASE][@ 0] = TGMS.ShortCodesEase[? TGMS_StringStrip(_t[TWEEN.EASE][0])];
		}
		else
		if (is_real(_t[TWEEN.EASE][0]))		// CONVERT FUNCTION ID OR ANIMATION CURVE ID
		{
			if (_t[TWEEN.EASE][0] < 100000)
			{
				_t[TWEEN.EASE][@ 0] = animcurve_get_channel(_t[TWEEN.EASE][0], 0);
			}
			else
			{
				_t[TWEEN.EASE][@ 0] = method(undefined, _t[TWEEN.EASE][0]);
			}
		}
		
		if (is_string(_t[TWEEN.EASE][1]))
		{
			_t[TWEEN.EASE][@ 1] = TGMS.ShortCodesEase[? TGMS_StringStrip(_t[TWEEN.EASE][1])];
		}
		else
		{
			if (is_real(_t[TWEEN.EASE][1]))
			{
				if (_t[TWEEN.EASE][1] < 100000)
				{
					_t[TWEEN.EASE][@ 1] = animcurve_get_channel(_t[TWEEN.EASE][1], 0);
				}
				else
				{
					_t[TWEEN.EASE][@ 1] = method(undefined, _t[TWEEN.EASE][1]);
				}
			}
		}
		
		// Store Raw Tweens For Swapping
		_t[@ TWEEN.EASE_RAW] = _t[TWEEN.EASE];
		// Set Active Ease Function
		_t[@ TWEEN.EASE] = _t[TWEEN.EASE][0];
	}
	
	// Tween Mode with Continue Count
	if (is_array(_t[TWEEN.MODE]))
	{
		_t[@ TWEEN.CONTINUE_COUNT] = _t[TWEEN.MODE][1];
		_t[@ TWEEN.MODE] = is_real(_t[TWEEN.MODE][0]) ? _t[TWEEN.MODE][0] : TGMS.ShortCodesMode[? TGMS_StringStrip(_t[TWEEN.MODE][0])];
	}

	// Duration Swapping
	if (is_array(_t[TWEEN.DURATION]) && array_length(_t[TWEEN.DURATION]) == 2)
	{
		if (_t[TWEEN.DURATION][0] <= 0) { _t[TWEEN.DURATION][@ 0] = 0.0000001; }
		if (_t[TWEEN.DURATION][1] <= 0) { _t[TWEEN.DURATION][@ 1] = 0.0000001; }
		_t[@ TWEEN.DURATION_RAW] = _t[TWEEN.DURATION];
		_t[@ TWEEN.DURATION] = _t[TWEEN.DURATION][0];
	}
	
	// Delay and Rests
	if (is_array(_t[TWEEN.DELAY]))
	{
		_t[@ TWEEN.REST] = (array_length(_t[TWEEN.DELAY]) == 2) ? _t[TWEEN.DELAY][1] : [_t[TWEEN.DELAY][1], _t[TWEEN.DELAY][2]];
		_t[@ TWEEN.DELAY] = _t[TWEEN.DELAY][0];
	}	

	// HANDLE QUEUED CALLBACK EVENTS
	if (_qCallbacks != -1)
	{
		repeat(ds_queue_size(_qCallbacks) div 2)
		{
			var _event = ds_queue_dequeue(_qCallbacks);
			var _cbData = ds_queue_dequeue(_qCallbacks);
			
			if (!is_array(_cbData)) // REGULAR CALLBACK WITH ASSUMED TARGET BELONGING TO TWEEN
			{
				TweenAddCallback(_tID, _event, _t[TWEEN.TARGET], _cbData);
			}
			else // Handle advanced callback
			{
				// First argument is a method or function id -- _cbData[0] is first argument in array
				if (is_method(_cbData[0]) || is_real(_cbData[0]))
				{
					var _cArgs = array_create(array_length(_cbData) + 3);
					_cArgs[0] = _tID;
					_cArgs[1] = _event;
					_cArgs[2] = _t[TWEEN.TARGET];
					array_copy(_cArgs, 3, _cbData, 0, array_length(_cbData));
				}
				else // Undefined -- use original method environment
				if (is_undefined(_cbData[0])) 
				{
					var _cArgs = array_create(array_length(_cbData) + 2);
					_cArgs[0] = _tID;
					_cArgs[1] = _event;
					_cArgs[2] = method_get_self(_cbData[1]);
					array_copy(_cArgs, 3, _cbData, 1, array_length(_cbData)-1);
				}
				else // Explicit target using {target: some_target}
				if (is_struct(_cbData[0]))
				{	
					var _cArgs = array_create(array_length(_cbData) + 2);
					_cArgs[0] = _tID;
					_cArgs[1] = _event;
					
					if (_cbData[0] == self)
					{
						_cArgs[2] = self;
					}
					else
					if (_cbData[0] == other)
					{
						_cArgs[2] = other;
					}
					else
					{
						_cArgs[2] = _cbData[0].target;
					}
					
					array_copy(_cArgs, 3, _cbData, 1, array_length(_cbData)-1);
				}
				else // String -- "self" | "other"
				{
					var _cArgs = array_create(array_length(_cbData) + 2);
					_cArgs[0] = _tID;
					_cArgs[1] = _event;
					_cArgs[2] = _cbData[0];
					array_copy(_cArgs, 3, _cbData, 1, array_length(_cbData)-1);
				}
				
				// Execute TweenAddCallback() with defined arguments above
				script_execute_ext(TweenAddCallback, _cArgs);	
			}
		}

		// Destroy temp callback queue
		ds_queue_destroy(_qCallbacks);	
	}

	// Track Delay Start
	_t[@ TWEEN.DELAY_START] = _t[TWEEN.DELAY];

	// Return early for specific calls
	switch(script)
	{
		case TweenCreate: return _tID;
		case TweenDefine: return TGMS_TweenPreprocess(_t);
	}

	// Check for a delay
	if (_t[TWEEN.DELAY] != 0)
	{   
		if (_doStart)
		{
			_t[@ TWEEN.STATE] = TWEEN_STATE.DELAYED; // Set tween as waiting 
		}
	
		// Put to starting tween position right away if a negative delay is given ( NOTE: This is a hidden feature! )
		if (_t[TWEEN.DELAY] < 0)
		{
			_t[@ TWEEN.DELAY] = abs(_t[TWEEN.DELAY]);
			TGMS_TweenPreprocess(_t);
			TGMS_TweenProcess(_t, _t[TWEEN.TIME], _t[TWEEN.PROPERTY_DATA], is_real(_t[TWEEN.TARGET]) ? _t[TWEEN.TARGET] : _t[TWEEN.TARGET].ref);
		}
	
		ds_list_add(_sharedTweener.delayedTweens, _t); // Add tween to delay list
	}
	else // Start tween right away
	if (_doStart)
	{		
		if (_sharedTweener.inUpdateLoop)
		{
			ds_queue_enqueue(_sharedTweener.stateChanger, _t, _t[TWEEN.TARGET]);	
		}
		else
		{
			_t[@ TWEEN.STATE] = _t[TWEEN.TARGET]; // Set tween as active
		}
		
		// Pre-process tween data
		TGMS_TweenPreprocess(_t);
		// Process tween
		TGMS_TweenProcess(_t, _t[TWEEN.TIME], _t[TWEEN.PROPERTY_DATA],  is_real(_t[TWEEN.TARGET]) ? _t[TWEEN.TARGET] : _t[TWEEN.TARGET].ref); // TODO: Maybe allow this to be skipped ??
		// Execute "On Play" event
		TGMS_ExecuteEvent(_t, TWEEN_EV_PLAY);
	}

	// Return tween id
	return _tID;
}


function TGMS_TweenPreprocess(_t) 
{	/// @func TGMS_TweenPreprocess(rawTween)
	static STR_AT = "@";
	static STR_AT_PLUS = "@+";
	static STR_EMPTY = "";
	static STR_SPACE = " ";
	static STR_DOLLAR = "$";
	static STR_DOT = ".";
	static STR_self = "self";
	static STR_global = "global";
	static STR_other = "other";
	
	static TGMS_PropertySetters = global.TGMS.PropertySetters;
	static TGMS_PropertyNormals = global.TGMS.PropertyNormals;
	static TGMS_Variable_Struct_String_Set = global.TGMS.Variable_Struct_String_Set;
	static TGMS_Variable_Global_Set  = global.TGMS.Variable_Global_Set;
	static TGMS_Variable_Instance_Set = global.TGMS.Variable_Instance_Set;
	
	var _target = _t[TWEEN.TARGET];
	if (is_struct(_target)) { _target = _target.ref; }

	//var _pData = _t[TWEEN.PROPERTY_DATA_RAW];
	var _pData = [];
	array_copy(_pData, 0, _t[TWEEN.PROPERTY_DATA_RAW], 0, array_length(_t[TWEEN.PROPERTY_DATA_RAW]));
	
	var _propCount = array_length(_pData) div 3;
	var _extIndex = -3; // Careful with this!
	var _extData = array_create(1+_propCount*4); // Create array holding properties data
	_extData[0] = _propCount; // Cache property count in first index (Used later when processing)
	
	var _caller = _t[TWEEN.CALLER];
	if (is_struct(_caller)) { _caller = _caller.ref; }
					
	var _other = _t[TWEEN.OTHER];
	if (is_struct(_other)) { _other = _other.ref; }
	
	// ** PARSE PROPERTY STRINGS **
	var i = -1;
	repeat(_propCount)
	{
		var _variable = _pData[++i];
	
		repeat(2)
		{
			var _pValue = _pData[++i];
	
			if (is_real(_pValue))
			{
				continue;	
			}
		
			// RELATIVE ARRAY VALUES [100]
			if (is_array(_pValue))
			{	// Add "+" for positive numbers ... No "-" needed for negative numbers.
				_pValue = _pValue[0] >= 0 ? STR_AT_PLUS+string(_pValue[0]) : STR_AT+string(_pValue[0]);
			}
	
			// STRING VALUES (START DEST)
			if (is_string(_pValue))
			{	// Remove any spaces in string
				_pValue = string_replace_all(_pValue, STR_SPACE, STR_EMPTY); 

				// Check for initial minus sign
				var _preOp = 1;
				if (string_byte_at(_pValue, 1) == 45) // "-" minus
				{
					_preOp = -1;
					_pValue = string_delete(_pValue, 1, 1);
				}

				// TODO: THIS IS CAUSING AN ISSUE... POST-OP CAUSES PROBLEMS...
				var _op = 0;
				var _iOp = 1;
				repeat(string_length(_pValue)-2)
				{
					// TODO: add operator map check
					++_iOp;
					if (string_byte_at(_pValue, _iOp) != 46 && string_byte_at(_pValue, _iOp) <= 47 && string_byte_at(_pValue, _iOp) >= 42)
					{
						_op = _iOp;
						break;
					}
				}
			
				// Update TWEEN_SELF macro
				global.TGMS.tween_self = _t[TWEEN.ID];
			
				if (_op) // HANDLE MATH OPERATION
				{		
					var _pre = string_copy(_pValue, 1, _op-1);
					var _post = string_copy(_pValue, _op+1, string_length(_pValue)-_op);
	
					_pre = _pre == STR_AT ? TGMS_Variable_Get(_target, _variable, _caller, _other) : TGMS_Variable_Get(_target, _pre, _caller, _other);
					_post = _post == STR_AT ? TGMS_Variable_Get(_target, _variable, _caller, _other) : TGMS_Variable_Get(_target, _post, _caller, _other);

					// Perform operation
					switch(string_byte_at(_pValue, _op)) // TODO: Add _postOp??
					{
						case 43: _pData[i] = _preOp * _pre + _post; break; // + TODO:
						case 45: _pData[i] = _preOp * _pre - _post; break; // -
						case 42: _pData[i] = _preOp * _pre * _post; break; // *
						case 47: _pData[i] = _preOp * _pre / _post; break; // /
					}
				}
				else // NO MATH OPERATION
				{	
					_pData[i] = _pValue == STR_AT ? _preOp*TGMS_Variable_Get(_target, _variable, _caller, _other) : _preOp*TGMS_Variable_Get(_target, _pValue, _caller, _other);
				}
				
				// Clear TWEEN_SELF macro
				global.TGMS.tween_self = undefined;
			}
		}
	
		// ** PROCESS PROPERTY DATA VALUES **
		_extIndex += 4;
	
		if (is_array(_variable)) // ADVANCED PROPERTY
		{
			// Track raw property setter method
			_extData[_extIndex] = TGMS_PropertySetters[? _variable[0]];
			
			if (ds_map_exists(TGMS_PropertyNormals, _variable[0])) // NORMALIZED
			{	
				_extData[1+_extIndex] = 0; // normalized start
				_extData[2+_extIndex] = 1; // normalized change
				var _data = _variable[1];
				_data[@ 1] = _pData[i-1]; // start
				_data[@ 2] = _pData[i]; // change
			}
			else // NOT NORMALIZED
			{	
				_extData[1+_extIndex] = _pData[i-1]; // start
				_extData[2+_extIndex] = _pData[i] - _pData[i-1]; // change
			}
		
			_extData[3+_extIndex] = _variable[1]; // data
		}
		else // METHOD PROPERTY
		if (_target[$ STR_DOLLAR+_variable] != undefined)
		{
			_extData[  _extIndex] = _target[$ STR_DOLLAR+_variable];
			_extData[1+_extIndex] = _pData[i-1]; // start
			_extData[2+_extIndex] = _pData[i] - _pData[i-1]; // change
			_extData[3+_extIndex] = _variable; // data
		}
		else // OPTIMISED PROPERTY
		if (ds_map_exists(TGMS_PropertySetters, _variable))
		{
			_extData[_extIndex] = TGMS_PropertySetters[? _variable]; // Track raw property
			
			//if (!is_method(_variable) && ds_map_exists(global.TGMS.PropertyNormals, _variable))
			if (ds_map_exists(TGMS_PropertyNormals, _variable)) // NORMALIZED -- BUG IN HTML5 when checking method against a map?
			{
				_extData[1+_extIndex] = 0; // normalized start
				_extData[2+_extIndex] = 1; // normalized change
				_extData[3+_extIndex] = [_pData[i-1], _pData[i]];
			}
			else // NOT NORMALIZED
			{	
				var _start = _pData[i-1];
				_extData[1+_extIndex] = _start; // start
				_extData[2+_extIndex] = _pData[i] - _start; // change
				_extData[3+_extIndex] = undefined; // data
			}
		}
		else // DYNAMIC PROPERTY
		{
			// NEW -- CHECK FOR STRUCTURE -- May need to extend this to support indexing object data
			var _dotPos = string_pos(STR_DOT, _variable);
			
			if (!_dotPos) // Default Dynamic Property
			{
				_extData[  _extIndex] = _target[$ _variable] == undefined ? TGMS_Variable_Global_Set : TGMS_Variable_Instance_Set;
				_extData[1+_extIndex] = _pData[i-1]; // start
				_extData[2+_extIndex] = _pData[i] - _pData[i-1]; // change
				_extData[3+_extIndex] = _variable; // data
			}
			else // Handle dot notation
			{
				//
				// TODO: I could possibly check for optimised properties here???
				//			Might wait for next update when [target] is set for each property??
				//
				
				_extData[  _extIndex] = TGMS_Variable_Struct_String_Set; // Should rename this function for better clarity
				// These next 2 lines are potentionally redudant
				_extData[1+_extIndex] = _pData[i-1]; // start
				_extData[2+_extIndex] = _pData[i] - _pData[i-1]; // change
				
				// Handle difference between 1 or 2 dots used
				if (string_count(STR_DOT, _variable) == 1)
				{
					var _structName = string_copy(_variable, 1, _dotPos-1);
					// _extData[3+_extIndex] = undefined; // We will leave this undefined
				}
				else
				{
					var _sub_target = string_copy(_variable, 1, _dotPos-1);
					
					if (_target[$ _sub_target] != undefined)
					{
						_target = _target[$ _sub_target];
					}
					else
					if (_caller[$ _sub_target] != undefined)
					{
						_target = _caller[$ _sub_target];	
					}
					
					_variable = string_delete(_variable, 1, _dotPos);
					_dotPos = string_pos(STR_DOT, _variable);
					_structName = string_copy(_variable, 1, string_pos(STR_DOT, _variable)-1);
				}
				
				 // Lookup prefix inside target first
				if (_target[$ _structName] != undefined)
				{
					var _passTarget = _target[$ _structName];
						
					// Create weak reference if passed target is a struct
					if (!is_real(_passTarget)) 
					{
						_passTarget = weak_ref_create(_passTarget);	
					}
					
					// NEW =====================
					_variable = string_copy(_variable, _dotPos+1, 100);
					
					if (ds_map_exists(TGMS_PropertyNormals, _variable))
					{
						_extData[1+_extIndex] = 0; // start
						_extData[2+_extIndex] = 1; // change
						_extData[3+_extIndex] = [_passTarget, _variable, [_pData[i-1], _pData[i]], undefined]; // This gets sorted later for optimised properties in the slower generic setter
					}
					else
					{
						_extData[3+_extIndex] = [_passTarget, _variable, undefined, undefined]; // This gets sorted later for optimised properties in the slower generic setter
					}
				}
				else // Look for prefix inside caller
				if (_caller[$ _structName] != undefined) 
				{
					var _passTarget = _caller[$ _structName];
						
					if (!is_real(_passTarget))
					{
						_passTarget = weak_ref_create(_passTarget);	
					}
						
					// NEW =====================
					_variable = string_copy(_variable, _dotPos+1, 100);
					
					if (ds_map_exists(TGMS_PropertyNormals, _variable))
					{				
						_extData[1+_extIndex] = 0; // start
						_extData[2+_extIndex] = 1; // change
						_extData[3+_extIndex] = [_passTarget, _variable, [_pData[i-1], _pData[i]], undefined]; // This gets sorted later for optimised properties in the slower generic setter
					}
					else
					{
						_extData[3+_extIndex] = [_passTarget, _variable, undefined, undefined]; // This gets sorted later for optimised properties in the slower generic setter
					}
				}
				else
				if (variable_global_exists(_structName)) // Look for prefix in global scope
				{
					var _passTarget = variable_global_get(_structName);
						
					if (!is_real(_passTarget))
					{
						_passTarget = weak_ref_create(_passTarget);	
					}
						
					// NEW =====================
					_variable = string_copy(_variable, _dotPos+1, 100);
					
					if (ds_map_exists(TGMS_PropertyNormals, _variable))
					{
						_extData[1+_extIndex] = 0; // start
						_extData[2+_extIndex] = 1; // change
						_extData[3+_extIndex] = [_passTarget, _variable, [_pData[i-1], _pData[i]], undefined]; // This gets sorted later for optimised properties in the slower generic setter
					}
					else
					{
						_extData[3+_extIndex] = [_passTarget, _variable, undefined, undefined]; // This gets sorted later for optimised properties in the slower generic setter
					}
				}
				else
				{		
					var _objectID = asset_get_index(_structName);	
			
					if (_objectID >= 0 && object_exists(_objectID))
					{
						// NEW =====================
						_variable = string_copy(_variable, _dotPos+1, 100);
					
						if (ds_map_exists(TGMS_PropertyNormals, _variable))
						{
							_extData[1+_extIndex] = 0; // start
							_extData[2+_extIndex] = 1; // change
							_extData[3+_extIndex] = [_objectID.id, _variable, [_pData[i-1], _pData[i]], undefined]; // This gets sorted later for optimised properties in the slower generic setter
						}
						else
						{
							_extData[3+_extIndex] = [_objectID.id, _variable, undefined, undefined]; // This gets sorted later for optimised properties in the slower generic setter
						}	
					}
					else
					{
						// This is where we need to handle self and other
						if (string_count(STR_DOT, _variable) >= 2)
						{
							var _structName = string_copy(_variable, 1, _dotPos-1);
							var _passTarget;
							switch(_structName)
							{
								case STR_global:
									_variable = string_delete(_variable, 1, 7);
									_dotPos = string_pos(STR_DOT, _variable);
									_passTarget = variable_global_get(string_copy(_variable, 1, _dotPos-1));
								break;
								
								case STR_self:
									_variable = string_delete(_variable, 1, 5);
									_dotPos = string_pos(STR_DOT, _variable);
									_passTarget = _caller[$ string_copy(_variable, 1, _dotPos-1)];
								break;
								
								case STR_other:
									_variable = string_delete(_variable, 1, 6);
									_dotPos = string_pos(STR_DOT, _variable);
									_passTarget = _other[$ string_copy(_variable, 1, _dotPos-1)];
								break;
							}
							
							// Finalize used target -- create weak reference if struct or get id if instance
							_passTarget = is_struct(_passTarget) ? weak_ref_create(_passTarget) : _passTarget.id;
							
							_variable = string_copy(_variable, _dotPos+1, 100);
							
							// Normalized
							if (ds_map_exists(TGMS_PropertyNormals, _variable))
							{
								_extData[1+_extIndex] = 0; // start
								_extData[2+_extIndex] = 1; // change
								_extData[3+_extIndex] = [_passTarget, _variable, [_pData[i-1], _pData[i]], undefined];
							}
							else
							{
								_extData[3+_extIndex] = [_passTarget, _variable, undefined, undefined];
							}
						}
						else
						{	
							_variable = string_copy(_variable, _dotPos+1, 100);
							
							switch(_structName)
							{
							case STR_global:
								_extData[  _extIndex] = global.TGMS.Variable_Global_Set;
								_extData[3+_extIndex] = _variable;
							break;
							
							case STR_self:
								// NORMALIZED PROPERTY
								if (ds_map_exists(TGMS_PropertyNormals, _variable))
								{
									_extData[1+_extIndex] = 0; // start
									_extData[2+_extIndex] = 1; // change
									_extData[3+_extIndex] = [(is_struct(_caller) ? weak_ref_create(_caller) : _caller), _variable, [_pData[i-1], _pData[i]], undefined];
								}
								else // NOT NORMALIZED PROPERTY
								{
									_extData[3+_extIndex] = [(is_struct(_caller) ? weak_ref_create(_caller) : _caller), _variable, undefined, undefined];
								}
							break;
							
							case STR_other:
								// NORMALIZED PROPERTY
								if (ds_map_exists(TGMS_PropertyNormals, _variable))
								{
									_extData[1+_extIndex] = 0; // start
									_extData[2+_extIndex] = 1; // change
									_extData[3+_extIndex] = [(is_struct(_other) ? weak_ref_create(_other) : _other), _variable, [_pData[i-1], _pData[i]], undefined];
								} 
								else // NOT NORMALIZED PROPERTY
								{
									// NEW SINGLE
									_extData[3+_extIndex] = [(is_struct(_other) ? weak_ref_create(_other) : _other), _variable, undefined, undefined];
								}
							break;
							
							default:
								show_error("Invalid tween property prefix! Check preceding struct or object name.", false);
							}
						}
					}
				}
			}
		}		
	}
	
	// Assign property data values
	_t[@ TWEEN.PROPERTY_DATA] = _extData;
	
	// Handle per-step/second [durations]
	if (!is_real(_t[TWEEN.DURATION]))
	{
		var _duration = _t[TWEEN.DURATION];
		
		// Backwards support for [duration] array
		if (is_array(_duration) && array_length(_duration) == 1)
		{
			_duration = {rate: _duration[0]}
		}
		
		if (is_struct(_duration))
		{	
			var _data = _t[TWEEN.PROPERTY_DATA];
			var _struct = _duration;
			var _absChangeSum = 0;
			var _count = _struct[$ "use"];

			if (is_undefined(_count)) { _count = array_length(_data) div 4; }
		
			// Sum the absolute change values from each property
			var i = 3;	
			repeat(_count)
			{
				_absChangeSum += abs(_data[i]);
				i += 4;
			}
		
			if (!is_undefined(_struct[$ "rate"])) // Rate Duration
			{
				_t[@ TWEEN.DURATION] = _absChangeSum/_struct.rate;
			}
			else // Weighted Duration
			if (!is_undefined(_struct[$ "weight"]))
			{
				i = 3;
				var _output = 0;
				repeat(_count)
				{
					//var _rate_amount = _struct.weight * abs(_data[i]) / _absChangeSum;
					//_rate_amount = 1 + _rate_amount/_struct.weight;
					//_output += _data[i] * _rate_amount / _count;
				
					_output += abs(_data[i]) * (1 + (_struct.weight * abs(_data[i]) / _absChangeSum)/_struct.weight) / _count;
					i += 4;
				}
			
				_t[@ TWEEN.DURATION] = _output / _struct.weight;
			}
			else
			if (!is_undefined(_struct[$ "max"]))
			{	
				var _max = _data[3]; 
				i = 7;
				repeat(_count-1)
				{
					if (_data[i] > _max) 
					{ 
						_max = abs(_data[i]); 
					}
				
					i += 4;
				}
			
				_t[@ TWEEN.DURATION] = _max / _struct.max;
			}
			else
			if (!is_undefined(_struct[$ "min"]))
			{
				var _min = _data[3];
				i = 7;
				repeat(_count-1)
				{
					if (_data[i] < _min) 
					{ 
						_min = abs(_data[i]); 
					}
				
					i += 4;
				}
			
				_t[@ TWEEN.DURATION] = _min / _struct.min;
			}
			else // Divide Duration by Property Count "split"
			{
				_t[@ TWEEN.DURATION] = _absChangeSum/_struct.split/_count;
			}
		}
	}
		
	// Make sure we don't have an invalid duration [0]
	if (_t[TWEEN.DURATION] <= 0)
	{
		_t[@ TWEEN.DURATION] = 0.000000001;	
	}
	
	// Adjust for amount
	if (_t[TWEEN.AMOUNT] > 0)
	{
		_t[@ TWEEN.TIME] = _t[TWEEN.AMOUNT] * _t[TWEEN.DURATION];	
	}
	//else
	//{
	//	_t[@ TWEEN.TIME] = 0; // Moved into TweenPlay case up above... was preventing "=0.5" from working
	//}
}


function TGMS_TweenProcess(_t, time, data, target) 
{ 	
	switch(data[0]) // Property Count
	{
	case 1:
		if (is_method(_t[TWEEN.EASE]))
		{
			data[1](_t[TWEEN.EASE](time, data[2], data[3], _t[TWEEN.DURATION], _t), target, data[4], _t);
		}
		else
		{
			data[1](data[2] + data[3]*animcurve_channel_evaluate(_t[TWEEN.EASE], time/_t[TWEEN.DURATION]), target, data[4], _t); 
		}
	break;
	
	case 2:
		time = is_method(_t[TWEEN.EASE]) ? _t[TWEEN.EASE](time, 0, 1, _t[TWEEN.DURATION], _t) : animcurve_channel_evaluate(_t[TWEEN.EASE], time/_t[TWEEN.DURATION]);
		data[1](time*data[3]+data[2], target, data[4], _t);
		data[5](time*data[7]+data[6], target, data[8], _t);
	break;
	
	case 3:
		time = is_method(_t[TWEEN.EASE]) ? _t[TWEEN.EASE](time, 0, 1, _t[TWEEN.DURATION], _t) : animcurve_channel_evaluate(_t[TWEEN.EASE], time/_t[TWEEN.DURATION]);
	    data[1](time*data[3]+data[2], target, data[4], _t);
	    data[5](time*data[7]+data[6], target, data[8], _t);
	    data[9](time*data[11]+data[10], target, data[12], _t);
	break;

	case 4:
		time = is_method(_t[TWEEN.EASE]) ? _t[TWEEN.EASE](time, 0, 1, _t[TWEEN.DURATION], _t) : animcurve_channel_evaluate(_t[TWEEN.EASE], time/_t[TWEEN.DURATION]);
	    data[1](time*data[3]+data[2], target, data[4], _t);
	    data[5](time*data[7]+data[6], target, data[8], _t);
	    data[9](time*data[11]+data[10], target, data[12], _t);
	    data[13](time*data[15]+data[14], target, data[16], _t);
	break;
	
	case 5:
		time = is_method(_t[TWEEN.EASE]) ? _t[TWEEN.EASE](time, 0, 1, _t[TWEEN.DURATION], _t) : animcurve_channel_evaluate(_t[TWEEN.EASE], time/_t[TWEEN.DURATION]);
	    data[1](time*data[3]+data[2], target, data[4], _t);
	    data[5](time*data[7]+data[6], target, data[8], _t);
	    data[9](time*data[11]+data[10], target, data[12], _t);
	    data[13](time*data[15]+data[14], target, data[16], _t);
	    data[17](time*data[19]+data[18], target, data[20], _t);
	break;
	
	case 6:
		time = is_method(_t[TWEEN.EASE]) ? _t[TWEEN.EASE](time, 0, 1, _t[TWEEN.DURATION], _t) : animcurve_channel_evaluate(_t[TWEEN.EASE], time/_t[TWEEN.DURATION]);
	    data[1](time*data[3]+data[2], target, data[4], _t);
	    data[5](time*data[7]+data[6], target, data[8], _t);
	    data[9](time*data[11]+data[10], target, data[12], _t);
	    data[13](time*data[15]+data[14], target, data[16], _t);
	    data[17](time*data[19]+data[18], target, data[20], _t);
	    data[21](time*data[23]+data[22], target, data[24], _t);
	break;
	
	case 7:
		time = is_method(_t[TWEEN.EASE]) ? _t[TWEEN.EASE](time, 0, 1, _t[TWEEN.DURATION], _t) : animcurve_channel_evaluate(_t[TWEEN.EASE], time/_t[TWEEN.DURATION]);
	    data[1](time*data[3]+data[2], target, data[4], _t);
	    data[5](time*data[7]+data[6], target, data[8], _t);
	    data[9](time*data[11]+data[10], target, data[12], _t);
	    data[13](time*data[15]+data[14], target, data[16], _t);
	    data[17](time*data[19]+data[18], target, data[20], _t);
	    data[21](time*data[23]+data[22], target, data[24], _t);
	    data[25](time*data[27]+data[26], target, data[28], _t);
	break;
	
	case 8:
		time = is_method(_t[TWEEN.EASE]) ? _t[TWEEN.EASE](time, 0, 1, _t[TWEEN.DURATION], _t) : animcurve_channel_evaluate(_t[TWEEN.EASE], time/_t[TWEEN.DURATION]);
	    data[1](time*data[3]+data[2], target, data[4], _t);
	    data[5](time*data[7]+data[6], target, data[8], _t);
	    data[9](time*data[11]+data[10], target, data[12], _t);
	    data[13](time*data[15]+data[14], target, data[16], _t);
	    data[17](time*data[19]+data[18], target, data[20], _t);
	    data[21](time*data[23]+data[22], target, data[24], _t);
	    data[25](time*data[27]+data[26], target, data[28], _t);
	    data[29](time*data[31]+data[30], target, data[32], _t);
	break;
	
	case 9:
		time = is_method(_t[TWEEN.EASE]) ? _t[TWEEN.EASE](time, 0, 1, _t[TWEEN.DURATION], _t) : animcurve_channel_evaluate(_t[TWEEN.EASE], time/_t[TWEEN.DURATION]);
	    data[1](time*data[3]+data[2], target, data[4], _t);
	    data[5](time*data[7]+data[6], target, data[8], _t);
	    data[9](time*data[11]+data[10], target, data[12], _t);
	    data[13](time*data[15]+data[14], target, data[16], _t);
	    data[17](time*data[19]+data[18], target, data[20], _t);
	    data[21](time*data[23]+data[22], target, data[24], _t);
	    data[25](time*data[27]+data[26], target, data[28], _t);
	    data[29](time*data[31]+data[30], target, data[32], _t);
	    data[33](time*data[35]+data[34], target, data[36], _t);
	break;
	
	case 10:
		time = is_method(_t[TWEEN.EASE]) ? _t[TWEEN.EASE](time, 0, 1, _t[TWEEN.DURATION], _t) : animcurve_channel_evaluate(_t[TWEEN.EASE], time/_t[TWEEN.DURATION]);
	    data[1](time*data[3]+data[2], target, data[4], _t);
	    data[5](time*data[7]+data[6], target, data[8], _t);
	    data[9](time*data[11]+data[10], target, data[12], _t);
	    data[13](time*data[15]+data[14], target, data[16], _t);
	    data[17](time*data[19]+data[18], target, data[20], _t);
	    data[21](time*data[23]+data[22], target, data[24], _t);
	    data[25](time*data[27]+data[26], target, data[28], _t);
	    data[29](time*data[31]+data[30], target, data[32], _t);
	    data[33](time*data[35]+data[34], target, data[36], _t);
	    data[37](time*data[39]+data[38], target, data[40], _t);
	break;
	
	case 0: // Break out for tweens with no properties
	break;
	
	default: // Handle "unlimited" properties
		var i = 1;				
		if (is_method(_t[TWEEN.EASE])) // Function
		{	
			repeat(data[0])
			{
				data[i](_t[TWEEN.EASE](time, data[i+1], data[i+2], _t[TWEEN.DURATION], _t), target, data[i+3], _t);
				i += 4;
			}
		}
		else // Animation Curve
		{	
			repeat(data[0])
			{
				data[i](data[i+1] + data[i+2]*animcurve_channel_evaluate(_t[TWEEN.EASE], time/_t[TWEEN.DURATION]), target, data[i+3], _t);	
				i += 4;
			}
		}
	break;
	}
}


function TGMS_ExecuteEvent(_t, eventType, index=0, target=0) 
{	/// @desc Executes tween callback events -- DO NOT CALL THIS DIRECTLY!!
	// Set events map for TweenIs*() checks
	ds_map_set(global.TGMS.EventMaps[eventType], _t[TWEEN.ID], 0);

	// IF events and event type initialized...
	if (_t[TWEEN.EVENTS] != -1)
	{
	    if (ds_map_exists(_t[TWEEN.EVENTS], eventType))
	    {
	        // Get event data
	        eventType = _t[TWEEN.EVENTS][? eventType];
			// Track Tween Self
			global.TGMS.tween_self = _t[TWEEN.ID];
			
	        // Iterate through all event callbacks (isEnabled * event list size)
	        repeat(eventType[| 0] * (ds_list_size(eventType)-1))
	        {	
	            _t = eventType[| ++index]; // Cache callback -- THIS IS ACTUALLY A CALLBACK... REUSING 'tween' to avoid local 'var' variable overhead!
   
				// First check to see if callback is to be removed
				if (_t[TWEEN_CB.TWEEN] == TWEEN_NULL)
				{
					ds_list_delete(eventType, index--);	
				}
				else // Execute callback script with proper number of arguments
				if (_t[TWEEN_CB.ENABLED])
				{
					target = _t[TWEEN_CB.TARGET];
					
					// Do this part at callback execution
					if (is_struct(target))
					{
						if (weak_ref_alive(target))
						{
							target = target.ref;
						}
						else
						{
							continue;
						}
					}
					else
					if (!instance_exists(target))
					{
						continue;
					}
					
					// Execute callback
					switch(array_length(_t)-TWEEN_CB.ARG)
					{
						case 0:  method(target, _t[TWEEN_CB.SCRIPT])(); break;
						case 1:  method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG]); break;
						case 2:  method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1]); break;
						case 3:  method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2]); break;
						case 4:  method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3]); break;
						case 5:  method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4]); break;
						case 6:  method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5]); break;
						case 7:  method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6]); break;
						case 8:  method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7]); break;
						case 9:  method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8]); break;
						case 10: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9]); break;
						case 11: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10]); break;
						case 12: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10], _t[TWEEN_CB.ARG+11]); break;
						case 13: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10], _t[TWEEN_CB.ARG+11], _t[TWEEN_CB.ARG+12]); break;
						case 14: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10], _t[TWEEN_CB.ARG+11], _t[TWEEN_CB.ARG+12], _t[TWEEN_CB.ARG+13]); break;
						case 15: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10], _t[TWEEN_CB.ARG+11], _t[TWEEN_CB.ARG+12], _t[TWEEN_CB.ARG+13], _t[TWEEN_CB.ARG+14]); break;
						case 16: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10], _t[TWEEN_CB.ARG+11], _t[TWEEN_CB.ARG+12], _t[TWEEN_CB.ARG+13], _t[TWEEN_CB.ARG+14], _t[TWEEN_CB.ARG+15]); break;
						case 17: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10], _t[TWEEN_CB.ARG+11], _t[TWEEN_CB.ARG+12], _t[TWEEN_CB.ARG+13], _t[TWEEN_CB.ARG+14], _t[TWEEN_CB.ARG+15], _t[TWEEN_CB.ARG+16]); break;
						case 18: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10], _t[TWEEN_CB.ARG+11], _t[TWEEN_CB.ARG+12], _t[TWEEN_CB.ARG+13], _t[TWEEN_CB.ARG+14], _t[TWEEN_CB.ARG+15], _t[TWEEN_CB.ARG+16], _t[TWEEN_CB.ARG+17]); break;
						case 19: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10], _t[TWEEN_CB.ARG+11], _t[TWEEN_CB.ARG+12], _t[TWEEN_CB.ARG+13], _t[TWEEN_CB.ARG+14], _t[TWEEN_CB.ARG+15], _t[TWEEN_CB.ARG+16], _t[TWEEN_CB.ARG+17], _t[TWEEN_CB.ARG+18]); break;
						case 20: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10], _t[TWEEN_CB.ARG+11], _t[TWEEN_CB.ARG+12], _t[TWEEN_CB.ARG+13], _t[TWEEN_CB.ARG+14], _t[TWEEN_CB.ARG+15], _t[TWEEN_CB.ARG+16], _t[TWEEN_CB.ARG+17], _t[TWEEN_CB.ARG+18], _t[TWEEN_CB.ARG+19]); break;
						case 21: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10], _t[TWEEN_CB.ARG+11], _t[TWEEN_CB.ARG+12], _t[TWEEN_CB.ARG+13], _t[TWEEN_CB.ARG+14], _t[TWEEN_CB.ARG+15], _t[TWEEN_CB.ARG+16], _t[TWEEN_CB.ARG+17], _t[TWEEN_CB.ARG+18], _t[TWEEN_CB.ARG+19], _t[TWEEN_CB.ARG+20]); break;
						case 22: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10], _t[TWEEN_CB.ARG+11], _t[TWEEN_CB.ARG+12], _t[TWEEN_CB.ARG+13], _t[TWEEN_CB.ARG+14], _t[TWEEN_CB.ARG+15], _t[TWEEN_CB.ARG+16], _t[TWEEN_CB.ARG+17], _t[TWEEN_CB.ARG+18], _t[TWEEN_CB.ARG+19], _t[TWEEN_CB.ARG+20], _t[TWEEN_CB.ARG+21]); break;
						case 23: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10], _t[TWEEN_CB.ARG+11], _t[TWEEN_CB.ARG+12], _t[TWEEN_CB.ARG+13], _t[TWEEN_CB.ARG+14], _t[TWEEN_CB.ARG+15], _t[TWEEN_CB.ARG+16], _t[TWEEN_CB.ARG+17], _t[TWEEN_CB.ARG+18], _t[TWEEN_CB.ARG+19], _t[TWEEN_CB.ARG+20], _t[TWEEN_CB.ARG+21], _t[TWEEN_CB.ARG+22]); break;
						case 24: method(target, _t[TWEEN_CB.SCRIPT])(_t[TWEEN_CB.ARG], _t[TWEEN_CB.ARG+1], _t[TWEEN_CB.ARG+2], _t[TWEEN_CB.ARG+3], _t[TWEEN_CB.ARG+4], _t[TWEEN_CB.ARG+5], _t[TWEEN_CB.ARG+6], _t[TWEEN_CB.ARG+7], _t[TWEEN_CB.ARG+8], _t[TWEEN_CB.ARG+9], _t[TWEEN_CB.ARG+10], _t[TWEEN_CB.ARG+11], _t[TWEEN_CB.ARG+12], _t[TWEEN_CB.ARG+13], _t[TWEEN_CB.ARG+14], _t[TWEEN_CB.ARG+15], _t[TWEEN_CB.ARG+16], _t[TWEEN_CB.ARG+17], _t[TWEEN_CB.ARG+18], _t[TWEEN_CB.ARG+19], _t[TWEEN_CB.ARG+20], _t[TWEEN_CB.ARG+21], _t[TWEEN_CB.ARG+22], _t[TWEEN_CB.ARG+23]); break;
					}
					
					// NOTE: Document this change... 
					// Returning TGMS_REMOVE_CALLBACK from callback script will have the callback removed here after being called above ^
					//if (is_string(_t) && _t == TGMS_REMOVE_CALLBACK)
					//{
					//	ds_list_delete(eventType, index--);	
					//}
				}
	        }
			
			// Clear "TWEEN_SELF"
			global.TGMS.tween_self = undefined;
	    }
	}
}


function TGMS_TweensExecute() 
{	/// @func TGMS_TweensExecute(tweens, script, [args0, ...])
	/*
		INFO:
		    Iterates through all relevant tweens and executes a specified script for each.
		    Currently takes only a max of 3 optional arguments.
	*/

	var _tweens = SharedTweener().tweens;
	var _argCount = argument_count-2;
	var _tStruct = argument[0];
	var _script = argument[1];
	var _tIndex = -1;
	var _args = array_create(1 + _argCount);
	
	var _argIndex = 0;
	repeat(_argCount)
	{
		++_argIndex;
		_args[_argIndex] = argument[_argIndex+1];
	}
	
	// TARGET SELECT
	static STR_Target = "target";
	if (variable_struct_exists(_tStruct, STR_Target))
	{	
		if (is_array(_tStruct.target)) // ARRAY
		{
			repeat(ds_list_size(_tweens))
			{
			    var _t = _tweens[| ++_tIndex];
			    var _target = _t[TWEEN.TARGET];
						
				if (TGMS_TargetExists(_target)) 
				{
					var i = -1;
					repeat(array_length(_tStruct.target))
					{
						var _selectionData = _tStruct.target[++i];
						
						if (_selectionData == _tStruct) 
						{ 
							_selectionData = self; 
						}
						
						if (is_struct(_target)) // STRUCT
						{
							if (is_struct(_selectionData) && _target.ref == _selectionData)
							{
								_args[0] = _t;
								script_execute_ext(_script, _args, 0, 1+_argCount);
							}
						}
						else // INSTANCE
						if (!is_struct(_selectionData) && instance_exists(_selectionData)) 
						{ 
							_selectionData = _selectionData.id;
							
							if (_target == _selectionData || _target.object_index == _selectionData || object_is_ancestor(_target.object_index, _selectionData))
							{
								_args[0] = _t;
								script_execute_ext(_script, _args, 0, 1+_argCount);
							}
						}	
					}
				}
			}
		}
		else
		if (_tStruct.target == all) // All Targets
		{	
			repeat(ds_list_size(_tweens))
			{
				var _t = _tweens[| ++_tIndex];
	            
				if (TGMS_TargetExists(_t[TWEEN.TARGET]))
				{
					_args[0] = _t;
					script_execute_ext(_script, _args, 0, 1+_argCount);
				}
			}
		}
		else // Specific Target
		{
			var _selectionData = (_tStruct == _tStruct.target) ? self : _tStruct.target;
			
			if (!is_struct(_selectionData) && instance_exists(_selectionData))
			{
				_selectionData = _selectionData.id;	
			}
			
			repeat(ds_list_size(_tweens))
			{
				var _t = _tweens[| ++_tIndex];
		        var _target = _t[TWEEN.TARGET];
	
				if (TGMS_TargetExists(_target))
				{
					if (is_struct(_target))
					{
						if (_target.ref == _selectionData)
						{
							_args[0] = _t;
							script_execute_ext(_script, _args, 0, 1+_argCount);
						}
					}
					else
					if (is_real(_selectionData)) // Instance | Instance
					{
						if (_target == _selectionData || _target.object_index == _selectionData || object_is_ancestor(_target.object_index, _selectionData))
						{
							_args[0] = _t;
							script_execute_ext(_script, _args, 0, 1+_argCount);
						}
					}
				}
			}
		}
	}
	
	// GROUP
	static STR_Group = "group";
	if (variable_struct_exists(_tStruct, STR_Group))
	{	// SINGLE
		if (is_real(_tStruct.group))
		{
			var _tIndex = -1;
			var _selectionData = _tStruct.group;
        
			repeat(ds_list_size(_tweens))
			{
		        var _t = _tweens[| ++_tIndex];
		        if (_t[TWEEN.GROUP] == _selectionData && TGMS_TargetExists(_t[TWEEN.TARGET]))
				{
					_args[0] = _t;
					script_execute_ext(_script, _args, 0, 1+_argCount);	
				}
		    }
		}
		else // ARRAY
		{
			var _tIndex = -1;
			
			repeat(ds_list_size(_tweens))
			{
		        var _t = _tweens[| ++_tIndex];
				var i = -1;
				repeat(array_length(_tStruct.group))
				{	
					var _selectionData = _tStruct.group[++i];
					if (_t[TWEEN.GROUP] == _selectionData && TGMS_TargetExists(_t[TWEEN.TARGET]))
					{
						_args[0] = _t;
						script_execute_ext(_script, _args, 0, 1+_argCount);
					}
				}
		    }
		}
	}
	
	// TWEEN STRUCT IDS
	static STR_Tween = "tween";
	if (variable_struct_exists(_tStruct, STR_Tween))
	{
		var _tIndex = -1;
		var _tweens = _tStruct.tween;
		
		// SINGLE
		if (is_real(_tweens))
		{
			var _t = TGMS_FetchTween(_tweens);
		    if (is_array(_t) && TGMS_TargetExists(_t[TWEEN.TARGET]))
			{
				_args[0] = _t;
				script_execute_ext(_script, _args, 0, 1+_argCount);
			}
		}
        else // ARRAY
		{
			repeat(array_length(_tweens))
			{
		        var _t = TGMS_FetchTween(_tweens[++_tIndex]);
		        if (is_array(_t) && TGMS_TargetExists(_t[TWEEN.TARGET]))
				{
					_args[0] = _t;
					script_execute_ext(_script, _args, 0, 1+_argCount);
				}
		    }
		}
	}
	
	// TWEEN LISTS OR ARRAYS
	static STR_List = "list";
	if (variable_struct_exists(_tStruct, STR_List))
	{
		var _tIndex = -1;
		var _tweens = _tStruct.list;
		
		if (is_array(_tweens)) // array
		{
			repeat(array_length(_tweens))
			{
				var _t = TGMS_FetchTween(_tweens[++_tIndex]);
		        if (is_array(_t) && TGMS_TargetExists(_t[TWEEN.TARGET]))
				{
					_args[0] = _t;
					script_execute_ext(_script, _args, 0, 1+_argCount);
				}
			}
		}
		else // ds_list
		{
			repeat(ds_list_size(_tweens))
			{
				var _t = TGMS_FetchTween(_tweens[| ++_tIndex]);
		        if (is_array(_t) && TGMS_TargetExists(_t[TWEEN.TARGET]))
				{
					_args[0] = _t;
					script_execute_ext(_script, _args, 0, 1+_argCount);
				}
			}
		}
	}
}





