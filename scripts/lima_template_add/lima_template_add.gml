/// @param layer_id/name
function lima_template_add(_layer) {
    var _name, _id;
    
    #region Id o string
    if (is_string(_layer) ) {
        var _id   = layer_get_id(_layer);
        var _name = _layer;
    }
    else {
        var _name = layer_get_name(_layer);
        var _id = _layer;
    }
    #endregion
    
    // Agregar layer al mapa
    if (!ds_map_exists(global.__lima_templates, _name) ) {
        var _elements = layer_get_all_elements(_id);
        
        // Agregar referencia al layer en los componentes lima
        var i = 0; repeat(array_length(_elements) ) {
            var _in = _elements[i++];
            with (_in.lima) {
                __layer_id   =   _id;
                __layer_name = _name;
            }
        }
        
        global.__lima_templates[? _name] = _id;
    }
}

