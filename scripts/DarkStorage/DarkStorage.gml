global.__dark_storage = ds_map_create();    // El programador no debe de utilizarlo!

#macro dkfun function(caster, target, extras)

/// @param dark_key
/// @param dark_type
/// @param spell
function dark_add(_key, _type, _spellClass) {
    if (!dark_exists(_key) ) {    
        ds_map_add(global.__dark_storage, _key, _spellClass.SetType(_type).SelfKey(_key) );   
    }
    
    return (_spellClass);
}

/// @param dark_key
function dark_exists(_key) {
    return (ds_map_exists(global.__dark_storage, _key) );
}

/// @param dark_key
/// @param spell_component*
function dark_get(_key, _component) {
    return (is_undefined(_component) ) ? global.__dark_storage[? _key] : global.__dark_storage[? _key][$ _component];
}

/// @param sub_type
/// @param consume
/// @param include_caster?
/// @param targets
/// @param {array} conditions
function DarkSpell(_subtype, _consume = 0, _include = true, _targets = 1, _conditions) : MallComponent() constructor {
    __subtype = _subtype;   // El sub-tipo al que pertenece    
    __type    = "";         // Se agrega al final    
        
    __consume = _consume;  // Cuanto consume de algo (definido por usuario) al usar este hechizo
    __include = _include;
    __targets = _targets;
    
    __method = undefined;           // Funcion a usar
    __conditions = _conditions ?? [true, false];    // Condiciones a usar depende del programador como usarlo
    
    #region Metodos    
    /// @param type
    static SetType = function(_type) {__type = _type; return self; }
    
    /// @param method
    static SetSpell = function(_method) {
        __method  =  method(undefined, _method);    // Bindear funcion
        
        return self;
    }
    
    /// @param include
    /// @param targets
    static SetOptions = function(_include, _targets) {
        __include = _include;
        __targets = _targets;
        
        return self;
    }
    
    #endregion    
}

function DarkEffect() : MallComponent() constructor {
    id = "DE000"; // ID unica del efecto
    
    __key = -1;                 // A que estadistica/estado afecta
    __start = [false, MN.B, 0]; // Valor que coloca al inicio   
    __value = [0, MN.R, 0]; // Valor que va aumentando con cada ciclo (__turns)
    
    /* Ejemplo: 
        Veneno: start [true, MN.B, 0]
                value [15, MB.P, 0] 
    */
    
    __turns = (new Counter() ).ToggleIterate();    
}

#macro DARK_TYPE_MAGIC "MAGIA"
#macro DARK_TYPE_TICK  "TICK"

/// En esta funcion se incializan los hechizos
function dark_database() {
        
    dark_add("DARK.WSPELL.HEAL1", DARK_TYPE_MAGIC, (new DarkSpell("W.S", 20) ).SetSpell(dkfun {
            
    }) );    
        
    dark_add("DARK.GSPELL.VENENO", DARK_TYPE_TICK, (new DarkSpell("G.S",  0) ).SetSpell(dkfun {
        
    }) );
        
        
        
}







