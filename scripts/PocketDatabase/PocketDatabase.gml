global.__pocket = [];
global.__pocket_database = ds_map_create(); // Guardar informacion

#macro POCKET_DATABASE_DUMMY "POCKET.DUMMY"

/// @desc Inicia la base de datos de los objetos (Acá se deben crear los objetos)
function pocket_data_init() {
    pocket_create_item(POCKET_DATABASE_DUMMY, new PocketItem("DUMMY") );
    
    pocket_create_item("ESPADA.HIERRO", new PocketItem("ESPADAS", 100, 60, "FUE", 10, MN.R) );
    pocket_create_item("GUANTES.CUERO", new PocketItem("GUANTES",  50, 25, "FUE",  3, MN.R, "DEF", 6, MN.R) );    
}


/// @param key
/// @param {PocketItem} pocket_item
/// @desc Crea un objeto y lo agrega a la base de datos
function pocket_create_item(_key, _item) {
    if (!pocket_exists(_key) ) {
        global.__pocket_database[? _key] = _item.SetKey(_key).SetTranslate();  
        return (_item);
    } else {
        show_error("POCKET: ITEM" + string(_key) + "YA EXISTE", true);
    }    
    
    return undefined;    
}

/// @param item_key
function pocket_exists(_key) {
    return (ds_map_exists(global.__pocket_database, _key) );
}

/// @param item_key
function pocket_data_get(_key) {
    return (global.__pocket_database[? _key] );
}
