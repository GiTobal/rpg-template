/// @param subtype
/// @param buy
/// @param sell
/// @param stat_key*
/// @param stat_value*
/// @param ...
function PocketItem(_subtype, _buy, _sell) : MallComponent() constructor {
    __type = mall_search_item(_subtype);    // Buscar tipo esto
    __subtype = _subtype;    
    
    __canBuy  = true;    
    __buy  = _buy ;
    
    __sell = _sell;
    __canSell = true;    
    
    __parts = 1;    // Cuantas partes necesita para ser equipado
    
    __stats    = {};    // Donde se guardan sus estadisticas    
    __statsInv = {};    // Las estadisticas de arriba invertidas  
    
    __returnInv = false; // Devolver las estadisticas invertidas(true) o normales (false)
     
    // Effectos
    __effects = {   
        inEquip:        MALL_DUMMY_METHOD,  // Metodo que se ejecuta al equiparse este objeto     
        inDesequip:     MALL_DUMMY_METHOD,  // Metodo que se ejecuta al desequiparse este objeto
        inEquipUpdate:  MALL_DUMMY_METHOD,  // Metodo que se ejecuta cada ciclo mientras se mantenga este objeto (fuera de la batalla)
        
        // En Batallas
        inBattleStart : MALL_DUMMY_METHOD,  
        inBattleUpdate: MALL_DUMMY_METHOD,
        inBattleEnd:    MALL_DUMMY_METHOD,
        
        // En turnos
        inTurnStart:    MALL_DUMMY_METHOD,  
        inTurnUpdate:   MALL_DUMMY_METHOD,
        inTurnEnd:      MALL_DUMMY_METHOD
    };

    #region Metodos
    
    /// @param stat_key
    /// @param value
    /// @param number_type
    // Pone valores a las estadisticas
    static SetStat = function(_statKey, _value, _type = MN.R) {
        if (argument_count < 4) {
            __stats   [$ _statKey] = mall_data(_value, _type);
            __statsInv[$ _statKey] = mall_data(_value * -1, _type);
        } else {
            // Varios argumentos
            var i = 0; repeat(argument_count) {
                var _key = argument[i++];
                var _val = argument[i++];
                var _typ = argument[i++];
                
                SetStat(_key, _val, _typ);    
            }
        }   
        
        return self;
    }
    
    
    /// @param {struct}
    /// Permite devolver las estadisticas de este objeto, como tambien las estadisticas invertidas
    static GetStats = function() {
        var _ret = !__returnInv ? __stats : __statsInv;
        __returnInv = false;
        
        return _ret;
    }
    
    /// @param can_sell?
    /// @param sell
    /// @param can_buy?
    /// @param buy
    static SetTrade  = function(_cansell = true, _sell, _canbuy = true, _buy) {
        __canSell = _cansell;    
        __sell = _sell;
        
        __canBuy = _canbuy;
        __buy = _buy;
        
        return self;
    }
    
    /// @param effect_name
    /// @param value
    static SetEffect = function(_name, _value) {
        __effects[$ _name] = _value;    
        return self;    
    }
    
    // Cuantas partes necesita
    static SetEquip = function(_use = 1) {
        __parts = _use;
        return self;
    }
    
    #endregion

    // Iniciar estadisticas rapidamente
    var i = 3; repeat( (argument_count - 3) div 3) {
        var _key = argument[i++];
        var _val = argument[i++];
        var _typ = argument[i++];
        
        SetStat(_key, _val, _typ);
    }
}



