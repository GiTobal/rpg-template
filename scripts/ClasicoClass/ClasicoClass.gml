#region Global

/**	@return {Struct.__ClasicoStruct__}	*/
function __ClasicoStruct__() constructor {
	/// @ignore
	__is = asset_get_index(instanceof(self) );
	
	static Copy = function() {}
}

#macro CLCOUNTER_AMOUNT 1


#endregion

#region Funcionan con cordenadas
/// @param x
/// @param y
/// @desc trabaja como un punto en un plano o como un vector


/**
	@desc Trabaja como un punto en un plano o como un vector
	@param {Real} x
	@param {Real} y
	@return {Struct}
*/
function Vector2(_x = 0, _y = 0) : __ClasicoStruct__() constructor {
	x = _x;
	y = _y;
	
	/// @type {Real}		
	_xo = 0;	// Origen x
	/// @type {Real}
	_yo = 0;	// Origen y
	
	#region Metodos
	
	/**
	@param {Real} x		posicion x
	@param {Real} [y]	posicion y
	@return {Struct.Vector2}
	*/
	static SetOrigin = function(_x, _y = _x) {
		_xo = _x;
		_yo = _y;
		
		return self;
	}
	
	/** @return {Bool} */
	static IsOrigin = function() {
		return (x == _xo) && (y == _yo);
	}		
	
	/**
	@param {Mixed}	x	posicion x o vector2
	@param {Real}	[y] posicion y
	@return {Struct.Vector2}
	*/
	static SetXY = function(_x, _y = _x) {
		if (is_vector2(_x) ) {
			x = _x.x;		
			y = _y.y;	
		} 
		else {
			x = _x;	
			y = _y;
		}
		
		return self;
	}
	
	/**
	@param {Mixed} x posicion o vector2
	@return {Struct.Vector2}
	*/
	static SetX  = function(_x) {
		x = (!is_vector2(_x) ) ? _x : _x.x;
		
		return self;
	}
	
	/**
	@param {Mixed} y posicion o vector2
	@return {Struct.Vector2}
	*/	
	static SetY  = function(_y) {
		y = (!is_vector2(_y) ) ? _y : _y.y;
		
		return self;
	}
	
	/**
	@param {Real} x
	@param {Real} y
	@return {Struct.Vector2}
	*/
	static With = function(_x, _y) {
		return (Copy() ).SetXY(_x, _y);
	}
	
	/**
	@param {Real} x posicion horizontal
	@return {Struct.Vector2}
	*/
	static WithX = function(_x) {
		return (Copy() ).SetX(_x);
	}
	
	/**
	@param {Real} y posicion vertical
	@return {Struct.Vector2}
	*/		
	static WithY = function(_y) {
		return (Copy() ).SetY(_y);		
	}

	/**
	@param {Mixed}	x	posicion horizontal o vector2
	@param {Real}	[y]	posicion vertical
	@return {Struct.Vector2}
	*/
	static Add = function(_x, _y = 0) {
		if (!is_vector2(_x) ) {
			x += _x;
			y += _y;
		}
		else {
			x += _x.x;	
			y += _x.y;
		}
		
		return self;
	}

	/**
	@param {Mixed} x	posicion horizontal o vector2
	@param {Real}  [y]	posicion vertical
	@return {Struct.Vector2}
	*/	
	static Multiply = function(_x, _y = _x) {
		if (!is_vector2(_x) ) {
			x *= _x;	
			y *= _y;	
		}
		else {
			x *= _x.x;
			y *= _x.y;
		}
		
		return self;
	}
	
	/**
	@param {Mixed} x	posicion horizontal o vector2
	@param {Real}  [y]	posicion vertical
	@return {Struct.Vector2}
	*/	
	static Division = function(_x, _y = _x) {
		if (!is_vector2(_x) ) {
			x /= max(1, _x);	
			y /= max(1, _y);	
		}
		else {
			x /= max(1, _x.x);
			y /= max(1, _x.y);
		}
		
		return self;
	}

	/**
	@desc Obtener la longitud desde el origen al punto
	@return {Real}
	*/
	static Length = function() {
		return (point_distance(_xo, _yo, x, y) );
	}
	
	/**
	@param {Struct.Vector2} Vector2
	@return {Real}
	*/
	static LengthTo = function(_point) {
		return (point_distance(x, y, _point.x, _point.y) );
	}

	/** @return {Real} */
	static GetX = function() {return x; }
	
	/** @return {Real} */
	static GetY = function() {return y; }
	
	/** @return {Real} */
	static GetAngle = function() {return darctan(y / x); }
	
	/**
	@param {Real} delta_x
	@param {Real} [delta_y]
	@return {Struct.Vector2}
	*/
	static Translated = function(_delta_x, _delta_y = _delta_x) {
		return (Copy() ).Add(_delta_x, _delta_y);
	}
	
	/**
	@return {Real}
	*/
	static Angle = function() {
		return darctan2( (_yo - y) , (_xo - x) );
	}

	/**
	@param {Struct.Vector2} point
	@return {Real}
	*/
	static AngleTo = function(_point) {
		return darctan2( (_point.y - y) , (_point.x - x) );
	}
	
	/**
	@param {Struct.Vector2} point
	@return {Real}
	*/
	static DotProduct = function(_point) {
		return (dot_product(x, y, _point.x, _point.y) );
	}
	
	/**
	@param {Real} x		posicion horizontal o Vector2
	@param {Real} [y]	posicion vertical
	@return {Real}
	*/
	static Cross = function(_x, _y) {
		if (is_numeric(_x) ) {
			return ((x * _y) - (y * _x) );	
		} else {
			return ((x * _x.x) - (y * _x.y) );
		}
	}

	// SignTest(_Ax, _Ay, _Bx, _By, _Lx, _Ly) {
	// 	return ((_Bx - _Ax) * (_Ly - _Ay) - (_By - _Ay) * (_Lx - _Ax));
	// }
	
	/** @return {Struct.Vector2}	*/
	static Normalized = function() {
		var len = Length();
		
		return (new Vector2( (x - _xo) / len, (y - _yo) / len) );
	}

	/**	@return {String}	*/
	static toString = function() {
		return "x: " + string(x) + "\n y: " + string(y);
	}
	
	/**
		@desc Copia al vector en origen 0, 0
		@return {Struct.Vector2}
	*/
	static Copy = function() {
		return (new Vector2(x, y) );
	}

	#endregion
}

/// @param x
/// @param y
/// @param z
function Vector3(_x, _y, _z) : __ClasicoStruct__() constructor {
    x = _x;
    y = _y;
    z = _z;
    
    // Origenes
    xo = 0;
    yo = 0;
    zo = 0;
    
    #region Metodos
    	#region Basic
    /// @param x (other vector)
    /// @param y
    /// @param z
    static Add = function(_x, _y, _z) {
    	if (is_numeric(_x) ) {
    		x += _x;
    		y += _y;
    		z += _z;
    		
    	} else {
    		x += _x.x;
    		y += _x.y;
    		z += _x.z;
    		
    	}
    	
    	return self;
    }
    	
    /// @param x (other vector)
    /// @param y
    /// @param z
    /// @returns {Vector3}
    static Added = function(_x, _y, _z) {
    	if (is_numeric(_x) ) {
    		return (new Vector3(x + _x, y + _y, z + _z) );
    	} else {
    		return (new Vector3(x + _x.x, y + _x.y, z + _x.z) );
    	}
    	
    	return self;
    }    	
	
	/// @param scale
	static Multiply = function(_scale) {
    	if (is_numeric(_x) ) {
    		x *= _scale;
    		y *= _scale;
    		z *= _scale;
    		
    	} else {
    		x *= _scale.x;
    		y *= _scale.y;
    		z *= _scale.z;
    	}		
    	
    	return self;
	}
	
	/// @param scale
	/// @returns {Vector3}
	static Multiplied = function(_scale) {
    	if (is_numeric(_x) ) {
    		return (new Vector3(x * _scale, y * _scale, z * _scale) );
    	} else {
    		return (new Vector3(x * _scale.x, y * _scale.y, z * _scale.z) );
    	}			
	}

	/// @param scale
	static Divide = function(_scale) {
    	if (is_numeric(_x) ) {
    		x /= _scale;
    		y /= _scale;
    		z /= _scale;
    		
    	} else {
    		x /= _scale.x;
    		y /= _scale.y;
    		z /= _scale.z;
    	}		
    	
    	return self;
	}
	
	/// @param scale
	/// @returns {Vector3}
	static Divided = function(_scale) {
    	if (is_numeric(_x) ) {
    		return (new Vector3(x / _scale, y / _scale, z / _scale) );
    	} else {
    		return (new Vector3(x / _scale.x, y / _scale.y, z / _scale.z) );
    	}			
	}
	
	#endregion

    /// @param x (other vector)
    /// @param y
    /// @param z    
    static Cross = function(_x, _y, _z) {
		if (is_numeric(_x) ) {
			x = (y * _z) - (z * _y);	
			y = (z * _x) - (x * _z);
			z = (x * _y) - (y * _x);
		} else {
			x = (y * _x.z) - (z * _x.y);	
			y = (z * _x.x) - (x * _x.z);
			z = (x * _x.y) - (y * _x.x);			
		}	  
    }
    
    /// @param x (other vector)
    /// @param y
    /// @param z
    /// @returns {Vector3}
    static Crossed = function(_x, _y, _z) {
		if (is_numeric(_x) ) {
			return (new Vector3((y * _z) - (z * _y), (z * _x) - (x * _z), (x * _y) - (y * _x) ) );
		} else {
			return (new Vector3((y * _x.z) - (z * _x.y), (z * _x.x) - (x * _x.z), (x * _x.y) - (y * _x.x) ) );
		}	      	
    }

    /// @param x (other vector)
    /// @param y
    /// @param z      
    static Dot = function(_x, _y, _z) {
    	if (is_numeric(_x) ) {
    		return (dot_product_3d(x, y, z, _x, _y, _z) );	
    	} else {
    		return (dot_product_3d(x, y, z, _x.x, _x.y, _x.z) );
    	}
    }
    
	/// @returns {number}     
    static Magnitude  = function() {
    	return (point_distance_3d(0, 0, 0, x, y, z) );	
    }
    
	/// @param x (other vector)
    /// @param y
    /// @param z 
    static DistanceTo = function(_x, _y, _z) {
		if (is_numeric(_x) ) {
			return (point_distance_3d(_x, _y, _z, x, y, z) );	
		} else {
			return (point_distance_3d(_x.x, _x.y, _x.z, x, y, z) );
		}
		
    }
    
    static Normalize  = function() {
        var _l = Magnitude();
        
        x = (x / _l);
        y = (y / _l);
        z = (z / _l);
        
        return self;
    }
    
    /// @returns {Vector3}
    static Normalized = function() {
        var _l = Magnitude();

        return (new Vector3(x / _l, y / _l, z / _l) );    	
    }

    /// @returns {Vector3}   
    static Abs = function() {
        return (new Vector3(abs(x), abs(y), abs(z) ) );    	
    }
    
    /// @param vector3
    /// @returns {bool}
    static Equals = function(_vec3) {
        return (x == _vec3.x) && (y == _vec3.y) && (z == _vec3.z);    		
    }
    
    #endregion
    
}

/// @param x_start 
/// @param y_start
/// @param x_end
/// @param y_end
function Line(_xstart = 0, _ystart = 0, _xend = 0, _yend = 0) : __ClasicoStruct__() constructor {
	pos_start = new Vector2(_xstart, _ystart);  /// @is {Vector2}
	pos_end   = new Vector2(_xend  , _yend  );	/// @is {Vector2}
	
	#region Metodos
	
	static IsVertical   = function() {return (pos_start.x == pos_end.x); }
	static IsHorizontal = function() {return (pos_start.y == pos_end.y); }
	
	/// @returns {bool}	
	static IsInsideX = function(_x) {
		return ( (_x >= pos_start.x && _x <= pos_end.x) );	
	}

	/// @returns {bool}
	static IsInsideY = function(_y) {
		return ( (_y >= pos_start.y && _y <= pos_end.y) );
	}
	
	/// @param {Vector2} point
	/// @returns {bool}	
	static IsInsidePoint = function(_pn) {
		return ( (IsInsideX(_pn.x) && IsInsideY(_pn.y) ) );	
	}

	/// @param {Line} line
	/// @returns {bool}	
	static IsInsideLine  = function(_ln) {
		return (
			(IsInsideX(_ln.pos_start.x) || IsInsideX(_ln.pos_end.x) ) &&
			(IsInsideY(_ln.pos_start.y) || IsInsideY(_ln.pos_end.y) )
		);	
	}

		#region Basics
	/// @desc Da vuelta el inicio con el final
	static Reverse = function() {
		var _newstart = pos_end  .Copy();
		var _newend   = pos_start.Copy();
	
		pos_start = _newstart;
		pos_end   = _newend  ;
		
		gc_collect();
		
		return self;
	}
	
	static Intersect = function(_ln) {
		var _a1 = pos_start.x - pos_end  .x;
		var _b1 = pos_end  .y - pos_start.y;
		var _c1 = _a1 + _b1;

		var _a2 = _ln.pos_start.x - _ln.pos_end  .x;
		var _b2 = _ln.pos_end  .y - _ln.pos_start.y;
		var _c2 = _a2 + _b2;		
		
		var _delta = (_a1 * _b2) - (_a2 * _b1);
		
		if (_delta == 0) return false;
		
		var _x = (_b2 * _c1 - _b1 * _c2) / _delta;
		var _y = (_a1 * _c2 - _a2 * _c1) / _delta;
		var _point = new Vector2(_x, _y);
		
		return (IsInsidePoint(_point) && _ln.IsInsidePoint(_point) );
	}
	
	#endregion
	
		#region Setters
	static SetVectorPoint = function(_pnStart, _pnEnd) {
		pos_start = _pnStart;
		pos_end   = _pnEnd  ;
		
		return self;
	}

	/// @param x
	/// @param y	
	static SetStart = function(_x, _y) {
		pos_start.SetXY(_x, _y);
		
		return self;
	}

	/// @param point
	static SetStartPoint = function(_pn) {
		pos_start.SetPoint(_pn);
		
		return self;
	}
	
	/// @param x
	/// @param y
	static SetEnd = function(_x, _y) {
		pos_end.SetXY(_x, _y);
		
		return self;
	}
	
	/// @param point
	static SetEndPoint = function(_pn) {
		pos_end.SetPoint(_pn);
		
		return self;
	}
	
	#endregion
	
		#region Getter´s
	/// @returns {Vector2}
	static GetStart = function() {
		return pos_start;
	}

	/// @returns {Vector2}	
	static GetEnd   = function() {
		return pos_end;
	}

	static GetStartX = function() {return pos_start.x; }
	static GetStartY = function() {return pos_start.y; }
	
	static GetEndX   = function() {return pos_end.x; }
	static GetEndY   = function() {return pos_end.y; }	

	
	static GetLength = function() {
		return pos_start.LengthTo(pos_end);
	}
	
	#endregion
	
		#region Misq
	static Copy = function() {
		return (new Line() ).SetVectorPoint(pos_start.Copy(), pos_end.Copy() );
	}
	
	#endregion
	
	#endregion
}

/// @param x
/// @param y
/// @param width
/// @param height
/// @param halign
/// @param valign
/// @desc Todos los cambios producidos en él afectan a la posicion de origen generalmente (x1, y1) que seria top-left
function Rectangle(_x = 0, _y = 0, _w = 1, _h = 1, _halign = fa_left, _valign = fa_top) : __ClasicoStruct__() constructor {
	// Limpiar manualmente cierta cosas
	gc_collect();
	
	w = abs(_w);
	h = abs(_h);
	
	wrel = 1;
	hrel = 1;
	
	wlast = w;
	hlast = h;
	
	x1 = _x;
	y1 = _y;
	
	x2 = x1 + _w;
	y2 = y1 + _h;
	
	angle = 0;	// °
	
	xrotate = (x1 + x2) * 0.5;
	yrotate = (y1 + y2) * 0.5;
	
	halign = _halign;
	valign = _valign;
	
	halign_last = _halign;
	valign_last = _valign;
	
	#region Metodos
		#region Funciones basicas
	/// @desc Alinea para estar concorde a su origen
	static Align = function() {

		return self;	
	}
	
	static Halign = function(_hal) {
		var _x = GetOriginX();
		
		halign = _hal;
			
		if (halign == fa_center) {
			if (halign_last == fa_center) {_x = 0; } else {_x = (w / 2); }
		}
		
		return _x;
	}
	
	static Valign = function(_val) {
		var _y = GetOriginY();
		
		valign = _val;

		if (valign == fa_middle) _y = h / 2;
		
		return _y;		
	}	
	
	/// @desc Suma un valor a la posicion de origen
	static Basic = function(_op_x, _op_y = _op_x) {
		var _x = GetOriginX(), _y = GetOriginY();
		
		return SetBounds(_x + _op_x, _y + _op_y);
	}
	
	/// @desc Multiplica a la posicion de origen
	static Multiply = function(_op_x, _op_y = _op_x) {
		return SetBounds(x1 * _op_x, y1 * _op_y);	
	}	
	
	/// @desc Divide a la posicion de origen.	
	static Division = function(_op_x, _op_y = _op_x) {
		return SetBounds(x1 / max(1, _op_x), y1 / max(1, _op_y) );	
	}

	/// @desc Multiplica el ancho y largo por un valor
	static Scale  = function(_scale) {
		return SetSize(w * _scale, h * _scale);
	}
	
	/// @param delta_x
	/// @param delta_y
	static Expand = function(_x, _y) {
		return SetBounds(x1 - _x, y1 - _y, w + (_x * 2), h + (_y * 2) );
	}

	/// @param delta_x
	/// @param delta_y	
	static Reduce = function(_x, _y) {
		return SetBounds(x1 + _x, y1 + _y, w - (_x * 2), h - (_y * 2) );
	}
	
	/// @param {number} angle
	static Rotate = function(_ang) {
		var _sin, _cos, _cx, _cy, _ox, _oy;
		
		if (_ang == undefined) _ang = angle;
		
		angle = _ang;

		_sin = dsin(angle); 
		_cos = dcos(angle);
		
		_cx = xrotate;
		_cy = yrotate;
		
		_ox = w;
		_oy = h;
		
		x1 = _cx + _ox * _cos - _oy * _sin;
		y1 = _cy + _ox * _sin + _oy * _cos;

		_cx += w; _cy += h;

		x2 = _cx + _ox * _cos - _oy * _sin;
		y2 = _cy + _ox * _sin + _oy * _cos;

		return self;
	}
	
	/// @param x1
	/// @param y1
	/// @param x2
	/// @param y2
	/// @param [scale]
	static Indent  = function(_x1, _y1, _x2 = _x1, _y2 = _y1, _s = 1) {
		#region Soporte otras clases
		if (is_rectangle(_x1) ) {
			var _r = _x1; // rectangulo
			_s = (!_y1) ? 1 : _y1;
			
			_x1 = _r.x1;
			_y1 = _r.y1;
			
			_x2 = _r.x2;
			_y2 = _r.y2;
			
		} else if (is_vector2(_x1) && is_vector2(_y1) ) {
			var _p1 = _x1, _p2 = _y1;
			_s = (!_x2) ? 1 : _x2;	

			_x1 = _p1.x; _y1 = _p1.y;
			_x2 = _p2.x; _y2 = _p2.y;
		}
		
		#endregion
		
		return SetCorners(x1 + (_x1 * _s), y1 + (_y1 * _s), x2 - (_x2 * _s), y2 - (_y2 * _s) );
	}

	/// @param x1
	/// @param y1
	/// @param x2
	/// @param y2
	/// @param scale	
	static Border = function(_x1, _y1, _x2 = _x1, _y2 = _y1, _s = 1) {
		#region Soporte otras clases
		if (is_rectangle(_x1) ) {
			var _r = _x1; // rectangulo
			_s = (!_y1) ? 1 : _y1;
			
			_x1 = _r.x1;
			_y1 = _r.y1;
			
			_x2 = _r.x2;
			_y2 = _r.y2;
			
		} else if (is_vector2(_x1) && is_vector2(_y1) ) {
			var _p1 = _x1, _p2 = _y1;
			_s = (!_x2) ? 1 : _x2;

			_x1 = _p1.x; _y1 = _p1.y;
			_x2 = _p2.x; _y2 = _p2.y;
		}
		
		#endregion		
		
		
		return SetCorners(x1 - (_x1 * _s), y1 - (_y1 * _s), x2 + (_x2 * _s), y2 + (_y2 * _s) );	
	}

	/// @param x
	/// @param y
	/// @desc Mueve este rectangulo a la posicion (_x, _y) y le suma/resta su posicion original
	static Traslade = function(_x, _y, _s = 1) {
		#region Soporte otras clases
		if (is_rectangle(_x) ) {
			var _r = _x; // rectangulo
			_s = (!_y) ? 1 : _y;
			
			_x = _r.GetOriginX();
			_y = _r.GetOriginY();
			
		} else if (is_vector2(_x) ) {
			var _p = _x;
			_s = (!_y) ? 1 : _y;	
			
			_x = _p.x;
			_y = _p.y;
		}
		
		#endregion

		var _xo = GetOriginX(), _yo = GetOriginY();
		
		return SetBounds(_x + _xo, _y + _yo);
	}
	
	#region Trim (Ignoran origen)
	static TrimTop    = function(_pos) {
		return SetCorners(x1, y1 + _pos, x2, y2);
	}	
	
	static TrimLeft   = function(_pos) {
		return SetCorners(x1 + _pos, y1, x2, y2);
	}

	static TrimBottom = function(_pos) {
		return SetCorners(x1, y1, x2, y2 - _pos);			
	}

	static TrimRight  = function(_pos) {
		return SetCorners(x1, y1, x2 - _pos, y2);		
	}
	
	/// @desc Hace un trim arriba y abajo al mismo tiempo
	static TrimTopBottom = function(_pos1, _pos2 = _pos1) {
		TrimTop(_pos1);
		
		return TrimTop(_pos2);
	}

	/// @desc Hace un trim izquierda y derecha al mismo tiempo
	static TrimLeftRight = function(_pos1, _pos2 = _pos1) {
		TrimLeft(_pos1);
		
		return TrimRight(_pos2);
	}

	#endregion

	#endregion

		#region Getter´s
	/// @desc Obtiene el tamaño (w, h) a partir de la posicion x e y 
	static GetSize   = function() {
		w = abs(x2 - x1);
		h = abs(y2 - y1);
		
		return self;
	}
	
	/// @desc Obtiene el largo
	static GetWidth  = function() {return w; }
	/// @desc Obtiene la altura
	static GetHeight = function() {return h; }
	
	/// @desc Obtener el largo relativo
	static GetWidthRelative  = function() {return (w / wrel); }
	
	/// @desc Obtener la altura relativa
	static GetHeightRelative = function() {return (h / hrel); }
	
	/// @desc Obtiene el centro de x1
	static GetCenterX = function() {return (x1 + (w / 2) );	}
	
	/// @desc Obtiene el centro de y1
	static GetCenterY = function() {return (y1 + (h / 2) ); }		
	
	/// @desc Obtiene el punto left
	static GetX = function() {return x1; }
	
	/// @desc obtiene el punto top
	static GetY = function() {return y1; }

	/// @desc Devuelve el origen x	
	static GetOriginX = function() {
		switch (halign) {
			case fa_left :  return x1; break;
			case fa_right:  return x2; break;
			case fa_center: return GetCenterX(); break;
		}	
	}
	
	/// @desc Devuelve el origen y
	static GetOriginY = function() {
		switch (valign) {
			case fa_top   : return y1; break;
			case fa_bottom: return y2; break;
			case fa_middle: return GetCenterY(); break;
		}
	}
	
	/// @returns {Vector2}
	static GetPosition = function() {
		return (new Vector2(x1, y1) );
	}
		
	/// @returns {Vector2}
	static GetPositionOrigin = function() {
		var _x = GetOriginX(), _y = GetOriginY();
		
		return (new Vector2(_x, _y) );
	}
	
	/// @desc Devuelve el aspect-ratio
	static GetAspectRatio = function() {
		return (w / max(1, h) );
	}
	
	static GetHalign = function() {return halign; }
	static GetValign = function() {return valign; }
	
	static GetSizeRelative = function() {return (hrel != 1 || wrel != 1); }
	
	#endregion

		#region Setter´s	
	/// @desc Cambia el tamaño del rectangulo a partir de su origen; generalmente (x1, y1)
	static SetAlign   = function(_hal, _val) {
		if (_hal == undefined) _hal = halign;
		if (_val == undefined) _val = valign;
		
		// Aplico los nuevos
		// Guardo Aligns anterior
		halign_last = halign;
		valign_last = valign;
		
		var _x = Halign(_hal), _y = Valign(_val);

		SetBounds(_x, _y);
	
		return self;
	}
	
	/// @desc Establece el ancho
	static SetWidth   = function(_w) {w = _w;	return SetSize(w); }
	
	/// @desc Establece la altura
	static SetHeight  = function(_h) {h = _h;	return SetSize(undefined, h); }
		
	/// @desc Cambia el tamaño del rectangulo y las posiciones respecto a su origen
	static SetSize   = function(_w, _h) {
		#region Antes de
		if (is_rectangle(_w) ) {
			var _r = _w;
			_w = _r.GetWidth ();
			_w = _r.GetHeight();
		}
	
		if (_w == undefined) _w = wlast;	if (_h == undefined) _h = hlast;
		
		w = _w;
		h = _h;
		
		#endregion
		
		switch (halign) {
			case fa_left  :	x2 = x1 + w;		break;
			case fa_right :	x1 = x2 - w;		break;
			case fa_center:	
				if (halign_last == fa_left) {
					x1 = x2 - w;
					
				} else if (halign_last == fa_right) {
					x2 = x1 + w;
					
				} else {
					x1 = x2 - w;
				}
				
				break;
		}
		
		switch (valign) {
			case fa_top    : y2 = y1 + h;		break;
			case fa_bottom : y1 = y2 - h;		break;
			case fa_middle : 
				if (valign_last == fa_top) {
					x1 = y2 - h;
					
				} else if (valign_last == fa_bottom) {
					y2 = y1 + h;
					
				} else {
					y1 = y2 - h;
				}
				
				break;
		}
		
		wlast = _w; hlast = _h;
		
		return self;
	}
	
	static SetSizeRelative = function(_wrel, _hrel) {
		wrel = _wrel;
		hrel = _hrel;
		
		return self;
	}
	
	/// @param x
	/// @param y
	/// @param weight
	/// @param height
	/// @desc Cambia todos los valores del rectangulo a partir de su origen; generalmente (x1, y1)
	static SetBounds = function() {
		var _x, _y, _w, _h;
		
		#region Soporte para otros rectangulos!
		if (is_struct(argument[0] ) ) {
			var _rect = argument[0];
			
			_x = _rect.x1;
			_y = _rect.y1;
			
			_w = _rect.w;
			_h = _rect.h;
		} else {
			_x = argument[0];
			_y = argument[1];
			
			_w = (argument_count > 2) ? argument[2] : w;
			_h = (argument_count > 3) ? argument[3] : h;
		}
		
		#endregion

		switch (halign) {
			case fa_left  :	x1 = _x;	break;
			case fa_right :	x2 = _x;	break;
			case fa_center:
				if (halign_last == fa_left) {
					x2 = x1 + _x;
					
				} else if (halign_last == fa_right) {
					x1 = x2 - _x;
					
				} else {
					x2 = x2 - _x;
				}

				break;
		}
		
		switch (valign) {
			case fa_top    : y1 = _y;		break;
			case fa_bottom : y2 = _y;		break;
			case fa_middle : 
				if (valign_last == fa_top) {
					y2 = y1 + _y;
					
				} else if (valign_last == fa_bottom) {
					y1 = y2 - _y;
					
				} else {
					y2 = y2 - _y;
				}			
			
			
				break;			
		}

		return SetSize( abs(_w), abs(_h) );
	}
	
	/// @desc Cambia el valor de las posiciones y obtiene su nuevo tamaño (w y h)
	static SetCorners = function(_x1, _y1, _x2, _y2) {
		x1 = _x1;
		y1 = _y1;
		
		x2 = _x2;
		y2 = _y2;
		
		GetSize();
		
		return self;
	}
	
	/// @param new_x2
	/// @desc Cambia la posicion de la derecha del rectangulo
	static SetRight = function(_x2) {
		return SetCorners(x1, y1, _x2, y2);	
	}

	/// @param new_x1
	/// @desc Cambia la posicion de la izquierda del rectangulo	
	static SetLeft  = function(_x1) {
		return SetCorners(_x1, y1, x2, y2);			
	}

	/// @param new_y2
	/// @desc Cambia la posicion de abajo del rectangulo		
	static SetBottom = function(_y2) {
		return SetCorners(x1, y1, x2, _y2);		
	}

	/// @param new_y1
	/// @desc Cambia la posicion de arriba del rectangulo	
	static SetTop    = function(_y1) {
		return SetCorners(x1, _y1, x2, y2);		
	}
	
	#endregion
	
		#region Is
	static IsEmpty  = function() {
		return (w <= 0) || (h <= 0);
	}	
	
	/// @param x1
	/// @param y1
	/// @param x2
	/// @param y2
	/// @desc Comprueba si las cordenadas se encuentran adentro del rectangulo
 	static IsInside = function(_x3, _y3, _x4, _y4) {
 		#region Soporte otras clases
 		if (is_rectangle(_x1) ) {	
  			var _r = _x1;
 		
 			_x3 = _r.x1; _y3 = _r.y1;
 			_x4 = _r.y2; _y4 = _r.y2;	
 		} else if (is_vector2(_x1) && is_vector2(_y1) ) {
 			var _p1 = _x1, _p2 = _y1;
 			
  			_x3 = _p1.x; _y3 = _p1.y;
 			_x4 = _p2.x; _y4 = _p2.y;			
	 	}
 		#endregion
		
		return ! ( (_x3 > x2) || (_x4 < x1) || (_y3 > y2) || (_y4 < y1)	);
 	}
 	
	#endregion
	
		#region Obtainers (Regresan otro cuadrado)
	
	/// @param x
	/// @parma y
	static Basiced = function(_x, _y) {
		return Copy().Basic(_x, _y);
	}

	/// @param delta_x
	/// @param delta_y
	/// @desc Regresa un rectangulo que ha sido expandido de su origen (0, 0)
	/// @returns {Rectangle}
	static Expanded = function(_x, _y) {
		if (_y == undefined) _y = _x;
		
		return Copy().Expand(_x, _y);
	}
	
	/// @param x1
	/// @param y1
	/// @param x2
	/// @param y2
	/// @param scale
	/// @desc Regresa un rectangulo que se le ha aplicado sangría
	/// @returns {Rectangle}
	static Indented = function(_x1, _y1, _x2, _y2, _s = 1) {
		return Copy().Indent(_x1, _y1, _x2, _y2, _s);
	}			

	/// @param x1
	/// @param y1
	/// @param x2
	/// @param y2
	/// @param scale
	static Bordered = function(_x1, _y1, _x2 = _x1, _y2 = _y1, _s = 1) {
		return Copy().Border(_x1, _y1, _x2, _y2, _s); 	
	}	

	/// @param escala
	/// @desc Regresa un rectangulo con sus valores multiplicados con origen (0, 0)
	/// @retun {Rectangle}
	static Scaled  = function(_scale) {
		return Copy().Scaled(_scale);
	}
	
	/// @param halign
	/// @param valign
	/// @desc Regresa un rectangulo con sus valores pero con un origen diferente
	/// @retun {Rectangle}	
	static Aligned = function(_h, _v) {
		return Copy().SetAlign(_h, _v);
	}

	#region Removed
	/// @param delta_x
	/// @param delta_y
	/// @desc Regresa un rectangulo que ha sido reducido de su origen (0, 0)
	/// @returns {Rectangle}
	static Reduced = function(_x, _y) {
		if (_y == undefined) _y = _x;
		
		return Copy().Reduce (_x, _y);
	}	

	/// @param value
	/// @returns {Rectangle}
	static RemoveTop    = function(_pos) {
		TrimTop(_pos);
		
		return (Copy() ).SetCorners(x1, y1 - _pos, x2, y1);
	}	
	
	/// @param value
	/// @returns {Rectangle}
	static RemoveLeft   = function(_pos) {
		TrimLeft(_pos);
		
		return (Copy() ).SetCorners(x1 - _pos, y1, x1, y2);
	}

	/// @param value
	/// @returns {Rectangle}
	static RemoveBottom = function(_pos) {
		TrimBottom(_pos);

		return (Copy() ).SetCorners(x1, y2, x2, y2 + _pos);
	}

	/// @param value
	/// @returns {Rectangle}
	static RemoveRight  = function(_pos) {
		TrimRight(_pos);

		return (Copy() ).SetCorners(x2, y1, x2 - _pos, y2);
	}

	#endregion

	
	/// @desc Regresa una copia del rectangulo cuyo origen es (0, 0)
	/// @returns {Rectangle}
	static Copy = function() {
		return (new Rectangle(x1, y1, w, h) ).SetSizeRelative(wrel, hrel);
	}
	
	#endregion
	
		#region Misq
	static HalignToString = function() {
		switch (halign) {
			case fa_left  : return "hal: fa_left"  ; break;
			case fa_right : return "hal: fa_right" ; break;
			case fa_center: return "hal: fa_center"; break;
		}	
	}	
		
	static ValignToString = function() {
		switch (valign) {
			case fa_top   : return "val: fa_top"   ; break;
			case fa_bottom: return "val: fa_bottom"; break;
			case fa_middle: return "val: fa_middle"; break;
		}		
	}	
		
	/// @desc Intenta convertir los valores a un string
	/// @returns {string}
	static ToString = function() {
		return "x1: " + string(x1) + "\n x2: " +string(x2) + "\n y1: " + string(y1) + "\n y2 " + string(y2) + "\n w: " + string(w) + "\n h: " + string(h) + 
			   "\n" + HalignToString() + "\n" + ValignToString();
	}
	
	static GetPositionRatio = function() {
		return (GetOriginX() / max(1, GetOriginY() ) );	
	}
	
	#endregion
	
	#endregion
	
	Align();
}

#endregion

#region QL

globalvar __trstack, __trtop, __trold;

__trstack = ds_stack_create();
__trtop = -1;
__trold = -2;

/**
	name
	type
	[ds_type]
*/
function __TypeReader(_name, _type, _ds = undefined) {
	if (__trtop == __trold) return (__trtop.__reset() );
	
	var _push = {
		__in: undefined,
		
		__name : "",
		__kname: "",
		__iname: "",
		
		__pos: -1,
		__len: -1,
		
		__reset: function() {__pos = -1; return (__set() ); },
		__set:	 function() {return self; }
	}
	
	if (is_array(_type) ) {
		with (_push) {
			__in = _type;
			__name  = (is_array(_name) ) ? _name[0] : _name;
			__iname = (is_array(_name) ) ? _name[1] :   "i";
			
			__len = array_length(__in) - 1;
			__set = function() {
				__pos++;
				
				variable_struct_set(self, __name ,	__in[__pos] );
				variable_struct_set(self, __iname,	__pos ); 

				return self;
			}
		}
	}
	else if (is_struct(_type) ) {
		with (_push) {
			__in = _type;
			__name  = (is_array(_name) ) ? _name[0] : _name;
			__kname = (is_array(_name) ) ? _name[1] : "key";
			__iname = (is_array(_name) ) ? _name[2] :   "i";
			
			__keys = variable_struct_get_names(__in);
			__len  = array_length(__keys) - 1;
			
			__set = function() {
				__pos++;
				var _key = __keys[__pos];
				variable_struct_set(self, __name ,	__in[$ _key] );
				
				variable_struct_set(self, __kname,	_key ); 
				variable_struct_set(self, __iname,	__pos);
				
				return self;
			}
		}
	}
	else if (is_string(_type) ) {
		with (_push) {
			__in = _type;
			__name  = (is_array(_name) ) ? _name[0] : _name;
			__iname = (is_array(_name) ) ? _name[1] :   "i";
			__rname = (is_array(_name) ) ? _name[2] : "pos";	// Posicion empezando desde 0
			
			__len = string_length(__in) - 1;
			__pos = 0;	// String empiezan de 1 (osea poner en 0)
			
			__set = function() {
				__pos++;
				
				variable_struct_set(self, __name ,	string_char_at(__in, __pos) );
				variable_struct_set(self, __iname,	__pos );
				variable_struct_set(self, __rname,	__pos - 1);

				return self;
			}
			
			__reset = function() {
				__pos = 0;
				return (__set() );
			}
		}
	}
	else {
		switch (_ds) {
			case ds_type_list:
				with (_push) {
					__in = _type;
					__name  = (is_array(_name) ) ? _name[0] : _name;
					__iname = (is_array(_name) ) ? _name[1] :   "i";
			
					__len = ds_list_size(__in);
					__set = function() {
						__pos++;
				
						variable_struct_set(self, __name ,	__in[| __pos] );
						variable_struct_set(self, __iname,	__pos ); 

						return self;
					}
				}
				break;
			
			case ds_type_map:
				with (_push) {
					__in = _type;
					__name  = (is_array(_name) ) ? _name[0] : _name;
					__kname = (is_array(_name) ) ? _name[1] : "key";
					__iname = (is_array(_name) ) ? _name[2] :   "i";
			
					__keys = ds_map_keys_to_array(__in);
					__len  = array_length(__keys);
					__pos  = -1;
					
					__set = function() {
						__pos++;
						var _key = __keys[__pos];
						
						variable_struct_set(self, __name ,	__in[? _key] );
				
						variable_struct_set(self, __kname,	 _key);
						variable_struct_set(self, __iname,	__pos);
	
						return self;
					}
				}
				break;
		}
	}
	// Push shiet
	__trold = __trtop;
	__trtop =   _push;
	
	ds_stack_push(__trstack, _push);
	
	return (__trtop.__set() );
}

#macro iter	with(__TypeReader(
#macro exc	)) {repeat(__len) {
#macro fin	__set();}} ds_stack_pop(__trstack);

/// @param width
/// @param height
/// @desc Crea una estructura que almacena un surface.
function Canvas(_w = 1, _h = 1) : __ClasicoStruct__() constructor {
	__surface = -1;
	__sprite  = -1;	// Sprite
	
	w = _w;
	h = _h;
	
	wlast = _w;
	hlast = _h;
	
	__redraw = false;
	
	#region Metodos
	
	/// @param width
	/// @param height	
	static Resize = function( _w, _h) {
		_h ??= _w;

		var _wo = w, _ho = h;
		
		w = _w;
		h = _h;
		
		// more fast
		if (w == wlast && h == hlast) return self;
		
		surface_free(__surface);
		
		wlast = _wo;
		hlast = _ho;
		
		return self;
	}
	
	/// @param {bool} force?
	static Update = function(_force = false) {
		__redraw = (_force || !surface_exists(__surface) );
		
		if (__redraw) {
			__redraw = false;
			Set();
			
			return true;
		}
		
		return false;
	}

	static Redraw = function() {
		__redraw = true;
		return self;
	}
	
	static ToSprite = function(_x, _y, _xorig, _yorig, _remback = false, _smooth = true) {
		if (!surface_exists(__surface) ) return undefined;
		
		var _spr = sprite_create_from_surface(__surface, _x, _y, w, h, _remback, _smooth, _xorig, _yorig);
		
		Free();
		return _spr;
	}
	
	// -- Basico
	/// @desc surface_set_target()
	static Set = function() {
		if (!surface_exists(__surface) ) __surface = surface_create(w, h);
		
		if (surface_get_target() != __surface) {
			surface_set_target(__surface);
		}
		
		return self;
	}
	
	static Get = function() {
		if (surface_exists(__surface) ) return __surface;
		return undefined;
	}
	
	/// @desc surface_reset_target()	
	static Reset = function() {
		if (surface_get_target() == __surface) surface_reset_target();		
		return self;
	}
	
	static Free = function() {
		if (surface_exists(__surface) ) surface_free(__surface);
		return self;
	}
	
	static Draw = function(_x, _y) {
		if (!surface_exists( __surface ) ) exit;

		draw_surface(__surface, _x, _y);	
	}
	
	static DrawExtend = function(_x, _y, _xscale, _yscale, _angle, _color, _alpha) {
		if (!surface_exists( __surface ) ) exit;
		
		draw_surface_ext(__surface, _x, _y, _xscale, _yscale, _angle, _color, _alpha);
	}

	#endregion
}

/**
	@param string
	@desc Permite leer archivos, iterar un string y hacer conversiones .json
*/
function Parser(_str = "") : __ClasicoStruct__() constructor {
	__start   =    _str;	// Primer contenido establecido
	__content = __start;	// Contenido

	// variables de metodos
	__split =		[];
	__marks = ["", ""];	
	
	__size = string_width (__content); // tamaño en pixeles
	__len  = string_length(__content); // numeros de caracteres
	
	__json = {};
	
	#region Metodos
	/// @param string
	static Set = function(_str) {
		__content  = _str;
		__size = string_width (__content); // tamaño en pixeles
		__len  = string_length(__content); // numeros de caracteres
		
		return self;
	}	
	
	/// @param [index]
	/// Obtiene el contenido o uno de los splits obtenidos.
	static Get = function(_ind = -1) {
		return (_ind > -1) ? __split[_ind] : __content;
	}
	
	/** 
	@param {String} _sep	separator
	@param {String} [_not]	not_found
	@param {Real} [_ind]	get_index
	@desc Separara un string utilizando 
	*/
	static Explode = function(_sep, _not = "", _ind = -1) {
		var _content = Get(_ind);
		var _array = [];
		var _count = string_count(_sep, _content);
			
		if (_count > 0) {
			var _len = string_length(_sep);	
			
			repeat(_count) {	
				var p = string_pos(_sep, _content) - 1;
				array_push(_array, string_copy(_content, 1, p) );
				
				_content = string_delete(_content, 1, p + _len);
			}
			
			// Lo que queda agregar al final.
			array_push(_array, _content);
				
		} else array_push(_array, _not); 
		
		// Cambiar el array de split
		__split = _array;
		
		return self;		
	}
	
	/** 
	@param {String} _sep 
	@param {String} _not
	@param {Real} _ind
	*/
	static Exploded = function(_sep, _not , _ind) {
		return (new Parser(__start) ).Explode(_sep, _not, _ind);
	}
	
	/// @param {array} array	no structs!
	/// @param {string} glue
	/// @desc Convierte valores de un array a string usando un pegamento
	static Implode = function(_array, _glue) {
		var _output = "", _len = string_length(_glue);	 
		
		var i = 0; repeat(_len) {
			var _in = _array[i++];
			
			if (is_string(_in) || is_numeric(_in) ) {
				_output += string(_in) + string(_glue); 
			}			
		}
		
		_output = string_copy(_output, 0, string_length(_output) - _len);
		 
		return Set(_output);		
	}
	
	/// @param replace
	/// @param search
	/// @param [split_index]
	static Replace  = function(_rep, _sub, _ind = -1) {
		var _str = __content;
		
		if (_ind > -1) {
			_str = __split[_ind];
			__split[_ind] = string_replace(_str, _sub, _rep);
		} else {
			__content = string_replace(_str, _sub, _rep);
		}
		
		return self;
	}
	
	/// @param [split_index]
	static Reversed = function(_ind = -1) {
		var _str = Get(_ind), _len = string_length(_str);
		
		var i = _len; repeat(_len) {_str += string_char_at(_str, i--); }
		
		return (Set(_str) );
	}
	
	/// @param [split_index]
	/// @desc Quita el espacio en blanco del contenido EJ: "  hola, , lol  " -> "hola, , lol"
	static Trim = function() {
		var _str = Get();
		
		///@param string
		///@desc trim a string ("  trim .  . spaces   " -> "trim .  . spaces")
		var _len = string_length(_str);
		
		while (_len > 0 && string_char_at(_str, 0)    == " " ) {_str  = string_copy(_str, 2, _len - 1); --_len; }
		
		while (_len > 0 && string_char_at(_str, _len) == " " ) {_str  = string_copy(_str, 0, _len - 1); --_len; }
			
		return (Set(_str) );
	}
	
	/// @param width
	/// @param [split_index]
	static Wrap = function(_width, _ind = -1) {
		var _str = Get(_ind);
		var _size = string_width (_str);	// Obtener el tamaño del string en pixeles
		var _len  = string_length(_str);	// Obtener cantidad de caracteres
	
		var _last = 1, _sub;
		
		// Evitar entrar al bucle si el mensaje es pequeño
		if (_size < (_width / 2) ) return _str;
		
		var i = 1; repeat (_len) {
			// -- Copiar y last_space --
			_sub = string_copy(_str, 1, i++);	// Copiar 1 a 1
			
			// Si se encuentra un espacio almacenarlo.
			if (string_char_at(_str, i) == " ") _last = i;
			
			// Agregar espacio cuando se necesite
			if (string_width(_sub) * (1.5 > _width) ) {
				_str = string_delete(_str, _last, 1);
				_str = string_insert("\n", _str, _last);
			}
		}
		
		return (_str);		
	}
	
	static Format = function(_method) {
		
	}
	
	/// @param split_index
	/// @param method
	/// Recorre el string de inicio a final pasando un metodo por cada ciclo
	static Foreach = function(_ind, _f) {
		var _str = Get(_ind);
		
		var i = 1; repeat(array_length(_str) ) {
			var _sub = string_copy(_str, 1, i++);

			_f(_sub);
		}
	}
	
	#region Archivos
	/// @param file
	/// @param [no_file]
	/// Lee un archivo de texto y almacena el contenido
	static Read = function(_file, _nofile = "") {
		var _txt = "";
		
		// Si no existe el archivo establecer el contenido pre-establecido
		if (file_exists(_file) ) {
			_f = file_text_open_read(_file);
	
			while (!file_text_eof(_f) ) {
				_txt += file_text_read_string(_f);
				file_text_readln(_f);
			}
			
			file_text_close(_f);
			
			Set(_txt);
			
		} else Set(_nofile);
	
		return self;
	}
	
	/// @param file_name
	/// @param [save_function]
	static Save = function(_file, _f) {
		if (_f == undefined) _f = function(_fl, _content) {
		    var _f = file_text_open_write(_fl);
		    file_text_write_string(_f, string(_content) );
		    file_text_close(_f);
		}
		
		// Guardar el contenido en un archivo de texto
		_f(_file, __content);
	}
	
	/// @param {struct} structToJSON
	/// @desc Convierte un struct en un json y cambia el valor del contenido
	static GetJSON = function(_struct) {
		__json = snap_to_json(_struct);
		
		return Set(__json);
	}
	
	/// @param {number} [split_index]
	/// @desc Convierte el contenido (o split) en un struct (json)
	static ToJSON  = function(_ind = -1) {
		try {
			var _j = snap_from_json(Get(_ind) ); 
			
		} catch (_j) {_j = {}; }
		
		__json = _j;
		
		return (__json);
	}
	
	#endregion
	
	#region Is
	/// @param [split_index]
	/// @returns {bool}
	static IsReal = function(_ind = -1) {
		var _str = Get(_ind);
		try {var _is = is_real(_str); } catch (_is) {_is = false; }
		
		return _is;
	}
	
	#endregion
		
	/// @desc Lo devuelve como se inicio originalmente
	static Reset = function() {
		gc_collect();	// Limpiar nosotros mismos
		
		Set(__start);
		
		__split = [];
		__json  = {};
	} 
	
	/// @desc Limpia el contenido (arrays y json)
	static Clean = function() {
		// variables de metodos
		__split =		[];
		__marks = ["", ""];	
		
		__json = {};
	}
	
	#endregion
}

/// @desc Numeros porcentuales.
function Data(_start = 0) : __ClasicoStruct__() constructor {
	// No se introduce un numero que mal...
	if (is_string(_start) ) show_error("u.u", true);
	
	__start = _start;
	
	__real = 0;	// Valor entero (no porcentaje puede ser decimal)
	__perc = 0; // Valor decimal (en porcentaje) es el valor de "__real" dividido en 100

	__str  = "0%"; // Si es numero lo pasa a string	
	
	__min = undefined;	// limites
	__max = undefined;	// limites
	
	#region Metodos

	/// @param pass<Data-number>
	// Eliminar soporte de string, simplemente no me gusta usarlo haha
	static Set = function(_value) {
		// Limite
		if (!is_undefined(__min) && !is_undefined(__max) ) {
			_value = clamp(_value, __min, __max);
		}
		
		// Solo numeros y Data
		__real = (is_numeric(_value) ) ? _value : _value.__real;
		
		__perc = (__real / 100);
		__str  = ToString(__real);
		
		return self;
	}
	
	static SetLimit = function(_min, _max) {
		__min = _min;
		__max = _max;
		return self;
	}
	
	static ToString  = function(_num) {
		return (string(_num) + "%");
	}
	
	/// @param pass<Data-number>
	/// @desc Suma o resta
	static Operate  = function(_value) {
		return Set(is_numeric(_value) ? (__real + _value ) : (__real + _value.__real ) );
	}
	
	/// @param pass<Data-number>
	/// @returns {Data} Nueva estructura con los valores operados
	static Operated = function(_value) {
		return (new Data(__real) ).Operate(_val);
	}

	/// @param pass<Data-number>
	/// @desc multiplicacion
	static Multiply = function(_value) {
		return (Set(is_numeric(_value) ? __real * _value : __real * _value.__real)  );
	}

	/// @param pass<Data-number>
	/// @returns {Data}	
	static Multiplied = function(_value) {
		return (new Data(__real) ).Multiply(_value);		
	}
	
	/// @param {Data} Data_class
	/// pone esta clase al mismo valor que otra
	static Match = function(_data) {
		return (Set(_data.__real) );
	}

	/// @desc Multiplica por -1	
	static Turn  = function() {
		return (Multiply(-1) );
	}
	
	/// @returns {Data}
	static Turned = function() {
		return (new Data(__real) ).Turn();
	}
	
	/// @param min
	/// @param max
	/// Limita los valores de este data
	static Clamp = function(_min, _max) {
		return (Set(clamp(__real, _min, _max) ) );	
	}
	
	/// @param min
	/// @param max
	static Clip  = function(_min, _max) {
		return (Set(clip(_min, _max, __real) ) );
	}
	
	static Reset = function() {
		return Set(__start);
	}
	
	/// @returns {Data}
	static Copy  = function() {
		return (new Data(__real) ).SetLimit(__min, __max);
	}
	
	#endregion
	
	Set(__start);
}

/**
	@desc	Es un tipo de estructura que permite tener una busqueda rapida de elementos, como tambien permite ciclar entre
			los contenidos (y llaves) rapidamente
	
	@return {Struct.Bucket}
*/
function Bucket() : __ClasicoStruct__() constructor {
	#region Interno

	/// @type {Struct}
	__content = {};	// Guardar los valores
	//	@type {Array.String} @ignore */
	__keys	= [];	// Guardar las llaves del bucket
	
	/// @ignore
	__last = 0;
	
	/// @ignore
	__size = 0;
	
	#endregion
	
	#region Metodos
	
	
	/**
	@desc Establecer un valor
	@param {String} Key
	@param {Mixed} Value
	@returns {Struct.Bucket}
	*/
	static Set = function(_key, _value) {
		if (!Exists(_key) ) {
			array_push(__keys, _key);
			__size = array_length(__keys);
		}
		
		__content[$ _key] = _value;
		
		return self;
	}

	/**
	@desc Establece todos los valores del bucket a un valor especificado
	@param {Mixed} Value
	@returns {Struct.Bucket}
	*/
	static All = function(_value) { 
		__last = 0;
		repeat (Size() ) {
			var _key = CicleKey();
			
			Set(_key, _value);
		}
		
		return self;
	}
	
	/** 
	@param {string} _key llave para obtener el valor
	@return {mixed} 
	*/
	static Get = function(_key) {
		return (is_string(_key) ? __content[$ _key] : __content[$ __keys[_key] ] );	
	}
	
	/**
	@param {String} key
	@return {Bool}
	*/
	static Exists = function(_key) {
		return (variable_struct_exists(__content, _key) );
	}

	/**
	@param {String} Key
	@return {Mixed}
	*/
	static Remove = function(_key) {
		var _deleted = undefined;
		
		// No soporte para undefined
		if (_key == undefined) return _deleted;
		
		if (is_array(_key) ) {	// Soporte para arrays
			_deleted = [];
			
			var i = 0; repeat(array_length(_key) ) {
				var _in = Remove(_key[i++] );
				
				array_push(_deleted, _in);	
			}
		} else { 
			var _index = Search(_key);
			
			if (_index != undefined) {
				// Remover contenido del struct
				variable_struct_remove(__content, _key);
				
				// Remover del array
				_deleted = __keys[_index];
				array_delete(__keys, _index, 1);
			}
		}
		
		__size = array_length(__keys);
		
		return _deleted;
	}
	
	/**
	@desc Devuelve la primera llave
	@return {String}
	*/
	static First = function() {
		return (Key(0) );
	}
	
	/**
	@desc Devuelve el primer valor
	@return {Mixed}
	*/
	static Peek  = function() {return (__content[$ First() ] ); }
	
	/**
	@desc Devuelve la ultima llave
	@return {String}
	*/
	static Last = function() {
		var _key = array_pop(__keys);
		
		__last = __size - 1;

		return _key;
	}
	
	/**
	@desc Devuelve el ultimo valor
	@return {Mixed}
	*/
	static Pop  = function() {return (__content[$ Last() ] ); }
	
	/**
	@desc Devuelve el indice de la llave y cambia el indice de ciclo
	@param {String} Key
	@return {Mixed}
	*/
	static Search = function(_key) {
		if (!Exists(_key) ) return undefined;
		
		__last = 0;
		
		repeat (Size() - 1) {if (CicleKey() == _key) return __last; }
	}
	
	/// @returns {array} Similares
	static SearchSimilar = function(_key) {
		repeat (Size() - 1) {	
		}
	}
		
		#region Ciclo
	/**
	@desc Devuelve una llave en el indice indicado
	@param {Real} Cicle
	@retun {String}
	*/
	static Key = function(_cicle = 0) {
		__last = clamp(_cicle, 0, __size - 1);
		return (__keys[__last] );
	}
	
	/**
	@desc Devuelve la siguiente llave a partir del ultimo indice de ciclo
	@param {Real} [cicle]
	@return {String}
	*/
	static CicleKey = function(_cicle = 1) { 
		__last = clip(0, __size - 1, __last + _cicle);
		
		return (__keys[__last] );
	}
	
	/** @desc Cicla los contenidos de derecha a izquierda */
	static Next = function() {return (__content[$ CicleKey() ] ); }
	
	/** @desc Cicla los contenidos de izquierda a derecha */
	static Previus = function() {return (__content[$ CicleKey(-1) ] ); }
	
	/** @desc Devuelve la llave con el indice de ciclo de actual */
	static Actual = function() {return (__keys[__last] ); }
	
	/** @return {Array} */
	static GetKeys = function() {return __keys; }
	
	#endregion
	
	/** 
	@desc Devuelve el tamaño del bucket
	@param {Bool} [reset_cicle]
	@return {Real} 
	*/
	static Size = function(_reset = false) {
		if (_reset) __last = -1; // Para ciclar
		
		__size = array_length(__keys);
		
		return (__size);
	}
	
	/**
	@desc True: Esta vacio False: tiene contenido
	@return {Bool}
	*/
	static Empty = function() {return (Size() < 1); }
	
	/** @return {Struct.Bucket} @desc Reinicia los valores del bucket */
	static Clear = function() {
		__content = {};
		__keys = [];
		
		__last = 0;
		__size = 0;
		return self;
	}
		
	/** @return {Struct.Bucket} */
	static Copy = function() {
		var _bucket = new Bucket(); /// @is {Bucket}
		
		// Pone en 0
		__last = 0;
		
		// Devolver valores
		repeat(Size() ) {
			var _key   = CicleKey();
			var _value =  Get(_key);
			
			_bucket.Set(_key, _value);
		}
		
		__last = 0;
		
		return (_bucket);
	}
	
	/** @return {Array} @desc Devuelve un array con las keys copiadas */
	static CopyArray = function() {
		var _array = array_create(0);	
		
		array_copy(_array, 0, __keys, 0, array_length(__keys) );
		return _array;
	}
	
	
	#endregion
}

/**
	@param {Real} _min
	@param {Real} _max
	@param {Bool} [_repeat]
	@param {Bool} [_active]
	@param {Real} [_start]
	
	@return	{Struct.Counter}
*/
function Counter(_min = 0, _max = 1, _repeat = false, _active = true, _start = _min) : __ClasicoStruct__() constructor {
	__count  = _min; // Valor de la cuenta
	
	__min = _min; // Minimo valor de cuenta
	__max = _max; // Maximo valor de cuenta

	__amount = CLCOUNTER_AMOUNT; // Cada cuanto aumenta el valor de la cuenta
	
	__active = _active;	// Si esta activo puede trabajar
	__activeStart = _active;

	__repeat = _repeat;	// Si al terminar reinicia el valor de la cuenta

	__iterate = false;
	__iterateTime = noone; // Cuantas iteracciones trabajará. noone: indefinidamente
	
	#region Metodos
	/** @desc Trabajar el contador. Posee 2 modos que dependen: Contador o Adiccion por ciclos*/
	static Work = function() {
		if (!__iterate) {
			#region Usar como un contador
			if (__active) {
				__count += __amount;
				
				if (__count >= __max) {
					if (__iterateTime > 1) { 
						// Si hay que repetir 
						__active = __repeat;
						__count  = __min * __repeat; // Reiniciar contador si repite
						
						// Itera indefinidamente
						if (__iterateTime != noone) {--__iterateTime; }
						
						return true;
					} else {	// Se acabaron las iteraciones
						__active = false;
						__iterateTime = 0;
						
						return false;
					}
				}
			}
			return false;
			
			#endregion
		} 
		else {
			#region Usar como adicion por ciclos
			
			var _test = (__iterateTime >= 0);
			
			if (__iterateTime >= 0) {	
				__count += __amount;
				// Restar los ciclos que quedan
				--__iterateTime;
				
				return true;
			}
			
			return false;
			
			#endregion
		}
	}
	
	/** @param {Real} [_amount] Default = 1 */
	static SetAument  = function(_amount = 1) {
		__amount = _amount;
		return self;
	}
	
	/** 
		@param {Real} [_time] Default = 0 
		@desc	Cambia	modo 0: Contador. modo 1: Adicion por ciclos.
	*/
	static ToggleIterate = function(_time = 0) {
		__iterate = !__iterate;
		__iterateTime = _time;
		
		return self;
	}
	
	/** @param {Bool} _active @param {Bool} [_start] */
	static SetActive = function(_active = true, _start) {
		__active = _active;
		__activeStart = _start ?? __activeStart;
	}
	
	/// @return {Struct.Counter}
	static Copy = function() {
		var _count = new Counter(__min, __max, __repeat, __activeStart);
		
		_count.__amount  = self.__amount ;
		_count.__iterate = self.__iterate;
		
		return (_count );
	}

	#endregion
}

/// Crea un controlador de tiempo usando delta
function DeltaTime() : __ClasicoStruct__() constructor {
	time	  =	(delta_time/1000000) * game_get_speed(gamespeed_fps);
	real_time =	(get_timer() / 1000000);
	
	scale = 1;	// 1 normal speed; .5 half speed; 2 double speed
	
	#region Metodos
	static Update = function() {
		time		= (delta_time/1000000) * game_get_speed(gamespeed_fps) * scale;	
		real_time	= (get_timer() / 1000000);
	}	

	/// @param segundos
	static Seconds = function(_s) {
		//Devuelve tiempo en segundos
		return (_s * room_speed * time);
	}
	
	static SetScale = function(_s = 1) {
		scale = _s;
		return self;
	}
		
	#endregion
}

/// @param hue
/// @param saturation
/// @param value
function ColorHSV(_h, _s, _v) : __ClasicoStruct__() constructor {
	h = _h;	// Hue
	s = _s;	// Saturation
	v = _v;  // Value
	
	#region Metodos
	/// @param [temp_h
	/// @param temp_s
	/// @param temp_v]
	static Produce = function(_temph, _temps, _tempv) {
		var _h = _temph ?? h;
		var _s = _temps ?? s;
		var _v = _tempv ?? v;

		return (make_color_hsv(_h, _s, _v) );
	}
	
	/// @param hue
	/// @param saturation
	/// @param value
	static Set = function(_h, _s, _v) {
		h = _h ?? h;
		s = _s ?? s;
		v = _v ?? v;
	
		return self;
	}
	
	#endregion
}

/// @param red
/// @param green
/// @param blue
function ColorRGB(_r, _g, _b) : __ClasicoStruct__() constructor {


}

#region GameMechanics
/// @param x
/// @param y
/// Based on the work of Jesse Brace (Boolean Dimensions and Brace Robotics (C) 2018)
function DragDrop(_x, _y) : __ClasicoStruct__() constructor {
	//where the origin of the thing is
	ispos  = new Vector2(_x, _y);	/// @is {Vector2}
	
	//where the thing was
	waspos = ispos.Copy();			/// @is {Vector2}
	
	//where the mouse was
	mouse = new Vector2(mouse_x, mouse_y);	/// @is {Vector2}
	
	//need this boolean bc GM executes mouse button before mouse pressed
	anchor = false;
	
	#region Metodos
	/// @desc When the mouse is clicked, set the x,y anchor positions for drag and drop.
	static PickUp = function() {
		anchor = true;

		//where the mouse was
		mouse.SetXY(mouse_x, mouse_y);
		waspos = ispos.Copy();
		
		return self;
	}
	
	static DragX = function() {
		if (anchor) {	
			ispos.SetX(mouse_x - (mouse.x - waspos.x) );
		}
		
		return self;
	}

	static DragY = function() {
		if (anchor) {	
			ispos.SetY(mouse_y - (mouse.y - waspos.y) );
		}
		
		return self;		
	}

	static Drag = function() {
		if (anchor) {	
			ispos.SetXY(mouse_x - (mouse.x - waspos.x), mouse_y - (mouse.y - waspos.y) );
		}		
		
		return self;
	}
	
	static Drop = function() {
		anchor = false;
		return self;
	}
	
	/// @returns {bool}
	static IsDragging = function() {
		return (anchor);
	}
	
	#endregion
}

/// @param xbox
/// @param ybox
/// @param xslider
/// @param yslider
/// @desc Crea un selector de color en hsv
function ColorPicker(_bx, _by, _sx, _sy) : __ClasicoStruct__() constructor {
	// -- Box
	box = new Vector2(_bx, _by);		/// @is {Vector2}
	canvas1 = new Canvas(255, 255);		/// @is {Canvas}
	color   = new ColorHSV(0, 255, 0);	/// @is {ColorHSV}

	// -- Slider
	slider  = new Vector2(_sx, _sy); 		/// @is {Vector2}
	canvas2 = new Canvas(24, 255);	/// @is {Canvas}

	handle_spr = -1; // Hue selector
	thumb_spr  = -1; // Saturation and value selector
	
	Start();

	#region Metodos
	
	/// @returns {bool}
	static BoxCheck = function() {
		return (mouse_is_here(box.x, box.y, box.x + canvas1.w, box.y + canvas1.h) );
	}
	
	static BoxMove  = function() {
		color.Set(undefined, mouse_x - x, mouse_y - y);	
		return self;
	}
	
	static BoxConstraint = function() {
		//mouse_x = clip(box.x, box.x + canvas1.w, mouse_x);
		//mouse_y = clip(box.y, box.y + canvas1.h, mouse_y);
		
		return self;
	}
	
	static BoxDraw = function() {
		canvas1.Set();
			draw_set_color(color.Produce(undefined, 255, 255) );
				draw_rectangle(0, 0, canvas1.w, canvas1.h, false);
		canvas1.Reset();
			
		shader_set(shdColorPicker);
			draw_surface(canvas1.Get(), box.x, box.y);
		shader_reset();

		// Si no es -1
		if (thumb_spr != -1) draw_sprite(thumb_spr, 0, box.x + color.s, box.y + color.v);
	}
	
	/// @returns {bool}
	static SliderCheck = function() {
		return (mouse_is_here(slider.x, slider.y, slider.x + canvas2.w, slider.y + canvas2.h) );	
	}
		
	static SliderMove = function() {	
		color.Set(mouse_y - y);	
		
		// mouse_y = clip(slider.y, slider.y + canvas2.h, mouse_y);
		
		return self;
	}	
	
	static SliderDraw = function() {
		draw_surface(canvas2.Get(), slider.x, slider.y);
		
		if (handle_spr != -1) draw_sprite(handle_spr, 0, slider.x + canvas2.w + 4, slider.y + canvas2.h);

		return self;
	}
	
	static Start = function() {
		canvas1.Set();
			draw_set_color(color.Produce(undefined, 255, 255) );
			draw_rectangle(0, 0, canvas1.w, canvas1.h, false);
		canvas1.Reset();

		canvas2.Set();
			var i = 0; repeat (255) {
				draw_set_color(make_color_hsv(i, 255, 255) );
					draw_line(0, i, canvas2.w, i);
				++i;
			}
		canvas2.Reset();
		
		// Reset
		draw_set_color(c_white);		
	}
	
	#endregion
	
}


#endregion


#endregion