#macro PARTY_STAT_SHOWMESSAGE false

/// @param lvl
/// @param group_key*

/**
@param {String} group_key
@param {Real} [Level]
*/
function PartyStats(_groupKey, _lvl = 1) : MallComponent("") constructor {
    __lvl    = _lvl;		// -1 signfica que no se ha inicializado
    __group  = _groupKey;	// llave del grupo que pertenece
    
    // CONDICIONES POSEEN  {bol: true-false, value: }
    __condition = function() {return {bol: true, value: 0}; }
    
    __methodStart = MALL_DUMMY_METHOD;
    __methodEnd   = MALL_DUMMY_METHOD;
    
    __start = false;
    
    __weakControl = undefined;
    
    #region Metodos
    static Init = function() {
        // Obtener un bucket para poder ciclar entre las key de las estadisticas
        var _keys = mall_get_stats();

        // Obtener las estadisticas personalizadas del grupo
        var _statGroup = mall_get_group(__group).__stats;

        var i = 0; repeat (array_length(_keys) ) {
			// Obtener llaves
			var _key  = _keys[i++], _stat = _statGroup[$ _key];
			
			// Que exista la estadistica
			if (!is_undefined(_stat) ) {
				variable_struct_set(self, _key, new __PartyStatsComponent(_key, _stat) );
			}
		}
	}

	/**
	@desc Establece el valor maximo de la estadistica a este nivel con limite el mayor y menor valor de la configuracion. (¡Solo usar cuando se sube de nivel!)
	@param {String} StatKey
	@param {Real} Value
	@param {Bool} [Actual]
	*/
    static SetUpper  = function(_key, _value, _actual = false) {
		/// @context {__PartyStatsComponent}
		var _stat = Get(_key);
        
		// Obtener limites
		var _min = _stat.limit[0];
		var _max = _stat.limit[1];
        
		// Que no salga de los limites
		_value = clamp(_value, _min, _max);

		_stat.final -= _stat.upper; // Obtener el valor de los objetos simplemente.
		_stat.upper = _value;   // Establecer nuevo valor.
		_stat.final += _stat.upper; // Aplicar el nuevo valor al final.        

		if (_actual) _stat.actual = _stat.final;

		return self;
    }
    
	/**
	@desc	Establece el valor actual de una estadistica teniendo como limite mayor el "final" y menor el de la configuración
	@param	{String} StatKey
	@param	{Real} Value
	*/
    static Set = function(_statKey, _value) {
        var _stat = Get(_statKey);
        
        var _min = _stat.limit[0];
        var _max = _stat.final;
        
        _value = clamp(_value, _min, _max);
        
        _stat.actual = _value;
        
        return self;
    }
    
    /// @param stat_key
    /// @param value(mall_data)
    /// aumenta o disminuye el valor de una estadistica (limit[0] <- actual -> final)
    /// Aplica cambios realizados en el control.
	
	/**
	@desc	Aumenta o disminuye el valor de una estadistica en base a sus limites (limit[0] <- actual -> final)
			Devuelve el valor aumentado
	@param {String} StatKey
	@param {Real} Change
	@return {Real}
	*/
    static Basic = function(_statKey, _change, _type, _ignore = false) {
        var _stat = Get(_statKey), _applied = 0;

		// Si existe la estadistica
        if (!is_undefined(_stat) ) {
            switch (_type) {
                case MN.R:  _applied += _change;	break;
                case MN.P:  _applied += (_stat.actual * _change);	break;
            }
            
            Set(_statKey, _stat.final + _applied);
        }
        
        return _applied;
    }
    
	/**
	@desc	Regresa si un control esta afectando a esta estadistica y disminuye o aumenta el valor
	@param	{String}	StatKey
	@param	{Real}		Change
	@param	{Enum.MN}	Type
	*/
    static PassAffected = function(_statKey, _value, _type = MN.R) {
		var _statGroup = mall_get_stat(_statKey);
		var _affected  = _statGroup.__affectedBy;
		
        var _affNames = variable_struct_get_names(_affected);
    
        #region Argumento
        if (is_undefined(_value) ) {
            var _stat = Get(_statKey);
            _value = _stat.final;
            _type  = _stat .type;
        } else if (is_array(_value) ) {
            // Pasarse a sí mismo
            _type  = _value[MDATA .TYPE];
            _value = _value[MDATA.VALUE];
        }
        
        #endregion
        
        var i = 0; repeat(array_length(_affNames) ) {
            var _key = _affNames[i++];    
            var _in = _affected[$ _key];
            
            var _iVal = _in[MDATA.VALUE];
            var _iTyp = _in[MDATA .TYPE];
            
            
        }
    }
    
    /**
	@param {String} StatKey
	@returns {Struct.__PartyStatsComponent}
	*/
	static Get = function(_key) {
		return (self[$ _key] );
	}
    
    /// @param stat_key
    /// @param {number} base
    static SetBase = function(_statKey, _base) {
        if (argument_count < 3) {
            if (!Exists(_statKey) ) return self;
            
            var _part = Get(_statKey);
            
            _part.base = _base;
        } else {
            var i = 0; repeat(argument_count div 2) {
                var _key = argument[i++];
                var _bas = argument[i++];
                
                SetBase(_key, _bas); 
            }
        }
        
        return self;
    }
    
    /// @param condition
    /// @param stat_key*
    /// permite establecer la condicion para subir de nivel global o individual
    static SetCondition = function(_condition, _statKey) {
        // Comprobamos individual
        if (is_string(_statKey) && Exists(_statKey) ) {
            // Establecer scope y metodo individual
            var _stat = Get(_statKey);
            
            if (_stat.__solo) {
                _stat.__condition = method(undefined, _condition);    
            }
            
            return self;
        }
        
        __condition = method(undefined, _condition);
        return self;
    }
    
    /// @param start_method
    /// @param end_method    
    static SetMethod = function(_methodStart, _methodEnd) {
        __methodStart = method(undefined, _methodStart);
        __methodEnd   = method(undefined, _methodEnd);
        
        return self;
    }
    
    /// @param lvl
    /// @param stat_key
    static LevelSet = function(_lvl, _statKey) {
        // Individual
        if (is_string(_statKey) && Exists(_statKey) ) {     
            var _stat = Get(_statKey);    
                
            if (_stat.__solo) _stat.__lvl = _lvl;
            return self;
        }
        
        __lvl = _lvl;
        return self;
    }
    
    /// @param level_sum
    /// @param force*
    static LevelUp = function(_operate = 0, _force = false) {
        __lvl += _operate;
        
        var _keys = mall_get_stats();
        var _statGroup = mall_get_group(__group).__stats;  // Obtener estadisticas     
        
        // Ejecutar funcion al ejecutar
        __methodStart();
        
        var _return = [];   // Si hay que regresar algo
        
        var _filed = undefined;
        
        // Ciclar por las estadisticas
        var i = 0; repeat(array_length(_keys) ) {
            var _key   = _keys[i++];            // Obtener llave
            var _group = _statGroup[$ _key];    /// @is {MallStat} Obtener configuracion del grupo  
            
            // Si no existe evitar
            if (is_undefined(_group) ) continue;
            
            var _stat = Get(_key); // Obtener struct
            
            var _useLevel, _filed, _form;
            var _toMax = false;
            var _toMin = false;
            
            #region Comprobar si sube de nivel solo o no
            if (_stat.solo) {
                _stat.lvl += _operate;  // Actualizar nivel individual
                _useLevel = _stat.lvl;
                
                _filed = _stat.condition();
            } else {
                _useLevel = __lvl;
                // Solo una vez ya que EXP o otra estadistica puede pasar a 0 y cagar todos los demas niveles a lo estupido desgraciado
                if (_filed == undefined) _filed = __condition(); 
            }
            
            #endregion
            
            // Agregar valor
            var _finalAdd = _stat.final - _stat.upper;
            
            // Si se cumplen las condiciones para subir de nivel
            if (_force || _filed) {
                // Evitar errores al no establecer base
                if (!is_undefined(_stat.base) ) {
                    _form = _group.__lvlMethod(_useLevel, _stat, self);
                    
                    var _toMax = _stat.toMax.Work();
                    var _toMin = _stat.toMin.Work();
                    
                    if (_toMax  && !_toMin) _stat.actual = _form + _finalAdd;   // Poner en el valor final
                    if (!_toMax &&  _toMin) _stat.actual = _stat.limit[0];
                    
                    // Actualizar ultimo valor
                    if (!__start) {
                        // Poner el nivel anterior
                        if (!_toMin) {
                            _stat.last = _group.__lvlMethod(max(1, _useLevel - 1), _stat, self); // Evitar menores a 1
                        } else {
                            _stat.last = _stat.limit[0];
                        }
                    } else {
                        _stat.last = _stat.upper;  
                    }
                } else {
                    // Si no posee base se usará upper
                    _form = _stat.upper; 
                }
                
                // Primera subida de nivel
                var _test = !_toMax && !_toMin;
                
                // Cambiar upper, final y actual
                SetUpper(_key, _form, (_test && !__start) );
                
                // Mostrar los valores en el debugger
                if (PARTY_STAT_SHOWMESSAGE) show_debug_message(_key + ": " + string(_form) );
                
                // Poner valores para regresar
                array_push(_return, {key: _key, actual: _stat.actual, upper: _stat.upper, last: _stat.last, final: _stat.final} ); 
            }
        }
        
        // Ejecutar funcion al terminar de subir de nivel
        __methodEnd();
        
        // Se cumplio la primera subida de nivel
        __start = true;
        
        return _return;
    }
    
    /// @param stat_key
    /// @returns {bool}
    static Exists = function(_statKey) {
        return (variable_struct_exists(self, _statKey) );
    }
    
    /// @param stat_key
    /// @param value
    static IsAbove = function(_statKey, _value) {
        return (Get(_statKey).actual > _value);
    }
    
    /// @param stat_key
    /// @param value
    static IsBelow = function(_statKey, _value) {
        return (Get(_statKey).actual < _value);
    }
    
    // Motivos de debug
    /// @returns {string}
    static Print = function() {
        static _printMethod = function(_value, _type) {
            var _in = (_type == MN.P) ? string(_value) + "%" : string(_value);
            
            return (_in + "\n") ;
        }
        
        var _print = "------------------------------- \n";
        var _keys = mall_get_stats();
        
        var i = 0; repeat(array_length(_keys) ) {
            var _key  = _keys[i++];
            var _stat = Get(_key), _type = _stat.type;
            
            // Nombre
            _print += _key + "\n";
            
            if (_type == MN.R) {_print += "type: Real \n";} else {_print += "type: Percent \n"; }
            
            _print += "final.value:  " + _printMethod(_stat.final , _type);
            _print += "upper.value:  " + _printMethod(_stat.upper , _type);
            _print += "actual.value: " + _printMethod(_stat.actual, _type);
        }
        
        show_debug_message(_print);
        return _print;
    }
    
    #endregion
    
    Init();
}

/** 
@param {String} Key
@param {Struct.MallStat} Stat
@ignore 
*/
function __PartyStatsComponent(_key, _stat) constructor {
	__key = _key;	// Track
	__displayKey = _stat.__displayKey;	// Llave para obtener el nombre de display
	__displayTextKey = _stat.__displayTextKey;	// Textos extras
	
	__return = method(self, _stat.__visualize);	// Como devolver sus valores
	
	lvl  = 1;	// Nivel de la estadistica si se usa individualmente
	solo = _stat.__lvlSingle	// Si sube de nivel individualmente
	
	base = undefined;	// Se establece su valor despues
	type = _stat.__init[0];
	
	// Valores que posee
	final = _stat.__init[1];	// Valor de la estadistica que solo recibe cambios con los equipamientos ("upper" + equipamientos)
	upper = final;	// Valor de la estadistica sin ningun cambio ("actual" maximo)
	
	actual = final;	// Valor de la estadistica que va cambiando con el tiempo (Unico que se puede cambiar)
	last   = final;	// Valor anterior de subir de nivel
	
	toMax = _stat.__toMax.Copy();	// Copiar contadores para que no hayan conflictos
	toMin = _stat.__toMin.Copy();	// Copiar contadores para que no hayan conflictos
	
	limit = _stat.__limit;	// Referencia a los limites de la configuracion
	condition = undefined;	// Condicion que debe cumplir para subir de nivel
}

/*
    start: // Valor a reiniciar
    added: // Tomar valor inicial
    same : // Si acepta el mismo control varias veces
    box: // donde se guardan los effectos y controladores
    
    _key_: // Track
*/

/// @param {string} group_key
/// @param {bool} stats_unique?
/// @param {bool} states_unique?
/// @param stat_context
/// @param part_context
function PartyControl(_groupKey, _statsUniq = false, _statesUniq = true, _statContext, _partContext) : MallComponent(_groupKey) constructor {
    __group = _groupKey;    // Key del grupo que pertenece (evitar crear referencias)
    __statsUniq = _statsUniq ;  // Si se permiten multiples ben/des a las estadisticas
    __stateUniq = _statesUniq;  // Si se permiten más de un mismo estado
    
    __weakStat = weak_ref_create(_statContext);  // referencia a las estadisticas
    with (_statContext) __weakControl = weak_ref_create(other); // Crea referencia en las estadisticas al control
    
    __weakPart = weak_ref_create(_partContext);  // referencia a las partes
    
    #region Metodos
    static Init = function() {
        var _group = mall_get_group(__group);
        
        #region Stat
        var _statMaster = mall_get_stats(); // Obtener estadisticas    
        var _statGroup  = _group.__stats;
            
        var i = 0; repeat(array_length(_statMaster) ) {
            var _key  = _statMaster[i++];  // Obtener llaves    
            var _stat = _statGroup[$ _key];    
                
            // Si existe la estadistica en el grupo
            if (!is_undefined(_stat) ) {
                var _start  = _stat.__init;
                
                var _value = _start[0];
                var _type  = _start[1];
                var _div   = _start[2];
                
                variable_struct_set(self, _key, {
                    _key_: _key,    // Track
                    type : _type,   // El tipo de numero
                    
                    start: _value, // Valor que reinicia este componente
                    affected: false,    // Si esta siendo afectado
                    update: [_value, _div, false],  // Valores que varian en el tiempo [real, percentual, booleano]
                    
                    same : false,  // Si acepta el mismo control varias veces
                    
                    box: (__statsUniq) ? undefined : []
                });    
            }    
        }
        
        #endregion
        
        #region State
        var _stateMaster = mall_get_states();
        var _stateGroup  = _group.__states;
        
        var i = 0; repeat(array_length(_statMaster) ) {
            var _key   = _statMaster[i++];  // Obtener llaves    
            var _state = _stateGroup[$ _key];    
                
            // Si existe el estado en el grupo
            if (!is_undefined(_state) ) {
                // Por sia
                _start  = _state.__init;
                
                _value = _start[0];
                _type  = _start[1];
                _div   = _start[2];
                
                variable_struct_set(self, _key, {
                    _key_: _key, // Track
                    type : _type, // El tipo de numero (generalmente booleano)

                    start: _value, // Valor a reiniciar
                    affected: false,    // Si esta siendo afectado
                    update: [_value, _div, false],  // Valores que varian en el tiempo [real, percentual, booleano]
                    
                    same : false,   // Si acepta el mismo control varias veces
                    
                    box: (__stateUniq) ? undefined : []
                });    
            }    
        }        
        #endregion
    }
    
    /// @param key
    /// @param value
    /// @param type*
    /// Establece un nuevo valor en "update" con el tipo de numero default o diferente
    static Set = function(_key, _value, _type) {
        var _cnl = Get(_key);
        
        if (_type == undefined) _type = _cnl.type;
        
        _cnl.update[_type] = _value;
        
        return self;
    }
    
    /// @param key
    static Get = function(_key) {
        return (self[$ _key] );
    }
    
    /// @param key
    /// @param operate
    /// @param number_type*
    static Operate = function(_key, _oper, _type) {
        var _cnl = Get(_key);
        
        if (_type == undefined) _type = _cnl.type;
        
        switch (_type) {
            case MN.B: _cnl.update[_type]  = abs(_oper); break;
            default:   _cnl.update[_type] += _oper; break;
        }
        
        return self;
    }
    
    /// @param key
    static Reset = function(_key) {
        var _cnl = Get(_key);
        
        _cnl.update[MN.R] = _cnl.start; // El valor real
        _cnl.update[MN.P] = _cnl.start / MALL_NUMBER_DIV;   // El valor porcentual
        _cnl.update[MN.B] = _cnl.start; // El valor boleano

        return self;
    }
    
    static ResetAll = function() {
        var _names = mall_get_stats();
        var i = 0; repeat(array_length(_names) ) {Reset(_names[i++] ); }
        
        var _names = mall_get_states();
        var i = 0; repeat(array_length(_names) ) {Reset(_names[i++] ); }
        
        return self;
    }
    
    /// @param effect
    /// Agrega un efecto al control que afecta (stat/state/action)
    static AddEffect  = function(_effect) {
        if (is_struct(_effect) ) {    
            var _key = _effect.__key;  // Obtener stat/state/action a la que afecta    
            var _cnl = Get(_key);   // Obtener control de este estado/estadistica
            
            // Existe
            if (!is_undefined(_cnl) ) { 
                // Agregar contenido a la box
                var _box = _cnl.box;
                
                // Permite varios
                if (is_array(_box) ) {
                    // No permite repetidos
                    if (!_cnl.same) {
                        var i = 0; repeat(array_length(_box) ) {     
                            var _in = _box[i++];
                            
                            if (_in.id == _effect.id) return false;
                        }
                    }
                    // Agregar efecto
                    array_push(_box, _effect);
                } else {    
                    // Solo permite 1
                    Reset(_key); // Reiniciar valores
                    
                    _cnl.box = _effect;
                }
                
                _cnl.affected = true;   // Indicar que esta siendo afectado
                
                var _value = _effect.__value[0];
                var _type  = _effect.__value[1];
                
                // Aumentar los valores de este control
                Operate(_key, _value, _type);
            }
        }
            
        return self;
    }
    
    /// @param key
    /// @param control_id*
    static DeleteEffect = function(_key, _id) {
        var _cnl = Get(_key);    
        
        if (!is_undefined(_cnl) ) {
            var _box = _cnl.box;    
            
            if (is_array(_box) ) {
                // Buscar
                var i = 0; repeat(array_length(_box) ) {
                    var _in = _box[i++];
                    
                    if (_in.id == _id) break;
                }
                
                // Eliminar
                array_delete(_box, i, 1);
                
                // Revisar si aun posee efectos
                if (array_length(_box) <= 0) {_cnl.affected = false; }
            } else {
                var _in = _cnl.box;

                // Solo 1
                _cnl.box = undefined;
                _cnl.affected = false;
            }
            
            // Quitar valores de este efecto
            Operate(_in.__key, -_in.__value[0], _in.__value[1] );            
        }
        
        return self;
    }
    
    /// @param key
    static UpdateControl = function(_key) {
        var _return = [];
        var _cnl, _box;
        
        #region Recursivo
        if (!is_struct(_key) ) {
            _cnl = Get(_key);    
            _box = _cnl.box;
        } else {
            _box = _key;
            _cnl = Get(_box._key_);
        }
        
        #endregion
        
        // Solo 1
        if (!is_undefined(_box) ) {
            var _turns = _box.__turns;
            var _work  = _turns.Work();
            
            // Aun quedan turnos
            if (_work) {
                var _update = _turns.__count; // Nuevo valor    
                
                // Ejecutar funcion de update
                _box.__methodUpdate(self, _update);
            } else {    // Terminaron los turnos
                var _update = _turns.__count; // Nuevo valor    
                
                // Ejecutar funcion de final
                _box.__methodEnd(self, _update);
                
                // Poner valor original
                _cnl.added -= _cnl.start;
            }
            
            return [_cnl.added, _work];
            
        } else if (is_array(_box) ) {
            var i = 0; repeat(array_length(_box) ) {    
                var _in = _box[i++];   
                
                array_push(_return, UpdateControl(_in) );    
            }
            
            return _return;
        }
    }
    
    static UpdateAll = function() {
        var _stats  = mall_get_stats ();
        var _states = mall_get_states();
        
        UpdateControl (_stats);
        UpdateControl(_states);
        
        return self;
    }
    
    /// @param key
    static Exists = function(_key) {
        return (variable_struct_exists(self, _key) );
    }
    
    #endregion
    
    Init();
}


/// @param {string} group_key
/// @param {PartyStats} stat_context
function PartyParts(_groupKey, _partyStat) : MallComponent(_groupKey) constructor {
    __group = _groupKey;
    __weakStats = weak_ref_create(_partyStat);  // Referencia a las estadisticas    
        
    #region Metodos    
    static Init = function() {
        var _keys  = mall_get_parts();    
        var _group = mall_get_group(__group).__parts;   // Obtener estadistica
        
        var i = 0; repeat(array_length(_keys) ) {
            var _key  = _keys[i++]; // Llave
            var _part = _group[$ _key];    // obtener configuracion de parte
            
            if (!is_undefined(_part) ) {
                #region Crear Parte
                var _start  = _part.__active;   // Es un array
                var _number = max(1, _part.__numbers);  // Repeticiones de este objeto no puede ser 0
                
                var _itemtype = _part.__items;
                var _max = _part.__equipMax;            // Cuantos objetos puede llevar
                
                // Crear entrada en el control de parte (en comp se almacenan las partes iguales)
                var _inMaster = {
                    _key_: _key,
                    _max_: _number, // Repeticiones
                    comp: [],   // Donde se almacenan las partes 
                    
                    ypow: 1,    // Poder de la parte al usarlo con un equipo
                    npow: 1,    // Poder de la parte al no usarlo con equipo
                    
                    item: _itemtype,    // Que tipo de objetos puede equipar
                    itemtype:  variable_struct_get_names(_itemtype),   // Que objeto es capaz de equipar esta parte (type: [subtype] ) Ciclar principalmente

                    damage: noone   // No usar por mientras
                }
                
                // Crear control de parte
                variable_struct_set(self, _key, _inMaster);
                
                var _inArray = _inMaster.comp;
                
                var j = 0; repeat(_number) {
                    var _push = {
                        _number_: j, // Indice de creacion
                        
                        equipped: array_create(_max, undefined),    // Donde se almacenan los objetos que lleva
                        previous: array_create(_max, undefined),    // Objeto anterior que se llevo
                        
                        usable: _start[j], // Si puede usarse al inciar o no
                        _max_: _max,    // Cuantos objetos puede equipar al mismo tiempo
                    };
                    
                    array_push(_inArray, _push);
                    ++j;
                }
                #endregion
            }
        }    
    }
    
    /// @param part_key
    /// @param item_key
    /// @param number*
    /// @param index*
    /// @returns {bool} false: no se logro equipar el objeto | true: equipado
    static Equip = function(_key, _itemKey, _number = 0, _index = 0) {
        var _part = Get(_key);
        
        if (!is_undefined(_part) ) {
            //var _item = A crear objetos como idiota!!
            var _item = pocket_data_get(_itemKey);
            
            // Comprobar que permita esta parte
            if (_part.item[$ _item.__subtype] == undefined) return false;
            
            // Evitar errores al salir del limite
            _number = clamp(_number, 0, _part._max_);
            var _in = _part.comp[_number];    
            
            // Si puede usar esta parte
            if (_in.usable) {
                // Evitar que se salga de los limites
                _index = clamp(_index, 0, _in._max_);    
                    
                _in.previous[_index] = _in.equipped[_index];    
                _in.equipped[_index] = _itemKey;
                
                ApplyStat(_key);
                
                return true;
            }
        }
        
        return false;
    } 
    
    /// @param part_key
    /// @param number*
    /// @param index*
    /// @rreturns {bool} 
    static Desequip = function(_key, _number = 0, _index = 0) {
        var _part = Get(_key);        
        
        // Si existe el objeto
        if (!is_undefined(_part) ) {
            // Evitar errores
            _number = clamp(_number, 0, _part._max_);
            var _comp = _part.comp[_number];
            
            // Guardar equipado
            _index = clamp(_index, 0, _in._max_);
            var _eqp = _comp.equipped[_index];
            
            _comp.equipped[_index] = undefined;
            _comp.previous[_index] = _eqp;
            
            ApplyStat(_key);
            
            return true;
        }
        
        return false;
    } 
    
    /// @param part_key
    /// @param part_number*
    /// @param part_index*
    /// @returns {bool}
    static IsEquiped = function(_key, _number = 0, _index = 0) {
        var _part = Get(_key);
        
        _number = clamp(_number, 0, _part._max_);
        var _in = _part.comp[_number];
        
        _index = clamp(_index, 0, _in._max_);
        return !(_inPart.equipped[_index] == undefined);
    }
    
    /// @param part_key
    /// @returns {struct}
    static ApplyStat = function(_key) {
        // Poder pasar la parte sin tener que buscarlo
        var _part = is_string(_key) ? Get(_key) : _key; 
     
        #region Errores
        if (is_undefined(_part) || weak_ref_alive(__weakStats) ) {
            __mall_trace("(ApplyStat) No existe la parte o no hay referencia a las estadisticas");
            
            return undefined;
        }
        
        #endregion
        
        var _item, _stats;
        
        var _myStats = __weakStats.ref;
        var _bonus = 1;
        
        // Obtener cuantas mismas partes existen.
        var _components = _part.comp;
        var _returnStat = {};
        
        var i = 0; repeat(array_length(_components) ) {
            var _inPart = _components[i++];
            var _inEquipped = _inPart.equipped;
            var _inPrevious = _inPart.previous;
            
            var j = 0; repeat(array_length(_inEquipped) ) {
                var _equipped = _inEquipped[j];
                var _previous = _inPrevious[j++];
                
                #region Revisar si hay equipo
                if (!is_undefined(_equipped) ) {
                    _item = pocket_data_get(_equipped); 
                    
                } else if (!is_undefined(_previous) ) {
                    _item = pocket_data_get(_previous); 
                    _item.__returnInv = true; 
                    
                } else {
                    _item = pocket_data_get(POCKET_DATABASE_DUMMY); 
                }
                
                #endregion
                
                _bonus = _part.item[$ _item.__subtype] ?? mall_data(0, MN.R);  // Obtener bono
                
                var _bValue = _bonus[MDATA.VALUE];
                var _bType  = _bonus[MDATA .TYPE];
                
                _stats = _item.GetStats(); // Referencia a las estadisticas.
                
                // Sumar o restar
                var _statsNames = variable_struct_get_names(_stats);
                var _inBonus;
                
                var i = 0; repeat(array_length(_statsNames) ) {
                    var _statName = _statsNames[i++];
                    
                    var _mData  = _myStats[$ _statName];  // Obtener propiedades de esta stat
                    var _uValue = _mData.upper;  // Valor upper
                    var _uType  = _mData.type ;  // Tipo de numero                    
                    
                    var _isData = _stats[$ _statName];  // Obtener data del objeto
                    var _isValue = _isData[MDATA.VALUE], _inItem = _isValue;
                    var _isType  = _isData[MDATA .TYPE]; 

                    #region Math
                    var _temp = 0;
                    
                    if (_isType == MN.R) {
                        if (_bType == MN.R) {
                            _inItem += _bValue; 
                        } else {
                            _inItem += (_bValue * _isValue) / MALL_NUMBER_DIV; 
                        }
                    } else if (_isType == MN.P) {
                        _inItem = (_uValue * _isValue) / MALL_NUMBER_DIV;   
                        
                        if (_bType == MN.P) {
                            _inItem += (_inItem * _uValue) / MALL_NUMBER_DIV;
                        } else {
                            if (_uType == MN.R) {_temp = _bValue; } else {_temp = (_uValue * _bValue) / MALL_NUMBER_DIV; }
                        }
                    }
                    
                    #endregion
                    
                    // Realizar suma final
                    if (_uType == MN.R) {
                        _mData.final += __mall_stat_rounding(_inItem);
                    } else if (_uType == MN.P) {
                        _mData.final += __mall_stat_rounding( (_inItem * _uValue) / MALL_NUMBER_DIV);
                    }
                    
                    // Pequeño extra.
                    _mData.final += __mall_stat_rounding(_temp);
                }
            }
        }
        
        return (_returnStat);
    } 

    /// @returns {array}
    static ApplyStatAll = function() {
        var _keys  = mall_get_parts();
        var _array = [];
        
        var i = 0; repeat(array_length(_keys) ) {
            var _key  = _keys[i++]; // Llave
            array_push(_array, ApplyStat(_key) );
        }
        
        return (_array);
    }
    
    /// @param part_key
    static Get = function(_key) {
        return (self[$ _key] );
    } 
    
    /// @param part_key/part_array
    /// @param item_key
    /// @param filter*
    /// @returns {struct}
    /// Compara las estadisticas del objeto actual con otro objeto obteniendo la diferencia en estadisticas
    static CompareItem = function(_key, _itemKey, _filter) {
        // Filtro
        /// @returns {array}
        if (_filter == undefined) _filter = function(_x, _y, name, i) {    
            var in  = string(_x[MDATA.VALUE] - _y[MDATA.VALUE] );
            var str = "";
            
    		if (in > 0) {str = ["+" + in, c_green]; } else 
    		if (in < 0) {str = ["-" + in, c_red  ]; } else {
    		    str = ["=" + in, c_white]; }
    		
    		// Si ambos son porcentajes agregar
    		if (_x[MDATA.TYPE] == MN.P && _y[MDATA.TYPE] ) {
    		    str[0] += "%"; 
    		}
    		
    		return str;
        }
        
        var _part, _partNumber, _partIndex;
        var _item1, _item2;
        var _stat1, _stat2;
        
        var _bon1, _bon2;
        
        #region Soporte number y index
        if (is_array(_key) ) {
            _partNumber = _key[1];
            _partIndex  = _key[2];
            _key = _key[0];
        } else {
            _partNumber = 0;
            _partIndex  = 0;
        }

        #endregion

        _part = Get(_key);
        
        // Si existe la referencia a las estadisticas y existe la parte
        if (weak_ref_alive(__weakStats) && !is_undefined(_part) ) {
            // Referencia
            var _myStat = __weakStats.ref;
            
            var _comp = _part.comp[_partNumber];
            var _inComp = _comp.equipped[_partIndex] ?? POCKET_DATABASE_DUMMY;  // Obtener el objeto equipado si posee uno
            
            // Obtener objeto equipado y objeto a comparar.
            _item1 = pocket_data_get(_inComp);
            _bon1  = _part.item[$ _item1.__subtype] ?? 0;   // Obtener bono 1
            _stat1 = _item1.GetStats();
            
            var _b1Val  = _bon1[MDATA.VALUE];
            var _b1Type = _bon1[MDATA .TYPE];
        
            _item2 = pocket_data_get(_itemKey);
            _bon2  = _part.item[$ _item2.__subtype] ?? 0;   // Obtener bono 2
            _stat2 = _item2.GetStats();
            
            var _b2Val  = _bon2[MDATA.VALUE];
            var _b2Type = _bon2[MDATA .TYPE];
                        
            var _difference = {};
            var _statsNames = mall_get_stats();

            var i = 0; repeat(array_length(_statsNames) ) {
                #region Diferencia entre 2 objetos
                var _statKey = _statsNames[i++];      
                
                var _iData1 = _stat1[$ _statKey] ?? mall_data(0, MN.R);
                var _iData2 = _stat2[$ _statKey] ?? mall_data(0, MN.R);
                
                var _iType1 = _iData1[MDATA.TYPE];
                var _iType2 = _iData1[MDATA.TYPE];
                
                var _iVal1 = _iData2[MDATA.VALUE];
                var _iVal2 = _iData2[MDATA.VALUE];
                
                #region Porcentual
                
                    #region 1° Item
                switch (_iType1) {  
                    case MN.R:
                        if (_b1Type == MN.R) {
                            _iVal1 += _b1Val;
                        } else if (_b1Type == MN.P) {
                            _iVal1 += (_iVal1 * _b1Val) / MALL_NUMBER_DIV;    
                        }
                    
                        break;
                        
                    case MN.P:
                        var _myVal  = _myStat[$ _statKey].upper[MDATA.VALUE];
                        var _myType = _myStat[$ _statKey].upper[MDATA. TYPE];
                    
                        _iVal1 = (_myVal * _iVal1) / MALL_NUMBER_DIV;
                        
                        if (_b1Type == MN.P) {
                            _iVal1 += (_iVal1 * _myVal) / MALL_NUMBER_DIV; 
                                
                        } else {
                            if (_myType == MN.R) {_iVal1 += _b1Val; } else {_iVal1 += (_myVal * _b1Val) / MALL_NUMBER_DIV; }
                        }

                        break;
                }
                    #endregion

                    #region 2° Item
                switch (_iType2) {  
                    case MN.R:
                        if (_b2Type == MN.R) {
                            _iVal2 += _b2Val;
                        } else if (_b1Type == MN.P) {
                            _iVal2 += (_iVal2 * _b2Val) / MALL_NUMBER_DIV;    
                        }
                    
                        break;
                        
                    case MN.P:
                        var _myVal  = _myStat[$ _statKey].upper[MDATA.VALUE];
                        var _myType = _myStat[$ _statKey].upper[MDATA. TYPE];
                    
                        _iVal2 = (_myVal * _iVal2) / MALL_NUMBER_DIV;
                        
                        if (_b2Type == MN.P) {
                            _iVal2 += (_iVal2 * _myVal) / MALL_NUMBER_DIV; 
                                
                        } else {
                            if (_myType == MN.R) {_iVal2 += _b2Val; } else {_iVal2 += (_myVal * _b2Val) / MALL_NUMBER_DIV; }
                        }

                        break;
                }
                    #endregion

                #endregion
                
                // Realizar diferencia entre valores
                _difference[$ _statKey] = _filter([__mall_stat_rounding(_iVal1), _iType1], [__mall_stat_rounding(_iVal2), _iType2], _statKey, i);
                
                #endregion
            } 
    
            return (_difference );
        }
        
        return undefined;
    }
    
    /// @param part_key/part_array
    /// @returns {struct}
    /// Regresa las estadisticas sin este objeto equipado.
    static CompareNoItem = function(_key, _number = 0, _index = 0) {
        // Desequipar objeto
        var _part = Get(_key);        
        
        if (is_undefined(_part) ) return undefined;
        
        var _comp = _part.comp[_number];
        var _eqp  = _comp.equipped[_index] ?? POCKET_DATABASE_DUMMY;

        var _item = pocket_data_get(_eqp);
        
        var _bonus = _part.item[$ _item.__subtype] ?? mall_data(0, MN.R);
        
        var _bVal = _bonus[MDATA.VALUE];
        var _bTyp = _bonus[MDATA .TYPE];
        
        var _itemStats = _item.GetStats();
        
        var _difference = {};
        var _statsNames = mall_get_stats();
    
        var _myStats = __weakStats.ref;
        
        var i = 0; repeat(array_length(_statsNames) ) {
            var _statKey = _statsNames[i++]; 
            
            // obtener estadistica
            var _mData = _myStats[$ _statKey];
            var _mTyp  = _mData.type;
            
            var _iData = _itemStats[$ _statKey];
            
            // Del objeto
            var _iVal = _iData[MDATA.VALUE];
            var _iTyp = _iData[MDATA .TYPE];
            
            #region Math
            switch (_iTyp) {
                case MN.R:
                    if (_bTyp == MN.R) {
                        _iVal += _bVal;
                    } else if (_bTyp == MN.P) {
                        _iVal += (_iVal * _bVal) / MALL_NUMBER_DIV;
                    }
                    
                    break;
                
                case MN.P:
                    var _val = _mData.upper;
                
                    _iVal = (_iVal * _val) / MALL_NUMBER_DIV;
                    
                    if (_bTyp == MN.P) {
                        _iVal += (_iVal * _val) / MALL_NUMBER_DIV; 
                            
                    } else {
                        if (_mTyp == MN.R) {_iVal += _bVal; } else 
                        if (_mTyp == MN.P) {_iVal += (_bVal * _val) / MALL_NUMBER_DIV; }
                    }
                    
                    break;
            }
            #endregion
            
            // obtener final para restar valores
            var _temp = _mData.final;
            
            if (_mTyp == MN.R) {
                _temp -= __mall_stat_rounding(_iVal);
            } else if (_mTyp == MN.P) {
                _temp -= __mall_stat_rounding((_iVal * _temp) / MALL_NUMBER_DIV);
            }
            
            _difference[$ _statKey] = _temp;
        }
        
        return (_difference);
    }
    
    #endregion
    
    
    Init();
}


