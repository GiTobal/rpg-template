/** @type {Struct.MallStorage} */
global.__mall_storage = new MallStorage();
global.__mall_dummy_method = function(_temp = 0) {return _temp; }  // Metodo sin nada para no crear infinitos

/** @type {Struct.MallStorage} */
#macro MALL_STORAGE global.__mall_storage

#macro MALL_NUMBER_DIV 100
#macro MALL_DUMMY_METHOD global.__mall_dummy_method

// Si un dato hay que tratarlo como real, porcentaje o booleano
enum MN {R, P, B, _SIZE_}
enum MDATA {VALUE, TYPE, DIV, _SIZE_}

/** 
	@desc	Se almacena los nombres de las partes de mall globalmente
			luego en un grupo se puede seleccionar especificamente que parte se necesita
	@return {Struct.MallStorage}
*/
function MallStorage() constructor {	
	__groupsMaster	 =	new Bucket();
	__statsMaster	 =	new Bucket();
	__statesMaster	 =	new Bucket();
	
	__partsMaster	 =	new Bucket();
    __elementsMaster =	new Bucket();
	
	// Almacena los tipos de objetos como Buckets para almacenar luego sus sub-tipos
    __itemsMaster   = new Bucket();
	
	// Almacena los tipos de acciones y sus subtipos
    __actionsMaster = new Bucket();
    
    #region Metodos
	/// @param {String} _key
	static GetGroup = function(_key)	{return (__groupsMaster ).Get(_key); }
	/// @param {String} _key
    static GetStat  = function(_key)	{return (__statsMaster  ).Get(_key); }
    /// @param {String} _key
    static GetState = function(_key)	{return (__statesMaster ).Get(_key); }
	/// @param {String} _key
    static GetPart  = function(_key)	{return (__partsMaster  ).Get(_key); }
    /// @param {String} _key
    static GetElement = function(_key)	{return (__elementsMaster ).Get(_key); }
	
    #endregion
}

/**
	@desc Crea componente de Mall (sistema RPG)
	@param {String} [Key]
*/
function MallComponent(_key = "") constructor {
    // __is = asset_get_index(instanceof(self) );
    __key  = _key;	// Llave propia
    __from =   "";	// Llave de a quien pertenece
    
    // -- Display
    __displayKey  = "";	// Llave para traduccion 
    __displayName = "";	// Que nombre mostrar en el display
    
    __displayText    = [];	// textos variados que el usuario le da su uso.
    __displayTextKey = [];	// llaves de estos textos variados    
    
    __displayIgnore = false;	// Si es ignorado en las funciones de GUI
    
    #region Metodos
        
    /** @param {String} _key */
    static SetKey  = function(_key) {__key  = _key; return self;}
    
    /** @param {String} _key */
    static FromKey = function(_key) {__from = _key; return self;}
	
	/**
		@desc Establece las llaves de traduccion que utiliza Lexicon
		@param {String} _displayKey
		@param {String} [TextKey...]
	*/
    static SetTranslate = function(_displayKey) {
        // Si es undefined se establece que la llave de display es la misma que la llave del componente
		_displayKey ??= __key;
		
		__displayKey  = _displayKey;
		__displayName = lexicon_text(__displayKey);
		
		var i = 0; repeat (argument_count) array_push(__displayTextKey, argument[i++] ); 

		return self;
	}

	#endregion
}

/// @param {string} key
/// @param ...
/// @desc Crear un (o varios) parts globalmente
function mall_create_parts() {
    var _parts = MALL_STORAGE.__partsMaster;

    var i = 0; repeat(argument_count) _parts.Set(argument[i++] );
}

/// @param {string} key
/// @param initialize
/// establece el grupo.
function mall_create_group(_key, _init = false) {
    var _group = new MallGroup(_key, _init);
	
    MALL_STORAGE.__groupsMaster.Set(_key, _group);
    
    MALL_GROUP = _group;
}

//--- GLOBAL
/**	@param	{Real} _value	@param	{Real} _type	@return	{Array}	*/
function mall_data(_value, _type) {
    return [
		_value,
		_type,
		_value / max(1, MALL_NUMBER_DIV)
	];
}

/// @param {Mixed} _str
function __mall_trace(_str) {
    show_debug_message("MALL: " + string(_str) );
}

#region Actions
/// Crea una accion y un lugar donde se almacenan sus sub-tipos
function MallAction() : MallComponent() constructor {
    __subaction = {};
    
    #region Metodos
    /// @param sub-action
    /// @param value
    static Set = function(_sub, _val) {
        __subaction[$ _sub] = _val;
        
        return self;
    }
    
    #endregion
}

/// @param action_key
/// @param sub_actions
/// @param ...
function mall_create_action(_key) {
    var _action = new MallAction(); /// @is {MallAction}
    
    // Rellenar sub-tipos
    var i = 1; repeat (argument_count - 1) {_action.Set(argument[i++] ); }
    MALL_STORAGE.__actionsMaster.Set(_key, _action);
}

/// @param item_key
/// Obtiene los sub-acciones de esta accion
function mall_get_action(_key) {
    return (MALL_STORAGE.__actionsMaster.Get(_key) );
}

#endregion

#region Items
function MallItems() : MallComponent() constructor {
    __subitem = {};
    
    #region Metodos
    /// @param sub-item
    /// @param value    
    static Set = function(_sub, _val) {
        __subitem[$ _sub] = _val;
        return self;
    }
    
    #endregion
}

/// @param item_key
/// @param sub_item
/// @param ...
function mall_create_item(_key) {
    var _item = new MallItems();    
    var i = 1; repeat (argument_count - 1) {_item.Set(argument[i++] ); }
    MALL_STORAGE.__itemsMaster.Set(_key, _item);
}

/// @param item_key
function mall_get_item(_key) {
    return (MALL_STORAGE.__itemsMaster.Get(_key) );    
}

/// @param item_subtype
/// @desc Regresa el tipo a partir del subtipo.
function mall_search_item(_subtype) {
    var _itemsMaster = MALL_STORAGE.__itemsMaster;    
    
    repeat (_itemsMaster.Size(true) ) {
        var _in = _itemsMaster.Next();    
        
        if (variable_struct_exists(_in.__subitem, _subtype) ) {
            var _actual = _itemsMaster.Actual();
            return _actual;
        }
    }
}


#endregion



