/// @type {Struct.MallGroup}
global.__mall_actual_group = new MallGroup("__backup");
/// @type {Struct.MallGroup}
#macro MALL_GROUP global.__mall_actual_group

/**
@desc	Un grupo es como debe funcionar los componentes guardados (MallStorage)
		entre sí. Esto sirve para diferenciar clases, especies o razas en distintos rpg (Humanos distintos a Orcos por ejemplo)
@param	{String} _key
@param	{Bool} [_init]
@return {Struct.MallGroup}
*/
function MallGroup(_key, _init = false) constructor {
    __key = _key; // Nombre del grupo (Humano, Guerrero, etc)
    
    // Se utiliza un struct para guardar los datos y acceder rapidamente
    __stats    = undefined;
    __states   = undefined; 
    __elements = undefined; 
    __parts    = undefined; 
    
    #region Metodos
    
    /// @param {Array.String} _deleteArray
	/// @desc Permite eliminar estadisticas de la cubeta, para luego rellenarlo con la estructura de estadisticas
    static InitStats  = function(_deleteArray) {    
        var _keys = mall_get_stats();   // Obtener llaves
        var _content = {};
        
        // Eliminar llaves
        var i = 0; repeat (array_length(_keys) ) {
            var _key = _keys[i++];
            _content[$ _key] = new MallStat(_key);
        }
        
        __stats = _content;
    }
    
    /// @param {Array.String} _deleteArray
    static InitStates = function(_deleteArray) {
        var _keys = mall_get_states();
        var _content = {};
        
        var i = 0; repeat (array_length(_keys) ) {
            var _key = _keys[i++];    
            _content[$ _key] = new MallState(_key);
        }
    
        __states = _content;
    }
    
    /// @param {Array.String} _deleteArray
    static InitParts  = function(_deleteArray) {
        var _keys = mall_get_parts();
        var _content = {};
        
        var i = 0; repeat (array_length(_keys) ) {
            var _key = _keys[i++];
            _content[$ _key] = new MallPart(_key);
        }
        
        __parts = _content;
    }
    
    /// @param {Array.String} _deleteArray
    static InitElements = function(_deleteArray) {
        var _keys = mall_keys_element();
        var _content = {};
        
        var i = 0; repeat (array_length(_keys) ) {
            var _key = _keys[i++];    
            _content[$ _key] = new MallElement(_key);
        }
        
        __elements = _content;
    }

    static Init = function() {
        InitStats ();
        InitStates();  
        InitParts ();
        InitElements();
    }
    
		#region Getters
	/** @return {Struct.MallStat} */
	static GetStat  = function(_key) {return __stats [$ _key]; }
	/** @return {Struct.MallState} */
	static GetState = function(_key) {return __states[$ _key]; }
	/** @return {Struct.MallElement} */
	static GetPart  = function(_key) {return __parts [$ _key]; }
	
	/** @return {Struct.MallElement} */
	static GetElement = function(_key) {return __elements[$ _key]; }
	
	#endregion
	
    #endregion
    
    // Iniciar
    if (_init) Init();
}

/**
@param {String} _group_key
@return {Struct.MallGroup}
*/
function mall_get_group(_group_key) {
    return (MALL_STORAGE.GetGroup(_group_key) ); 
}

/** @param {String} _group_key */
function mall_set_group(_group_key) {
    MALL_GROUP = MALL_STORAGE.GetGroup(_group_key);
}

/**
	@param {Array.String} [_delete_stats]
	@param {Array.String} [_delete_states]
	@param {Array.String} [_delete_parts]
	@param {Array.String} [_delete_elements]
	@desc Inicia todo los componentes del grupo. Permite eliminar caracteristicas
*/
function mall_group_init(_delete_stats, _delete_states, _delete_parts, _delete_elements) {
    var _storage = MALL_STORAGE;    
        
    MALL_GROUP.InitStats (_delete_stats );
    MALL_GROUP.InitStates(_delete_states);
    MALL_GROUP.InitParts (_delete_parts );
    
    MALL_GROUP.InitElements(_delete_elements);
}

