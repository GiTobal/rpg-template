function MallPart(_key) : MallComponent(_key) constructor {
    __numbers  = 1;  // Cantidad de la misma parte (EJ: dedos: 5 ; brazos: 2)
    __equipMax = 1;  // Cuantos items se pueden equipar en esta parte (mano: guantes, arma)
    __active = array_create(1, true);  // Que parte inicia true o false
  
    // Daño
    __damage = {
        use: false,
        top: noone,
        bottom: noone
    };
    
    __items = {};   // Que tipos de objetos puede llevar y si posee un bonus o no.
    
    __linked = [];  // Que partes estan unidas a esta.
    __joint  = {};  // A que parte esta unida.
    
    __affectedBy = {};
    
    #region Metodos
    
    /// @param item_key
    /// @param bonus_value
    /// @param number_type
    /// @param ...
    /// Indica que objetos puede equipar esta parte y el bonus agregado
    static SetItemType  = function(_itemKey, _bonus = 0, _type = 0) {
        if (argument_count > 3) {
            var i = 0; repeat(argument_count div 3) {
            	var _key = argument[i++];
            	var _bns = argument[i++];
            	var _tp = argument[i++];
            	
                SetItemType(_key, _bns, _tp);    
            }
        } else {
            // Agrega el objeto al diccionario
            __items[$ _itemKey] = mall_data(_bonus, _type);
        }
        
        return self;    
    }
    
    /// @param item_key
    static GetItem = function(_itemKey) {
        return (__items[$ _itemKey] );
    }
    
    /// @param part_key
    static Link = function(_partKey) {
        array_push(__linked, _partKey);    
            
        MALL_GROUP.__parts[$ _partKey].Joint(__key);
        return self;
    }
    
    /// @param part_key
    static Joint = function(_partKey) {
        __joint[$ _partKey] = true;
        return self;
    }
    
    #endregion
    
}

/// @param delete_array*
function mall_part_init(_deleteArray) {
    MALL_GROUP.InitParts(_deleteArray);
}

/// @returns {array}
function mall_get_parts() {
    return (MALL_STORAGE.__partsMaster).GetKeys();
}

/// @param part_key
/// @param set_group*
/// @returns {MallStat}
/// Regresa una configuracion de estadistica del grupo actual (No uso actual)
function mall_get_part(_key, _group) {
	// Se paso otra cosa
	if (is_string(_group) ) mall_set_group(_group);
	
	return (MALL_GROUP.__parts[$ _key] );
}

/// @param part_key
/// @param item_type
/// Obtiene el bonus que posee esta parte respecto a un objeto, si no existe devuelve 0
function mall_part_bonus(_key, _itemType) {
    var _part = mall_get_part(_key);
        
    return (_part.GetItem(_itemType) ?? 0);    
}

/// @param {string} part_key
/// @param number       Cantidad de estas partes y su valor inicial EJ: [true, false, false] 
/// @param equip_max
/// @returns {MallPart}
function mall_part_customize(_key, _number, _equip = 1) {
    var _part = mall_get_part(_key);
    
    // Copiar nuevos valores iniciales para cada numero
    if (is_array(_number) ) {
        // Copiar activos y cantidad de mismas partes
        var _len = array_length(_number);
        
        array_copy(_part.active, 0, _number, 0, _len);
        _part.__numbers = _len;
    } else {    // 1 solo valor sin array 
        var _temp = _number ?? true;
        
        _part.__active  = array_create(1,_temp);
        _part.__numbers = 1; 
    }
    
    _part.__equipMax = _equip;
    
    return (_part );
}

