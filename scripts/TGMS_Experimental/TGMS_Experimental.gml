

function TPTrack(property, start, track_target, track_variable)	
{ 
	return {data:[TPTrack, property, start, is_struct(track_target)? weak_ref_create(track_target) : track_target, track_variable, undefined]}; 
}


TPFuncShared(TPTrack, function(_value, _target, _data, _t) 
{	
	// Cache data execution for better performance -- called only the first time
	if (is_undefined(_data[4]))
	{
		if (is_array(_data[0]))
		{
			_data[@ 4] = 0;
		}
		else
		if (ds_map_exists(global.TGMS.PropertySetters, _data[0]))
		{
			_data[@ 0] = global.TGMS.PropertySetters[? _data[0]];
			
			if (ds_map_exists(global.TGMS.PropertyGetters, _data[3]))
			{
				_data[@ 4] = 1;
				_data[@ 3] = global.TGMS.PropertyGetters[? _data[3]];
			}
			else // Global Target
			if (is_real(_data[2]) && _data[2] == global)
			{
				_data[@ 4] = 2;
			}
			else // Instance Target
			{	
				_data[@ 4] = 3;
			}
		}
		else
		if (is_struct(_target) && weak_ref_alive(_target))
		{
			_data[@ 4] = 4;
		}
		else
		if (variable_instance_exists(_target, _data[0]))
		{
			_data[@ 4] = 5;
		}
		else
		{
			_data[@ 4] = 6;
		}
	}
	
	// Jump to execution type based
	switch(_data[4])
	{
	case 0: // ARRAY
		var _array = _data[0];
		var _length = array_length(_array)-1;
	
		if (_length == 1)
		{	
			_data = _array[1];
		}
		else
		{
			_data = array_create(_length);
			array_copy(_data, 0, _array, 1, _length);
		}
	
		var _script = _array[0];
		_script(_value, _target, _data, _t);
	break;
	case 1: _data[0](lerp(_data[1], _data[3](_data[2]), _value), _target, _data[0], _t);			break; // Built setter with built getter
	case 2: _data[0](lerp(_data[1], variable_global_get(_data[3]), _value), _target, _data[0], _t); break; // Global with built setter
	case 3: _data[0](lerp(_data[1], _data[2][$ _data[3]], _value), _target, _data[0], _t);			break; // Target with built setter and dynamic getter
	case 4: _target.ref[$ _data[0]] = lerp(_data[1], _data[2][$ _data[3]], _value)					break; // Struct Target
	case 5: variable_instance_set(_target, _data[0], lerp(_data[1], _data[2][$ _data[3]], _value)); break; // Instance Target
	case 6: variable_global_set(_data[0], lerp(_data[1], _data[2][$ _data[3]], _value));			break; // Global Target
	}
});
