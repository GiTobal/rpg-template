global._FOTILO = noone;   /// @is {__fo_struct}

#macro FOTILO global._FOTILO	/// @is {__fo_struct}

// -- Camara --
#macro FOTILO_CAM   FOTILO.cam_id

#macro FOTILO_X FOTILO.frame.x1
#macro FOTILO_Y FOTILO.frame.y1

#macro FOTILO_CEILING 256

#macro FOTILO_W FOTILO.frame.w
#macro FOTILO_H FOTILO.frame.h

// -- GUI --
#macro FOTILO_GUI_W display_get_gui_width ()	// Utilizar normalmente a menos que se haga un zoom
#macro FOTILO_GUI_H display_get_gui_height()	// Utilizar normalmente a menos que se haga un zoom

#macro FOTILO_ASPECT_RATIO (display_get_width() / display_get_height())

// Funciones para manejar las camaras
function fo_create(_camera, _x, _y, _w, _h) {
	return (new __fo_struct(_camera, _x, _y, _w, _h) );
}

/// @param {camera} camera_id
/// @param {number} x
/// @param {number} y
/// @param {array} w
/// @param {array} h
function __fo_struct(_camera = view_camera[0], _x = 0, _y = 0, _w = [640, 1024, 1280], _h = [480, 720, 768, 800]) constructor {
	// Variables
	cam_id	= _camera;
		
	objetive = [];
	objetive_len = array_length(objetive);
	
	work	= false;
	working = false;

	w_array = (!is_array(_w) ) ? [_w] : _w;
	h_array = (!is_array(_h) ) ? [_h] : _h;
	
	frame = (new Rectangle(_x, _y, _w[0], _h[0] ) );	/// @is {Rectangle}
	frame_start = frame.Copy();
	
	// Propiedades
	follow_w = 0.2;
	follow_h = 0.4;

	zoom_event = false;
	rest_event = false;
	
	angl = 0;
	angl_event = false;
	
	// Shake
	shak_event = false;
	
	shak_time  = noone;
	shak_force = noone;
	
	#region Methods
	static DisplayReset = function(_w, _h, _x, _y) {
		delete frame;
		frame = frame_start.Copy();
	}
	
	static DisplaySet = function() {
		surface_resize(application_surface, frame.w, frame.h);
		
		return self;
	}
	
	/// @param reset_script
	static DisplayFullscreen = function(_resetScript) {
		var _do = _resetScript(), _tmp = 0;

		while(_tmp <= 2) {
			switch (_tmp) {
				case 0: window_set_fullscreen(_do);	break;
				case 1: window_center();			break;
			}
			
			_tmp++;
		}	
	}
	
	/// @returns {Rectangle}
	static GetFrame = function() {return frame; }
	
	/// @param x_oper
	/// @param y_oper
	static Basic = function(_xop, _yop) {
		return (SetPosition(frame.x1 + _xop, frame.y1 + _yop) );	
	}
	
	/// @param x
	/// @param y
	static SetPosition = function(_x, _y) {
		frame.SetBounds(_cam_x, _cam_y);
		camera_set_view_pos(cam_id, _x, _y);
		
		return self;
	}
	
	static GetPosition = function() {
		return (new Vector2(frame.x1, frame.y1) );
	}
	
	/// @param width
	/// @param height
	static SetSize = function(_w, _h) {
		if (_w == undefined) _w = frame.w;
		if (_h == undefined) _h = frame.h;

		frame.SetSize(_w, _h);
		camera_set_view_size(cam_id, _w * frame.wrel, _h * frame.hrel);
		
		return self;
	}
	
	/// @param angle
	static SetAngle = function(_angl) {
		if (_angl == undefined) _angl = angl;
		
		angl = _angl;
		camera_set_view_angle(cam_id, angl);
		
		return self;
	}
	
	/// @param objetive
	/// @param work
	static AddObjetive = function(_obj, _work = false) {
		// Agrega objeto
		if (is_array(_obj) ) {
			var i = 0, _size = array_length(_obj);
			
			repeat(_size) {array_push(objetive, _obj[i] ); ++i;}
			
		} else array_push(objetive, _obj);

		objetive_len = array_length(objetive);
		working = _work;
	}
	
	/// @param bool
	static ToggleWork = function(_bool = !working) {
		working = _bool;	
		
		return self;
	}
	
	/// @returns {array} zoom y zoom inverso
	static GetZoomW = function() {
		return [frame.wrel, 1 / max(0.1, frame.wrel) ];
	}
	
	static GetZoomH = function() {
		return [frame.hrel, 1 / max(0.1, frame.hrel) ];		
	}
	
		#region Eventos
	static EventFollow = function() {
		if (!working) exit;	
		
		var _x = 0, _y = 0;
		
		var _cam_x = frame.x1;
		var _cam_y = frame.y1;
		
		var i = 0; repeat(objetive_len) {
			_x += objetive[i].x;
			_y += objetive[i].y;
		
			++i;
		}

		// Obtener la media
		_x = clamp((_x / objetive_len) - (frame.w / 2), 0, room_width  - frame.w);
		_y = clamp((_y / objetive_len) - (frame.h / 2), 0, room_height - frame.h);
		
		_cam_x = lerp(_cam_x, round(_x), follow_w);
		_cam_y = lerp(_cam_y, round(_y), follow_h);
		
		// Establecer
		SetPosition(_cam_x, _cam_y);
	}
	
	/// @param ease
	/// @param time
	/// @param delay
	/// @param [callback]
	/// @desc Regresa a los valores originales utilizando un Tween.
	static EventRestore = function(_ease, _time = 1, _delay = 0, _callback) {
		if (is_undefined(_callback) ) _callback = function() {FOTILO.rest_event = false; }
		
		var _tween = false;
		
		if (!rest_event) {
			var _tween = TweenFire("~", _ease, "$", _time, "+", _delay,
				"frame.wrel>", frame_start.wrel, "frame.hrel>", frame_start.hrel,	// zoom
				
				"frame.w>" , frame_start.w , "frame.h>" , frame_start.h ,	// Tamaño
				"frame.x1>", frame_start.x1, "frame.y1>", frame_start.y1,	// Posicion
				
				"@", _callback	// Callback
			);
			
			rest_event = true;
		} else {_tween = false; }

		return _tween;
	}  

	/// @param ease
	/// @param time
	/// @param delay
	/// @param zoom
	/// @param x
	/// @param y
	/// @param [callback]
	/// @desc Hace un ZoomIn a una posicion en especifico
	static EventZoom  = function(_ease, _time, _delay, _zoom, _x, _y, _callback) {
		if (is_undefined(_callback) ) _callback = function() {FOTILO.zoom_event = false; }
		
		var _tween = undefined;
		
		if (!zoom_event) {
			_tween = TweenFire("~", _ease, "$", _time, "+", _delay, "frame.wrel", _zoom, "frame.hrel", _zoom, "@", _callback);
			zoom_event = true;
		} else {
			var target_x_abs = _x - frame.x1;
			var target_y_abs = _y - frame.y1;			
		
			var target_x_p = target_x_abs / frame.GetWidthRelative ();
			var target_y_p = target_y_abs / frame.GetHeightRelative();
			
			SetSize();
			
			// Calculate what the new distance from the target to the origin should be.
			var target_x_new_abs = frame.GetWidthRelative () * target_x_p;
			var target_y_new_abs = frame.GetHeightRelative() * target_y_p;
		
			// Set the origin based on where the object should be.
			SetPosition(_x - target_x_new_abs, _y - target_y_new_abs);
		}
		
		return _tween;
	}
	
	/// @param time
	/// @param force
	/// @param fade
	static EventShake = function(_time, _force, _fade) {
		// Mientras no haya tiempo
		if (shak_time == noone) {
			shak_time  = _time;
			shak_force = _force;
		}
		
		if (!shak_event) {
			// Reducir tiempo
			shak_time--;
			
			var _xran = choose(-shak_force, shak_force);
			var _yran = choose(-shak_force, shak_force);
			
			// Sumar y restar
			Basic(_xran, _yran);
		
			if (shak_time <= 0) {
				shak_force -= _fade;
				
				if (shak_force <= 0) shak_event = true;
			}
			
		} else {SetPosition(frame_start.x1, frame_start.y1); }
		
		return shak_force;
	}
	
	/// @param ease
	/// @param time
	/// @param delay
	/// @param angle
	/// @param [callback]
	static EventRotate = function(_ease, _time, _delay, _angle, _callback) {
		if (is_undefined(_callback) ) _callback = function() {FOTILO.angl_event = false; }
		
		var _tween = undefined;
		
		if (!angl_event) {
			_tween = TweenFire("~", _ease, "$", _time, "+", _delay, "angl>", _angle, "@", _callback);
			angl_event = true;
			
		} else {SetAngle(); }
		
		return _tween;
	}	
	
	/// @param camera_id
	static EventRoom = function(_cam) {
		cam_id = _cam;
		
		// Actualizar camara nueva.
		SetPosition();
		SetSize ();
		SetAngle();
		
		return self;
	}
	
	#endregion	
	
	#endregion
}

FOTILO = fo_create();