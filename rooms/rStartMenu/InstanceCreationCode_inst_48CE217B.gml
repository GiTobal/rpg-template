skb.msdf_border(c_black, 1);

Templates.selected   = function() {
	blend(c_red, 1);	
}

Templates.deselected = function() {
	blend(c_white, 1);	
}
	
// Evento de seleccion	
lima_modify_event("SELECT",,,function() {
	// Desactivar compañeros
	inst_134FFBC6.Disable();
	inst_64D3F791.Disable();
	inst_2EC05291.Disable();
	
	Disable();
	
	inst_652F88C7.Activate(true);
});