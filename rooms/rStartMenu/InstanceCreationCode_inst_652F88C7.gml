RepeatExecute = function(count) {
	static pos = new Vector2(24, 8);
	
	static first = noone;
	static last  = noone;
	static pass  = noone;
	static cret = id;
	
	var _panel = instance_create_layer(pos.x + repeats_xoffset * count, pos.y + repeats_yoffset * count, layer, lima_panel);
	
	with (_panel) {
		creator = cret;
		
		// Cambiar xscale
		image_xscale =  37;
		image_yscale = 4.5;
		
		// Desactivar
		isActive = false;
		isFocus  = false;
		
		// Cambiar color
		image_blend = make_color_hsv(233, 70, 40);
	}
	
	var _textGame = instance_create_layer(_panel.x + 8, _panel.y - 2, layer - 1, lima_text);
	
	with (_textGame) {
		creator = other.id;

		text = "Partida " + string(count + 1);	
		font = "sprMSDF_font01";
		color = c_white;
		
		// Actualizar texto
		updateText();	
		
		// Templates
		Templates.selected	 = function() {blend(c_red  , 1); } 
		Templates.deselected = function() {blend(c_white, 1); }
		
		// Poner bordes
		skb.msdf_border(c_black, 1);
		
		isVisible = true;
		isActive = true;
		isFocus  = false;

		pressedD = 0;
		pressedU = 0;
		
		#region Logica
		if (count == 0) {
			first = id;
			isActive = true;
			isFocus  = true;
		} 
		else if (count == other.repeats - 1) {
			// El ultimo regresar arriba del todo
			select_up	=  pass;
			select_down	= first;
			
			pass.select_down = id;
			first.select_up  = id;
		}
		else {			
			// Cambiar selecciones
			select_up = pass;
			pass.select_down = id;
		}
		
		pass = id;
		
		#endregion
	
		#region Metodos
		lima_modify_event("SELECT_UP",,[keyboard_check_direct, vk_up] );
		lima_extend_event("SELECT_UP", function() {
			var _cindex = creator.index	, _cinlast = creator.index_last, _crep = creator.repeats;
			var _step	= creator.steps	, _h	= parent.sprite_height + 1, _steph = _h * _step;
			
			if (_cindex == 0) {
				var _hbot = _h * (_crep - (_crep div _step) + _step * 2);
				// Subir bajar al final				
				creator.Teleport(0, -_hbot, true);
				creator.index = _crep - 1;
				
				print("hbot: " + string(_hbot) );
			}
			else {
				if ((_cindex % _step) == 0) {
					if (_cindex != 0) {
						if (creator.index_now == creator.index_last - 1) creator.Teleport(0, _steph, true);
					}
				}
				
				creator.Advance(-1);
			}
			
			// Delay arriba
			alarm_delay(10, max(1, floor(8 - (creator.pressed div 16) ) ), select_up);	
			creator.pressed++;
			
			print(string(creator.index) );
		});
		
		lima_modify_event("SELECT_DOWN",,[keyboard_check_direct, vk_down] );
		lima_extend_event("SELECT_DOWN", function() {
			var _cindex = creator.index	, _crep = creator.repeats;
			var _step	= creator.steps	, _h	= parent.sprite_height + 1, _steph = _h * _step;
			
			if (_cindex == _crep - 1) {
				var _htop = _h * (_crep - (_crep div _step) + _step * 2);
				// Subir al inicio	
				creator.Teleport(0, +_htop, true);
				creator.index = 0;
				
				print("htop: " + string(_htop) );
			}
			else {
				if ((_cindex % _step) == 0) {
					if (_cindex != 0) {
						if (creator.index_now - 1  == creator.index_last) creator.Teleport(0, -_steph, true);
					}
				}
				
				creator.Advance(1);
			}
			
			// Delay abajo
			alarm_delay(10, max(1, floor(8 - (creator.pressed div 16) ) ), select_down);
			creator.pressed++;
		});
		
		lima_create_event("RELEASE_UP  ", LIMA_EVTYPE.ONKEY, [keyboard_check_released, vk_up]  , function() {creator.pressed = 0;} );
		lima_create_event("RELEASE_DOWN", LIMA_EVTYPE.ONKEY, [keyboard_check_released, vk_down], function() {creator.pressed = 0;} );
		
		
		#endregion
		
		// Agregar hijo al panel
		lima_send_child(_panel);
	}
	
	var _textTime = instance_create_layer(_panel.bbox_right - 8, _panel.y - 2, layer - 1, lima_text);
	
	with (_textTime) {
		creator = other.id;
		
		// Texto	
		text = "Tiempo: " + string_time_hms(1600);	
		font = "sprMSDF_font01";
		color = c_white;
		halign = fa_right;
		
		// Actualizar texto
		updateText();	
		
		// Poner bordes
		skb.msdf_border(c_black, 1);
		
		isVisible = true;
		isActive = false;
		isFocus  = false;
		
		// Agregar hijo al panel
		lima_send_child(_panel);		
	}
	
	var _textPosition = instance_create_layer(_panel.x + 8, _panel.y + 38, layer - 1, lima_text);
	
	with (_textPosition) {
		creator = other.id;
		
		text = "Lugar: Catedral";
		font =  "sprMSDF_font01";
		color = c_white;
		
		// Actualizar texto
		updateText();
		
		// Poner bordes
		skb.msdf_border(c_black, 1);
		
		isVisible = true;
		isActive = false;
		isFocus  = false;
		
		// Agregar hijo al panel
		lima_send_child(_panel);		
	}
	
	// Aumentar posicion (1 es constante)
	pos.y += _panel.sprite_height + 1;
	
	// Agregar a los elementos
	ds_list_add(elements, _panel);
}



