{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 7,
  "bbox_top": 0,
  "bbox_bottom": 7,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 8,
  "height": 8,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c85368c7-19ae-41fd-80cd-b375c6becf1f","path":"sprites/sprOccluder01/sprOccluder01.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c85368c7-19ae-41fd-80cd-b375c6becf1f","path":"sprites/sprOccluder01/sprOccluder01.yy",},"LayerId":{"name":"969801e9-ac86-4da5-ab5d-7cda78d34a16","path":"sprites/sprOccluder01/sprOccluder01.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"c85368c7-19ae-41fd-80cd-b375c6becf1f","path":"sprites/sprOccluder01/sprOccluder01.yy",},"LayerId":{"name":"f861c01e-b746-4297-b40c-51f1d596c33e","path":"sprites/sprOccluder01/sprOccluder01.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"c85368c7-19ae-41fd-80cd-b375c6becf1f","path":"sprites/sprOccluder01/sprOccluder01.yy",},"LayerId":{"name":"0b9d8cdc-6a1c-4ddb-9f18-c249ef2cca7c","path":"sprites/sprOccluder01/sprOccluder01.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"c85368c7-19ae-41fd-80cd-b375c6becf1f","path":"sprites/sprOccluder01/sprOccluder01.yy",},"LayerId":{"name":"10a29229-dfc8-4ea0-bf20-b156c0262e06","path":"sprites/sprOccluder01/sprOccluder01.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprOccluder01","path":"sprites/sprOccluder01/sprOccluder01.yy",},"resourceVersion":"1.0","name":"c85368c7-19ae-41fd-80cd-b375c6becf1f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprOccluder01","path":"sprites/sprOccluder01/sprOccluder01.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"69769d88-9375-4130-9336-64bd37763af7","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c85368c7-19ae-41fd-80cd-b375c6becf1f","path":"sprites/sprOccluder01/sprOccluder01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprOccluder01","path":"sprites/sprOccluder01/sprOccluder01.yy",},
    "resourceVersion": "1.4",
    "name": "sprCaster_01",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 3","resourceVersion":"1.0","name":"969801e9-ac86-4da5-ab5d-7cda78d34a16","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"f861c01e-b746-4297-b40c-51f1d596c33e","tags":[],"resourceType":"GMImageLayer",},
    {"visible":false,"isLocked":true,"blendMode":0,"opacity":55.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"0b9d8cdc-6a1c-4ddb-9f18-c249ef2cca7c","tags":[],"resourceType":"GMImageLayer",},
    {"visible":false,"isLocked":true,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"10a29229-dfc8-4ea0-bf20-b156c0262e06","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": {
    "left": 1,
    "top": 1,
    "right": 1,
    "bottom": 1,
    "guideColour": [
      4294902015,
      4294902015,
      4294902015,
      4294902015,
    ],
    "highlightColour": 1728023040,
    "highlightStyle": 0,
    "enabled": true,
    "tileMode": [
      0,
      0,
      0,
      0,
      1,
    ],
    "resourceVersion": "1.0",
    "loadedVersion": null,
    "resourceType": "GMNineSliceData",
  },
  "parent": {
    "name": "Sprite",
    "path": "folders/Bulb/Occluders/Sprite.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprOccluder01",
  "tags": [],
  "resourceType": "GMSprite",
}